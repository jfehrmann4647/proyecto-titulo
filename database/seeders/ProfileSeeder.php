<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        return \DB::table('profiles')->insert([
            [
                "id" => 1,
                "prf_first_name" => 'Alain',
                "prf_second_name" =>null,
                "prf_first_surname" => 'Moraga',
                "prf_second_surname" =>null,
                "prf_cellphone" =>null,
                "prf_address" =>null,
                "prf_birthday" =>null,
                "prf_gender" =>null,
                "prf_description" =>null,
                "gcf_code" => 1
            ],
            [
                "id" => 2,
                "prf_first_name" => 'Johann',
                "prf_second_name" =>null,
                "prf_first_surname" => 'Fehrmann',
                "prf_second_surname" =>null,
                "prf_cellphone" =>null,
                "prf_address" =>null,
                "prf_birthday" =>null,
                "prf_gender" =>null,
                "prf_description" =>null,
                "gcf_code" => 2
            ],
            [
                "id" => 3,
                "prf_first_name" => 'Patricia',
                "prf_second_name" =>null,
                "prf_first_surname" => 'Monares',
                "prf_second_surname" =>null,
                "prf_cellphone" =>null,
                "prf_address" =>null,
                "prf_birthday" =>null,
                "prf_gender" =>null,
                "prf_description" =>null,
                "gcf_code" => 3
            ],
            [
                "id" => 4,
                "prf_first_name" => 'Gabriel',
                "prf_second_name" =>null,
                "prf_first_surname" => 'Reyes',
                "prf_second_surname" =>null,
                "prf_cellphone" =>null,
                "prf_address" =>null,
                "prf_birthday" =>null,
                "prf_gender" =>null,
                "prf_description" =>null,
                "gcf_code" => 4
            ],
            [
                "id" => 5,
                "prf_first_name" => 'Ignacio',
                "prf_second_name" =>null,
                "prf_first_surname" => 'Pardo',
                "prf_second_surname" =>null,
                "prf_cellphone" =>null,
                "prf_address" =>null,
                "prf_birthday" =>null,
                "prf_gender" =>null,
                "prf_description" =>null,
                "gcf_code" => 5
            ],
            [
                "id" => 6,
                "prf_first_name" => 'Pablo',
                "prf_second_name" =>null,
                "prf_first_surname" => 'Sandoval',
                "prf_second_surname" =>null,
                "prf_cellphone" =>null,
                "prf_address" =>null,
                "prf_birthday" =>null,
                "prf_gender" =>null,
                "prf_description" =>null,
                "gcf_code" => 6
            ],
            [
                "id" => 7,
                "prf_first_name" => 'Andres',
                "prf_second_name" =>null,
                "prf_first_surname" => 'Vargas',
                "prf_second_surname" =>null,
                "prf_cellphone" =>null,
                "prf_address" =>null,
                "prf_birthday" =>null,
                "prf_gender" =>null,
                "prf_description" =>null,
                "gcf_code" => 7
            ],
            [
                "id" => 8,
                "prf_first_name" => 'Alexander',
                "prf_second_name" =>null,
                "prf_first_surname" => 'Rojas',
                "prf_second_surname" =>null,
                "prf_cellphone" =>null,
                "prf_address" =>null,
                "prf_birthday" =>null,
                "prf_gender" =>null,
                "prf_description" =>null,
                "gcf_code" => 8
            ],
            [
                "id" => 9,
                "prf_first_name" => 'Alejandra',
                "prf_second_name" =>null,
                "prf_first_surname" => 'Montoya',
                "prf_second_surname" =>null,
                "prf_cellphone" =>null,
                "prf_address" =>null,
                "prf_birthday" =>null,
                "prf_gender" =>null,
                "prf_description" =>null,
                "gcf_code" => 9
            ],
            [
                "id" => 10,
                "prf_first_name" => 'Valentina',
                "prf_second_name" =>null,
                "prf_first_surname" => 'Rojas',
                "prf_second_surname" =>null,
                "prf_cellphone" =>null,
                "prf_address" =>null,
                "prf_birthday" =>null,
                "prf_gender" =>null,
                "prf_description" =>null,
                "gcf_code" => 10
            ]
        ]);
    }
}

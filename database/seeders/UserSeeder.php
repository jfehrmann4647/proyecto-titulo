<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        return \DB::table('users')->insert([
            [
                "id" => 1,
                "name" => 'AlainMoraga',
                "email" => 'alain.andres.moraga.vargas@gmail.com',
                "password" => Hash::make('12345678'),
                "profile_photo_path" => 'img/default-photos/alain-moraga.jpg',
                "status_connection" => 0,
                "status_new_user" => 1,
                "status_account" => 1,
                "prf_code" => 1 
            ],
            [
                "id" => 2,
                "name" => 'JohannFehrmann',
                "email" => 'jfehrmannr@gmail.com',
                "password" => Hash::make('12345678'),
                "profile_photo_path" => 'img/default-photos/johann-fehrmann.jpg',
                "status_connection" => 0,
                "status_new_user" => 1,
                "status_account" => 1,
                "prf_code" => 2
            ],
            [
                "id" => 3,
                "name" => 'patriciaMonares',
                "email" => 'patricia@gmail.com',
                "password" => Hash::make('12345678'),
                "profile_photo_path" => 'img/default-photos/patricia-monares.jpg',
                "status_connection" => 0,
                "status_new_user" => 1,
                "status_account" => 1,
                "prf_code" => 3
            ],
            [
                "id" => 4,
                "name" => 'gabrielReyes',
                "email" => 'gabriel@gmail.com',
                "password" => Hash::make('12345678'),
                "profile_photo_path" => 'img/default-photos/gabriel-reyes.jpg',
                "status_connection" => 0,
                "status_new_user" => 1,
                "status_account" => 1,
                "prf_code" => 4
            ],
            [
                "id" => 5,
                "name" => 'ignacio',
                "email" => 'ignacio@gmail.com',
                "password" => Hash::make('12345678'),
                "profile_photo_path" => 'img/default-photos/breaknotes-profile.png',
                "status_connection" => 0,
                "status_new_user" => 1,
                "status_account" => 1,
                "prf_code" => 5 
            ],
            [
                "id" => 6,
                "name" => 'pabloSandoval',
                "email" => 'pablo@gmail.com',
                "password" => Hash::make('12345678'),
                "profile_photo_path" => 'img/default-photos/pablo-sandoval.jpg',
                "status_connection" => 0,
                "status_new_user" => 1,
                "status_account" => 1,
                "prf_code" => 6 
            ],
            [
                "id" => 7,
                "name" => 'andres',
                "email" => 'andres@gmail.com',
                "password" => Hash::make('12345678'),
                "profile_photo_path" => 'img/default-photos/breaknotes-profile.png',
                "status_connection" => 0,
                "status_new_user" => 1,
                "status_account" => 1,
                "prf_code" => 7 
            ],
            [
                "id" => 8,
                "name" => 'alexander',
                "email" => 'alexander@gmail.com',
                "password" => Hash::make('12345678'),
                "profile_photo_path" => 'img/default-photos/breaknotes-profile.png',
                "status_connection" => 0,
                "status_new_user" => 1,
                "status_account" => 1,
                "prf_code" => 8 
            ],
            [
                "id" => 9,
                "name" => 'alejandra',
                "email" => 'alejandra@gmail.com',
                "password" => Hash::make('12345678'),
                "profile_photo_path" => 'img/default-photos/breaknotes-profile.png',
                "status_connection" => 0,
                "status_new_user" => 1,
                "status_account" => 1,
                "prf_code" => 9
            ],
            [
                "id" => 10,
                "name" => 'valentinaPeña',
                "email" => 'valentina@gmail.com',
                "password" => Hash::make('12345678'),
                "profile_photo_path" => 'img/default-photos/valentina-pena.jpg',
                "status_connection" => 0,
                "status_new_user" => 1,
                "status_account" => 1,
                "prf_code" => 10 
            ]
        ]);
    }
}

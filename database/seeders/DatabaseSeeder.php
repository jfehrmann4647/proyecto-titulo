<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(GlobalConfigSeeder::class);
        $this->call(ProfileSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(UserFollowerSeeder::class);
        $this->call(InterestingSeeder::class);
    }
}

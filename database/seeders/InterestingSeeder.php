<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class InterestingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        return \DB::table('interestings')->insert([
            [
                "id" => 1,
                "itg_name" => 'Música',
                "itg_description" => 'Amantes de la música en todas sus formas.',
                "itg_photo_path" => '/img/breaknotes-interestings/musica.jpg'
            ],
            [
                "id" => 2,
                "itg_name" => 'Programación',
                "itg_description" => '¿Quieres crear aplicaciones y páginas web?',
                "itg_photo_path" => '/img/breaknotes-interestings/programacion.jpg'
            ],
            [
                "id" => 3,
                "itg_name" => 'Historia',
                "itg_description" => 'Los sucesos que marcaron el camino del mundo.',
                "itg_photo_path" => '/img/breaknotes-interestings/historia.jpg'
            ],
            [
                "id" => 4,
                "itg_name" => 'Ciencias',
                "itg_description" => '¿Crees ser tan bueno como Einstein?.',
                "itg_photo_path" => '/img/breaknotes-interestings/ciencias.jpg'
            ],
            [
                "id" => 5,
                "itg_name" => 'Política',
                "itg_description" => 'La forma ideológica de tomar decisiones individuales o colectivas.',
                "itg_photo_path" =>  '/img/breaknotes-interestings/politica.jpg'
            ],
            [
                "id" => 6,
                "itg_name" => 'Deportes',
                "itg_description" => '¿Te gusta la actividad física y vida sana?',
                "itg_photo_path" =>  '/img/breaknotes-interestings/deportes.jpg'
            ],
            [
                "id" => 7,
                "itg_name" => 'Astronomía',
                "itg_description" => 'astronomia',
                "itg_photo_path" =>  '/img/breaknotes-interestings/astronomia.jpg'
            ],
            [
                "id" => 8,
                "itg_name" => 'Tecnología',
                "itg_description" => 'tecnologia',
                "itg_photo_path" =>  '/img/breaknotes-interestings/tecnologia.jpg'
            ],
            [
                "id" => 9,
                "itg_name" => 'Matematicas',
                "itg_description" => 'Matematicas',
                "itg_photo_path" =>  '/img/breaknotes-interestings/matematicas.jpg'
            ],
            [
                "id" => 10,
                "itg_name" => 'Medioambiente',
                "itg_description" => 'Medioambiente',
                "itg_photo_path" =>  '/img/breaknotes-interestings/medioambiente.jpg'
            ]
        ]);
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserFollowerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        return \DB::table('user_followers')->insert([
            [
                "id" => 1,
                "ufl_following_list" => '["0"]',
                "ufl_follower_list" => '["0"]',
                "usr_user" => 1 
            ],
            [
                "id" => 2,
                "ufl_following_list" => '["0"]',
                "ufl_follower_list" => '["0"]',
                "usr_user" => 2 
            ],
            [
                "id" => 3,
                "ufl_following_list" => '["0"]',
                "ufl_follower_list" => '["0"]',
                "usr_user" => 3 
            ],
            [
                "id" => 4,
                "ufl_following_list" => '["0"]',
                "ufl_follower_list" => '["0"]',
                "usr_user" => 4
            ],
            [
                "id" => 5,
                "ufl_following_list" => '["0"]',
                "ufl_follower_list" => '["0"]',
                "usr_user" => 5 
            ],
            [
                "id" => 6,
                "ufl_following_list" => '["0"]',
                "ufl_follower_list" => '["0"]',
                "usr_user" => 6 
            ],            [
                "id" => 7,
                "ufl_following_list" => '["0"]',
                "ufl_follower_list" => '["0"]',
                "usr_user" => 7 
            ],
            [
                "id" => 8,
                "ufl_following_list" => '["0"]',
                "ufl_follower_list" => '["0"]',
                "usr_user" => 8 
            ],
            [
                "id" => 9,
                "ufl_following_list" => '["0"]',
                "ufl_follower_list" => '["0"]',
                "usr_user" => 9 
            ],
            [
                "id" => 10,
                "ufl_following_list" => '["0"]',
                "ufl_follower_list" => '["0"]',
                "usr_user" => 10 
            ]

        ]);
    }
}

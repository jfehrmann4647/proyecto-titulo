<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class GlobalConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        return \DB::table('global_configs')->insert([
            [
                "id" => 1,
                "gcf_first_name" => 1,
                "gcf_second_name" => 0,
                "gcf_first_surname" => 1,
                "gcf_second_surname" => 0,
                "gcf_cellphone" => 0,
                "gcf_address" => 0,
                "gcf_birthday" => 0,
                "gcf_gender" => 0,
                "gcf_description" => 0
            ],
            [
                "id" => 2,
                "gcf_first_name" => 1,
                "gcf_second_name" => 0,
                "gcf_first_surname" => 1,
                "gcf_second_surname" => 0,
                "gcf_cellphone" => 0,
                "gcf_address" => 0,
                "gcf_birthday" => 0,
                "gcf_gender" => 0,
                "gcf_description" => 0
            ],
            [
                "id" => 3,
                "gcf_first_name" => 1,
                "gcf_second_name" => 0,
                "gcf_first_surname" => 1,
                "gcf_second_surname" => 0,
                "gcf_cellphone" => 0,
                "gcf_address" => 0,
                "gcf_birthday" => 0,
                "gcf_gender" => 0,
                "gcf_description" => 0
            ],
            [
                "id" => 4,
                "gcf_first_name" => 1,
                "gcf_second_name" => 0,
                "gcf_first_surname" => 1,
                "gcf_second_surname" => 0,
                "gcf_cellphone" => 0,
                "gcf_address" => 0,
                "gcf_birthday" => 0,
                "gcf_gender" => 0,
                "gcf_description" => 0
            ],
            [
                "id" => 5,
                "gcf_first_name" => 1,
                "gcf_second_name" => 0,
                "gcf_first_surname" => 1,
                "gcf_second_surname" => 0,
                "gcf_cellphone" => 0,
                "gcf_address" => 0,
                "gcf_birthday" => 0,
                "gcf_gender" => 0,
                "gcf_description" => 0
            ],
            [
                "id" => 6,
                "gcf_first_name" => 1,
                "gcf_second_name" => 0,
                "gcf_first_surname" => 1,
                "gcf_second_surname" => 0,
                "gcf_cellphone" => 0,
                "gcf_address" => 0,
                "gcf_birthday" => 0,
                "gcf_gender" => 0,
                "gcf_description" => 0
            ],
            [
                "id" => 7,
                "gcf_first_name" => 1,
                "gcf_second_name" => 0,
                "gcf_first_surname" => 1,
                "gcf_second_surname" => 0,
                "gcf_cellphone" => 0,
                "gcf_address" => 0,
                "gcf_birthday" => 0,
                "gcf_gender" => 0,
                "gcf_description" => 0
            ],
            [
                "id" => 8,
                "gcf_first_name" => 1,
                "gcf_second_name" => 0,
                "gcf_first_surname" => 1,
                "gcf_second_surname" => 0,
                "gcf_cellphone" => 0,
                "gcf_address" => 0,
                "gcf_birthday" => 0,
                "gcf_gender" => 0,
                "gcf_description" => 0
            ],
            [
                "id" => 9,
                "gcf_first_name" => 1,
                "gcf_second_name" => 0,
                "gcf_first_surname" => 1,
                "gcf_second_surname" => 0,
                "gcf_cellphone" => 0,
                "gcf_address" => 0,
                "gcf_birthday" => 0,
                "gcf_gender" => 0,
                "gcf_description" => 0
            ],
            [
                "id" => 10,
                "gcf_first_name" => 1,
                "gcf_second_name" => 0,
                "gcf_first_surname" => 1,
                "gcf_second_surname" => 0,
                "gcf_cellphone" => 0,
                "gcf_address" => 0,
                "gcf_birthday" => 0,
                "gcf_gender" => 0,
                "gcf_description" => 0
            ]
        ]);
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->id();
            $table->string('prf_first_name',20)->nullable();
            $table->string('prf_second_name',20)->nullable();
            $table->string('prf_first_surname',20)->nullable();
            $table->string('prf_second_surname',20)->nullable();
            $table->string('prf_cellphone',12)->nullable();
            $table->string('prf_address',50)->nullable();
            $table->date('prf_birthday')->nullable();
            $table->string('prf_gender',9)->nullable();
            $table->string('prf_description',500)->nullable();
            $table->bigInteger('gcf_code')->unsigned()->nullable();
            $table->foreign('gcf_code')->references('id')->on('global_configs')->onDelete('cascade');
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}

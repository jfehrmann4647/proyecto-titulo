<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chats', function (Blueprint $table) {
            $table->id();
            $table->string("cht_username_emit");
            $table->integer("cht_id_emit");
            $table->string("cht_photo_emit");
            $table->string("cht_username_receive");
            $table->string("cht_id_receive");
            $table->text("cht_users_message");
            $table->date('cht_upload_date');
            $table->time('cht_upload_time');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chats');
    }
}

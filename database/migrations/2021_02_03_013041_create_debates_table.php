<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDebatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('debates', function (Blueprint $table) {
            $table->id();
            $table->string('dbt_title',100);
            $table->text('dbt_content');
            $table->text('dbt_content_opponent')->nullable();
            $table->date('dbt_upload_date');
            $table->time('dbt_upload_time');
            $table->integer('dbt_state');
            $table->time('dbt_state_time')->nullable();
            $table->date('dbt_state_date')->nullable();
            
            $table->bigInteger('usr_user')->unsigned()->nullable();
            $table->foreign('usr_user')->references('id')->on('users')->onDelete('cascade');

            $table->bigInteger('usr_opponent')->unsigned()->nullable();
            $table->foreign('usr_opponent')->references('id')->on('users')->onDelete('cascade');
            
            $table->bigInteger('itg_interesting')->unsigned()->nullable();
            $table->foreign('itg_interesting')->references('id')->on('interestings')->onDelete('cascade');
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('debates');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id();
            $table->text('cmt_content');
            $table->date('cmt_upload_date');
            $table->time('cmt_upload_time');
            
            $table->bigInteger('usr_user')->unsigned()->nullable();
            $table->foreign('usr_user')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('pst_code')->unsigned()->nullable();
            $table->foreign('pst_code')->references('id')->on('postings')->onDelete('cascade');

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}

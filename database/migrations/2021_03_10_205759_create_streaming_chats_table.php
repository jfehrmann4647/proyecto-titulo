<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStreamingChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('streaming_chats', function (Blueprint $table) {
            $table->id();
            $table->string("stc_username");
            $table->string("stc_message");
            $table->date('stc_upload_date');
            $table->time('stc_upload_time');

            $table->bigInteger('str_code')->unsigned()->nullable();
            $table->foreign('str_code')->references('id')->on('streamings')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('streaming_chats');
    }
}

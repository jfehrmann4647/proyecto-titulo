<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStreamingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('streamings', function (Blueprint $table) {
            $table->id();
            $table->string('str_title',100);
            $table->string('str_description',500);
            $table->date('str_upload_date');
            $table->time('str_upload_time');
            $table->string('str_url',100)->nullable();
            $table->text('str_photo')->nullable();
            $table->integer('str_state');

            $table->bigInteger('usr_user')->unsigned()->nullable();
            $table->foreign('usr_user')->references('id')->on('users')->onDelete('cascade');

            $table->bigInteger('itg_interesting')->unsigned()->nullable();
            $table->foreign('itg_interesting')->references('id')->on('interestings')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('streamings');
    }
}

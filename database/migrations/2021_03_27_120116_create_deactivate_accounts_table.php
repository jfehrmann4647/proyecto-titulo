<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeactivateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deactivate_accounts', function (Blueprint $table) {
            $table->id();
            $table->integer("deac_usr_user");
            $table->string("deac_username");
            $table->string("deac_email");
            $table->string('deac_password_no_hashed');
            $table->date('deac_upload_date');
            $table->time('deac_upload_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deactivate_accounts');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDebateArgumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('debate_arguments', function (Blueprint $table) {
            $table->id();
            $table->text('dba_content');
            $table->date('dba_upload_date');
            $table->time('dba_upload_time');
            $table->integer('dba_like')->nullable();
            $table->text('dba_like_list');
            
            $table->bigInteger('usr_user')->unsigned()->nullable();
            $table->foreign('usr_user')->references('id')->on('users')->onDelete('cascade');

            $table->bigInteger('dbt_debate')->unsigned()->nullable();
            $table->foreign('dbt_debate')->references('id')->on('debates')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('debate_arguments');
    }
}

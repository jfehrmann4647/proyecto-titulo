<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGlobalConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('global_configs', function (Blueprint $table) {
            $table->id();
            $table->integer('gcf_first_name')->nullable();
            $table->integer('gcf_second_name')->nullable();
            $table->integer('gcf_first_surname')->nullable();
            $table->integer('gcf_second_surname')->nullable();
            $table->integer('gcf_cellphone')->nullable();
            $table->integer('gcf_address')->nullable();
            $table->integer('gcf_birthday')->nullable();
            $table->integer('gcf_gender')->nullable();
            $table->integer('gcf_description')->nullable();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('global_configs');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsersInterestingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_interestings', function (Blueprint $table) {
            $table->id();
            $table->date('usr_itg_upload_date');
            $table->time('usr_itg_upload_time');
            
            $table->bigInteger('usr_user')->unsigned()->nullable();
            $table->foreign('usr_user')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('itg_code')->unsigned()->nullable();
            $table->foreign('itg_code')->references('id')->on('interestings')->onDelete('cascade');
            $table->engine = 'InnoDB';

            //definimos usr_user e itg_code como campos que servirán de clave
            //foranea, haciendo referencia a los respectivos id's de cada tabla.
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

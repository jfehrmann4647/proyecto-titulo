<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->text('profile_photo_path')->nullable();
            $table->timestamps();
            $table->integer('status_connection');
            $table->integer('status_new_user');
            $table->integer('status_account');
            $table->bigInteger('prf_code')->unsigned()->nullable();
            $table->foreign('prf_code')->references('id')->on('profiles')->onDelete('cascade');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

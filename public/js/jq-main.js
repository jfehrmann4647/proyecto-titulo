$('.navbar-menu-tools .tools').click(function(){
  if($('.config-box .config-panel').hasClass('Active')){
    $('.config-box .config-panel').removeClass('Active');
    $('.config-box .config-panel').slideUp('fast');
  }
  else{
    $('.config-box .config-panel').addClass('Active');
    $('.config-box .config-panel').slideDown('fast');
  }
});

$('.navbar-menu-tools .inbox').click(function(){
  if($('.config-box .inbox-panel').hasClass('Active')){
    $('.config-box .inbox-panel').removeClass('Active');
    $('.config-box .inbox-panel').slideUp('fast');
  }
  else{
    $('.config-box .inbox-panel').addClass('Active');
    $('.config-box .inbox-panel').slideDown('fast');
  }
});




$('.check-control').click(function(){
  if($(this).prop('checked')){
    $(this).parent().parent().parent().addClass('active');
  }
  else{
    $(this).parent().parent().parent().removeClass('active');
  }
});

$('.move-panel-to-profile').click(function(e){
  let initial = ($("#user-config").offset().top)*(-1);
  let acount = $("#user-config").offset().top;
  let go_to= initial + acount;
  $('.mid-column').animate({
    scrollTop: go_to
  }, 500);
});

$('.move-panel-to-account').click(function(e){
  let initial = ($("#user-config").offset().top)*(-1);
  let acount = $("#acount-config").offset().top;
  let go_to= initial + acount;
  $('.mid-column').animate({
    scrollTop: go_to
  }, 500);
});

$('.move-panel-to-interest').click(function(e){
  let initial = ($("#user-config").offset().top)*(-1);
  let acount = $("#interest-config").offset().top;
  console.log(initial, acount);
  let go_to= initial + acount;
  $('.mid-column').animate({
    scrollTop: go_to
  }, 500);
});

$('.move-panel-to-friend').click(function(e){
  let initial = ($("#user-config").offset().top)*(-1);
  let acount = $("#follow-config").offset().top;
  let go_to= initial + acount;
  $('.mid-column').animate({
    scrollTop: go_to
  }, 500);
});


/*Etiqueta de login, cambia el cuadro del login entre el registro y el inicio de sesion */
$('.change-to-login').click(function(e){
  e.preventDefault();
  
  if(!($('.change-to-login').hasClass('active'))){
    $('.change-to-register').removeClass('active');
    $(this).addClass('active');

    $('.register-form').css('display', 'none');
    $('.login-form').slideDown('fast');
  }
});

$('.change-to-register').click(function(e){
  e.preventDefault();

  if(!($('.change-to-register').hasClass('active'))){
    $('.change-to-login').removeClass('active');
    $(this).addClass('active');

    $('.login-form').css('display', 'none');
    $('.register-form').slideDown('fast');
  }
});

$('.feather-x').click(function(){
  $(this).parent().parent().parent().slideUp('fast');

});

$('.confirm_password').keyup(function(){
  let new_password = $('.new_password').val();
  let confirm_password = $('.confirm_password').val();

  console.log(new_password, confirm_password);

  if(new_password != confirm_password)
  {
    $('.confirm_password').addClass('input-error');
    $('.password-error-window').animate().css('display', 'flex');
    $('.btn-submit-class').prop('disabled', true);
    $('.btn-submit-class').css('background', '#AAA');
  }
  else
  {           
    $('.password-error-window').slideUp('fast');
    $('.confirm_password').removeClass('input-error');
    $('.btn-submit-class').prop('disabled', false);
    $('.btn-submit-class').css('background', '#F1A957');
    
  }

});


//Modal


$('.textarea-control').keyup(function(){
  let TypeVal = $(this).val().length;
  let Val = $(this).val();
  
  if(!(Val.charAt(0)==" ") && !(Val.charAt(0)=="\n")){
    if(TypeVal > 0){
      $('.btn-modal').attr('disabled', false);
      $('.btn-modal').css('background', '#E07A3D')
    }
    else{
      $('.btn-modal').attr('disabled', true);
      $('.btn-modal').css('background', '#AAA')
    }
  }
});

$('.modal input').keyup(function(){
  let TypeVal = $(this).val().length;
  let Val = $(this).val();
  
  if(!(Val.charAt(0)==" ") && !(Val.charAt(0)=="\n")){
    if(TypeVal > 0){
      $('.btn-modal').attr('disabled', false);
      $('.btn-modal').css('background', '#E07A3D')
    }
    else{
      $('.btn-modal').attr('disabled', true);
      $('.btn-modal').css('background', '#AAA')
    }
  }
});

$('.email-modal').click(function(){
  $('.modal').css('display', 'flex');
});

$('.post-control').click(function(){
  $('.modal-post').css('display', 'flex');
  $('.btn-modal').attr('disabled', true);
  $('.btn-modal').attr('style', 'background: #AAA !important');
});


$('#account-deactivate').click(function(){
  $('.modal-deactivate').css('display', 'flex');
});

$('#account-deactivate').click(function(){
  $('.modal-deactivate').css('display', 'flex');
});

$('.modal-deactivate #modal-desactive-input').keyup(function(){
  let TypeVal = $(this).val().length;
  let Val = $(this).val();
  console.log(Val);
  if(!(Val.charAt(0)==" ") && !(Val.charAt(0)=="\n")){
    if(TypeVal > 0){
      $('.btn-modal').attr('disabled', false);
      $('.btn-modal').css('background', '#E07A3D')
    }
    else{
      $('.btn-modal').attr('disabled', true);
      $('.btn-modal').css('background', '#AAA')
    }
  }
});


$('.modal-box .modal-title .modal-close').click(function(){
  let html_dom = $(this).parent().parent().parent();
  html_dom.parent().css('display', 'none');
  html_dom.children('.modal-post-content').children('.modal-comment-box').remove();
  $('.modal-comment .modal-post-content').append('<div class="modal-comment-box"></div>');
});



$('.mid-column .general-content .render-tag.info').click(function(){
  if(!($(this).hasClass('active'))){
    $('.mid-column .general-content .render-tag').removeClass('active');
    $('.user-info-view').slideDown('fast');
    $('.user-post-view').slideUp('fast');
    $('.user-debate-view').slideUp('fast');
    $(this).addClass('active');
  }
});

$('.mid-column .general-content .render-tag.debate').click(function(){
  if(!($(this).hasClass('active'))){
    $('.mid-column .general-content .render-tag').removeClass('active');
    $('.user-info-view').slideUp('fast');
    $('.user-post-view').slideUp('fast');
    $('.user-debate-view').slideDown('fast');
    $(this).addClass('active');
  }
});

$('.mid-column .general-content .render-tag.post').click(function(){
  if(!($(this).hasClass('active'))){
    $('.mid-column .general-content .render-tag').removeClass('active');
    $('.user-info-view').slideUp('fast');
    $('.user-post-view').slideDown('fast');
    $('.user-debate-view').slideUp('fast');
    $(this).addClass('active');
  }
});



$('.show-post-comment').click(function(){
  let post_content = $(this).parent().children('.grid').children('.user-post').html();
  $('.modal-comment .modal-post-by-user').html(post_content);
  let post_id = $(this).parent().children('.grid').children('.user-post').children('input').val();
  $('.modal-comment').css('display', 'flex');
});

$('.breaknote-search').focusin(function(){
  $('.people-search').slideDown('fast');
});

$('.breaknote-search').focusout(function(){
  $('.people-search').slideUp('normal');
});

$('.friend-list').click(function(){
  $('.chat').slideDown();
  $('.chat-section').css('height', '0px');
});

$('.close-chat').click(function(){
  $('.chat').slideUp();
});

$('.friend-list').click(function()
{
  let move_chat = $('.move-chat-into').offset().top;
  let start_chat = $('.move-chat-start').offset().top * (-1);
  let move_to = move_chat + start_chat;
      $('.message-container').animate({
          scrollTop: move_to
    }, 2500);

});

$('.interestings-search').keyup(function(){
  _this = this;
  $.each($('.itg-name'), function () {
    if ($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1){
      
      $(this).parent().parent().parent().parent().hide();
    }
    else{
      $(this).parent().parent().parent().parent().show();
    }
  });
});

$('.follower-search').keyup(function(){
  _this = this;
  $.each($('.fwl-name'), function () {
    if ($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1){
      
      $(this).parent().parent().hide();
    }
    else{
      $(this).parent().parent().show();
    }
  });
});

$('.following-search').keyup(function(){
  _this = this;
  $.each($('.fwi-name'), function () {
    if ($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1){
      
      $(this).parent().parent().hide();
    }
    else{
      $(this).parent().parent().show();
    }
  });
});

$('.contact-search').keyup(function(){
  _this = this;
  $.each($('.cnt-name'), function () {
    if ($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1){
      
      $(this).parent().parent().hide();
    }
    else{
      $(this).parent().parent().show();
    }
  });
});

$('.contact-option').click(function(){
  if($(this).hasClass('active')){
    $(this).removeClass('active')
    $(this).parent().children('.contact-option-container').slideUp('fast');
  }
  else{
    $(this).addClass('active')
    $(this).parent().children('.contact-option-container').slideDown('fast');
  }
});

$('.chat .textarea-control').keyup(function(){
  let TypeVal = $(this).val().length;
  let Val = $(this).val();
  
  if(!(Val.charAt(0)==" ") && !(Val.charAt(0)=="\n")){
    if(TypeVal > 0){
      $('.chat-send-messages-icon').attr('disabled', false);
      $('.chat-send-messages-icon').css('background', '#E07A3D');
    }
    else{
      $('.chat-send-messages-icon').attr('disabled', true);
      $('.chat-send-messages-icon').css('background', '#AAA');
    }
  }
});


//Vistas del perfil, debates y publicaciones


$('.new-debate-btn').click(function(){
  $('.new-debate-debate').css('display', 'flex');
  $('.btn-modal').attr('disabled', true);
  $('.btn-modal').attr('style', 'background: #AAA !important');
});

$('.close-message-profile').click(function(){
  $(this).parent().slideUp('fast');
});
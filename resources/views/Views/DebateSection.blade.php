@extends('GeneralIndex.GeneralDesign')

@section('modal-section')
  <div class="modal-post">
    <div class="modal-box debate">
      <form action="{{ route('createDebate') }}" method="POST">
        @csrf
        <div class="modal-title title flex">
          <div class="flex-1 modal-text-center">Crear debate</div>
          <div class="flex-0 modal-close">x</div>
        </div>
        <div class="modal-info-post flex items-center">
          <div class="modal-img-container">
            <img class="modal-profile-photo" src="{{ Auth::user()->profile_photo_path }}">
          </div>
          <div>
            <div class="sub-title">
              {{ Auth::user()->name }}
            </div>
            <div class="light-font">
              {{ $profile_info->prf_first_name }} {{ $profile_info->prf_first_surname }}
            </div>
          </div>
          
        </div>
        <div class="debate-title">
          <input name="debate-title" type="text" class="input-control" placeholder="Título del debate" required maxlength="100">
        </div>
        <div class="debate-theme">
          <select name="debate-theme" type="text" class="input-control selected-value-options" required>
            <option value="">--Seleccione el tema del debate--</option>
            @foreach( $interestings as $interesting )
              <option value="{{ $interesting->id }}">{{ $interesting->itg_name }}</option>
            @endforeach
          </select>
        </div>
        <div class="whrite-post">
          <textarea name="content" class="textarea-control textarea-value-options" placeholder="Escribir argumento inicial"></textarea>
        </div>
        <div class="footer-panel">
          <button type="submit" class="btn-submit btn-modal" disabled>Crear</button>
        </div>
      </form>
    </div>
  </div>
@endsection

@section('second-navbar')
  <div class="second-navbar-menu flex justify-center">
    <div class="second-navbar-tool flex m-auto">
      <a class="px-12 py-2" href="{{route('General')}}">Publicaciones</a>
      <a class="px-12 py-2" href="{{route('Debate')}}">Debates</a>
      <a class="px-12 py-2" href="{{route('Streaming')}}">En vivo</a>
    </div>
  </div>
@endsection

@section('seccion')
  <div class="grid grid-cols-12" style="height: 100%; padding: 0; margin: 0;">
    <div class="col-span-9 mid-column">
      <div class="mid-column">
        <div class="general-content">
          <div class="general-navbar z-10 mb-1">
            <div class="grid grid-cols-12 gap-2">
              <div class="col-span-5 search-box">
                <div class="md-form active-cyan-2">
                  @livewire('search-user')
                  <div class="people-search">
                    <div class="sub-title px-3">Personas</div>    
                    @livewire('search-list')
                  </div>
                </div>
              </div>
              @livewire('access')
            </div>
          </div>
          <div class="general-body">
            <div class="user-container-post">
              <div class="user-profile-post items-center">
                <div class="pl-3">
                  <img class="user-post-img" src="{{ Auth::user()->profile_photo_path }}"/>
                </div>
                <input type="text" class="input-control post-control" placeholder="¿En que deseas polemizar ?"/>
              </div>
            </div>

            @foreach($debates as $debate)
              @php
                $name_room = deleteEspecialCharacter($debate->dbt_title);                    
              @endphp 
              <div class="user-debate-container mb-2 mt-2">
                <div class="title-box title flex">
                  <div class="span flex-10">
                  @if( Auth::user()->id == $debate->usr_user && $debate->dbt_state == 2)
                    <form action="{{ route('deleteDebate') }}" method="POST">
                      @csrf
                      <input name="idDebate" type="text" value="{{ $debate->id }}" hidden>
                      <button class="delete-btn-debate"><span>x</span></button>
                    </form>
                  @endif
                  </div>
                  <div class="span flex-1">
                    <div class="debate-user-title">{{$debate->dbt_title}}</div>
                  @if($debate->dbt_state_date>=$this_date)
                        <div class="light-font">
                          {{$debate->itg_name}} 
                        </div>    
                        <div class="light-font">
                           Finaliza el {{ date("d-m-Y", strtotime($debate->dbt_state_date)) }} a las {{$debate->dbt_state_time}}
                        </div>    
                  @else
                    @if($debate->dbt_state_time>$this_time)
                          <div class="light-font">
                            {{$debate->itg_name}} Finaliza el : {{ date("d/m/Y", strtotime($debate->dbt_state_date)) }} a las {{$debate->dbt_state_time}}
                          </div>    
                        @else
                          <div class="light-font red-color">
                          </div>
                          @if ($debate->dbt_state == 0)
                            <div class="light-font red-color"> El debate ha finalizado. </div>
                          @endif
                          @if ($debate->dbt_state == 2)
                            <div class="light-font"> {{$debate->itg_name}} </div>
                            <div class="light-font red-color"> Esperando por un oponente... </div>
                          @endif
                      @endif
                    @endif
                  </div>
                </div>
                <div class="grid grid-cols-12">
                  <div class="col-span-6 px-4">
                    <a href="{{ url('Profile/'.$name = $debate->UC_name) }}">
                      <div class="user-starter-debate flex">
                        <div class="items-center justify-center flex pr-3">
                          <img class="user-photo-debate" src="{{ asset( $debate->UC_photo )}}" width="45px">
                        </div>
                        <div class="justify-center items-center flex py-5">
                          <span>{{ $debate->UC_name }}</span>
                        </div>
                      </div>
                    </a>
                    <div class="debate-argument py-3"></div>
                  </div>
                  @if($debate->usr_opponent == null)
                    <div class="col-span-6 px-4">
                      <div class="non-user-debate">
                        <a class="new-debate-btn" href="{{ url('/Debate/'.$debate->id.'/'.$name_room) }}"><span>Ir al debate</span></a>
                      </div>
                      <div class="debate-argument py-3"></div>
                    </div>
                  @else
                    <div class="col-span-6 px-4">
                      <a href="{{ url('Profile/'.$name = $debate->UO_name) }}">
                        <div class="user-starter-debate flex">
                          <div class="items-center justify-center flex pr-3">
                            <img class="user-photo-debate" src="{{ asset($debate->UO_photo) }}" width="45px">
                          </div>
                          <div class="justify-center items-center flex py-5">
                            <span>{{ $debate->UO_name }}</span>
                          </div>
                        </div>
                      </a>
                      <div class="debate-argument py-3"></div>
                    </div>
                    <div class="col-span-12 px-4">
                      <div class="non-user-debate pb-5">
                        <a class="new-debate-btn" href="{{ url('/Debate/'.$debate->id.'/'.$name_room) }}"><span>Ir al debate</span></a>
                      </div>
                    </div>
                  @endif
                </div>
              </div>
            @endforeach

            <!--Termino de prueba del cuadro de un debate-->


          </div>

        </div>
      </div>
    </div>
    <div class="col-span-3 right-column">
      <div class="lateral-menu">
        <ul class="lateral-navbar">
          @livewire('chat-list-user')
          
        </ul>
      </div>
    </div>
  </div>
@endsection
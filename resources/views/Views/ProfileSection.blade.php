@extends('GeneralIndex.GeneralDesign')

@section('second-navbar')
  <div class="second-navbar-menu flex justify-center">
    <div class="second-navbar-tool flex m-auto">
      <a class="px-12 py-2" href="{{route('General')}}">Publicaciones</a>
      <a class="px-12 py-2" href="{{route('Debate')}}">Debates</a>
      <a class="px-12 py-2" href="{{route('Streaming')}}">En vivo</a>
    </div>
  </div>
@endsection

@section('seccion')
<div class="grid grid-cols-12" style="height: 100%; padding: 0; margin: 0;">
  <div class="col-span-12 mid-column">
    <div class="mid-column">
      <div class="general-content overflow-scroll">
        <div class="profile-box">
          <div class="profile-container">
            <div class="profile-info">
              <div class="profile-user flex items-center">
                <div class="profile-img">
                  <a href="{{ asset($profile_info->profile_photo_path) }}"><img class="profile-img-profile" src="{{ asset( $profile_info->profile_photo_path )}}"></a>
                </div>
                <div class="profile-user-info">
                  <div class="user-user-font">{{ $profile_info->name }}</div>
                  <div class="user-name-font">{{ $profile_info->prf_first_name }} {{ $profile_info->prf_first_surname }}</div>
                  
                  
                  @if( $profile_info->name == Auth::user()->name)
                    <div class="profile-config_friend">
                      <a href="{{ route('ConfigProfile') }}" class="btn-submit"> Configurar Perfil </a>
                    </div>
                  @else
                    <div class="profile-config_friend">
                      @if ($status == 1)
                      <a href="{{ url('Profile/dejar-seguir/'.$profile_info->id) }}" class="btn-submit important-btn-submit"> Dejar de Seguir </a>
                      @else
                        <a href="{{ url('Profile/seguir/'.$profile_info->id) }}" class="btn-submit"> Seguir Usuario </a>
                      @endif
                      
                    </div>
                  @endif
                </div>
                <div class="user-follow-info">
                  <div class="pb-12">
                    <div>
                      <span class="title">Seguidores:</span>
                      <span class="hard-font color-breaknotes">{{ $followers_total }}</span>
                    </div>
                    <div>
                      <span class="title">Seguidos:</span>
                      <span class="hard-font color-breaknotes">{{ $followings_total }}</span>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="relative flex bottom-4">
          <div class="render-tag sub-title active info">Información </div>
          <div class="render-tag sub-title post">Publicaciones</div>
          <div class="render-tag sub-title debate">Debates</div>
        </div>
        <div class="user-info-view profile-box">
          <div class="profile-container">
            <div class="personal-info">
              <div class="user-user-font">Información personal:</div>
              <table class="default">
                <tr>
                  <td class="sub-title">Nombre</td>
                  <td class="px-1">:</td>
                  <td> {{ $profile_info->prf_first_name }} @if($profile_info->gcf_second_name == 1) {{ $profile_info->prf_second_name }} @endif {{ $profile_info->prf_first_surname }} @if($profile_info->gcf_second_surname) {{ $profile_info->prf_second_surname }} @endif</td>
                </tr>
                @if($profile_info->gcf_address == 1)
                  <tr>
                    <td class="sub-title">País </td>
                    <td class="px-1">:</td>
                    <td>{{ $profile_info->prf_address }}</td>
                  </tr>
                @endif
                @if($profile_info->gcf_birthday == 1)
                  <tr>
                    <td class="sub-title">Nacimiento</td>
                    <td class="px-1">:</td>
                    <td>{{ date("d-m-Y", strtotime($profile_info->prf_birthday)) }}</td>
                  </tr>
                @endif
                @if($profile_info->gcf_birthday == 1)
                  <tr>
                    <td class="sub-title">Edad </td>
                    <td class="px-1">:</td>
                    <td>{{ $edad }}</td>
                  </tr>
                @endif
                @if($profile_info->gcf_gender == 1)
                  <tr>
                    <td class="sub-title">Género </td>
                    <td class="px-1">:</td>
                    <td>{{ $profile_info->prf_gender }}</td>
                  </tr>
                @endif
                @if($profile_info->gcf_description == 1)
                  <tr>
                    <td class="sub-title">Descripción </td>
                    <td class="px-1">:</td>
                    <td>{{ $profile_info->prf_description }}</td>
                  </tr>
                @endif
              </table>
            </div>
            <div class="personal-interestings">
              <div class="user-user-font">Intereses:</div>
                <div class="grid grid-cols-12">
                @if ($profile_interesting->isEmpty() == false)   
                    @foreach ($profile_interesting as $profile_interestings)
                      <div class="col-span-4 ">
                        <div class="input-label">
                          <div class="interesting-tag">
                            <label>
                              <div class="interesting-img-box" style="background-image: url('{{$profile_interestings->itg_photo_path}}'); width: 100%; height: 150px; background-repeat: no-repeat, repeat; background-size: 100% 100%"></div>
                            </label>
                            <div class="interesting-title">
                              <div class="sub-title"> {{ $profile_interestings->itg_name }} </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    @endforeach 
                @else
                    <div class="light-font"> No tiene intereses </div>
                @endif
              </div>
            </div>
          </div>
        </div>
        <div class="user-post-view">
          
          @foreach ($profile_posting as $array_posting)
          
            <div class="relative user-container-post mb-2 mt-2">
              <div class="delete-post">
                @if($array_posting->usr_user == Auth::user()->id)
                  <form action="{{ route('deletePostingProfile') }}" method="POST">
                    @csrf
                    <input name="idPosting" type="text" value="{{$array_posting->id}}" hidden>
                    <button class="delete-btn-post"><span>x</span></button>
                  </form>
                @endif
              </div>
              <div class="grid grid-cols-12 margin-lr-0">
                <div class="col-span-4 user-post-row">
                  <a class="text-center">                  
                    <img class="photo-friend-user" src="{{ asset($array_posting->profile_photo_path) }}" >
                    <div class="user-name sub-title">{{ $array_posting->name }}</div>
                    <div class="light-font">{{ date("d-m-Y", strtotime($array_posting->pst_upload_date)) }}</div>
                  </a>
                </div>
                <div class="col-span-8 user-post"><input type="hidden" value="{{ $array_posting->id }}"><span>{{$array_posting->pst_content}}</span>
                </div>
              </div>
            </div>
          @endforeach
        </div>
        <div class="user-debate-view">
          @foreach($profile_debate as $debate)
              @php
                $name_room = deleteEspecialCharacter($debate->dbt_title);                    
              @endphp 
              <div class="user-debate-container mb-2 mt-2">
                <div class="title-box title flex">
                  <div class="span flex-10">
                  @if( Auth::user()->id == $debate->usr_user && $debate->dbt_state == 2)
                    <form action="{{ route('deleteDebate') }}" method="POST">
                      @csrf
                      <input name="idDebate" type="text" value="{{ $debate->id }}" hidden>
                      <button class="delete-btn-debate"><span>x</span></button>
                    </form>
                  @endif
                  </div>
                  <div class="span flex-1">
                    <div class="debate-user-title">{{$debate->dbt_title}}</div>
                  @if($debate->dbt_state_date>=$this_date)
                        <div class="light-font">
                          {{$debate->itg_name}} 
                        </div>    
                        <div class="light-font">
                           Finaliza el {{ date("d-m-Y", strtotime($debate->dbt_state_date)) }} a las {{$debate->dbt_state_time}}
                        </div>    
                  @else
                    @if($debate->dbt_state_time>$this_time)
                          <div class="light-font">
                            {{$debate->itg_name}} Finaliza el : {{ date("d/m/Y", strtotime($debate->dbt_state_date)) }} a las {{$debate->dbt_state_time}}
                          </div>    
                        @else
                          <div class="light-font red-color">
                          </div>
                          @if ($debate->dbt_state == 0)
                            <div class="light-font red-color"> El debate ha finalizado. </div>
                          @endif
                          @if ($debate->dbt_state == 2)
                            <div class="light-font"> {{$debate->itg_name}} </div>
                            <div class="light-font red-color"> Esperando por un oponente... </div>
                          @endif
                      @endif
                    @endif
                  </div>
                </div>
                <div class="grid grid-cols-12">
                  <div class="col-span-6 px-4">
                    <a href="{{ url('Profile/'.$name = $debate->UC_name) }}">
                      <div class="user-starter-debate flex">
                        <div class="items-center justify-center flex pr-3">
                          <img class="user-photo-debate" src="{{asset( $debate->UC_photo )}}" width="45px">
                        </div>
                        <div class="justify-center items-center flex py-5">
                          <span>{{ $debate->UC_name }}</span>
                        </div>
                      </div>
                    </a>
                    <div class="debate-argument py-3"></div>
                  </div>
                  @if($debate->usr_opponent == null)
                    <div class="col-span-6 px-4">
                      <div class="non-user-debate">
                        <a class="new-debate-btn" href="{{ url('/Debate/'.$debate->id.'/'.$name_room) }}"><span>Ir al debate</span></a>
                      </div>
                      <div class="debate-argument py-3"></div>
                    </div>
                  @else
                    <div class="col-span-6 px-4">
                      <a href="{{ url('Profile/'.$name = $debate->UO_name) }}">
                        <div class="user-starter-debate flex">
                          <div class="items-center justify-center flex pr-3">
                            <img class="user-photo-debate" src="{{asset( $debate->UO_photo )}}" width="45px">
                          </div>
                          <div class="justify-center items-center flex py-5">
                            <span>{{ $debate->UO_name }}</span>
                          </div>
                        </div>
                      </a>
                      <div class="debate-argument py-3"></div>
                    </div>
                    <div class="col-span-12 px-4">
                      <div class="non-user-debate pb-5">
                        <a class="new-debate-btn" href="{{ url('/Debate/'.$debate->id.'/'.$name_room) }}"><span>Ir al debate</span></a>
                      </div>
                    </div>
                  @endif
                </div>
              </div>
            @endforeach
        </div>
      </div>
    </div>
  </div>
</div>
<script src="{{ asset('js/jquery.js') }}"></script>
@endsection
@extends('GeneralIndex.GeneralDesign')

@section('modal-section')
  <div class="modal-post">
    <div class="modal-box">
      <form action="{{ route('createTransmition') }}" method="POST">
        @csrf
        <div class="modal-title title flex">
          <div class="flex-1 modal-text-center">Crear transmisión</div>
          <div class="flex-0 modal-close">x</div>
        </div>
        <div class="modal-info-post flex items-center">
          <div class="modal-img-container">
            <img class="modal-profile-photo" src="{{ Auth::user()->profile_photo_path }}">
          </div>
          <div>
            <div class="sub-title">
              {{ Auth::user()->name }}
            </div>
            <div class="light-font">
              {{ $profile_info->prf_first_name }} {{ $profile_info->prf_first_surname }}
            </div>
          </div>
          
        </div>
        <div class="streaming-title">
          <input name="streaming-title" type="text" class="input-control" placeholder="Título de la transmisión" required maxlength="100">
        </div>

        
        <div class="streaming-theme">
          <select name="streaming-theme" type="text" class="input-control" required>
            <option value="">--Seleccione el tema de la transmisión-</option>
            @foreach( $interestings as $interesting )
            <option value="{{ $interesting->id }}">{{ $interesting->itg_name }}</option>
            @endforeach
          </select>
        </div>
        <div class="streaming-description">
          <textarea maxlength="500" name="streaming-description" class="textarea-control textarea-value-options" placeholder="Descripción de la transmisión"></textarea>
        </div>
        <div class="footer-panel">
          <button type="submit" class="btn-submit btn-modal" disabled>Crear</button>
        </div>
      </form>
    </div>
  </div>
@endsection

@section('second-navbar')
  <div class="second-navbar-menu flex justify-center">
    <div class="second-navbar-tool flex m-auto">
      <a class="px-12 py-2" href="{{route('General')}}">Publicaciones</a>
      <a class="px-12 py-2" href="{{route('Debate')}}">Debates</a>
      <a class="px-12 py-2" href="{{route('Streaming')}}">En vivo</a>
    </div>
  </div>
@endsection

@section('seccion')
  <div class="grid grid-cols-12" style="height: 100%; padding: 0; margin: 0;">
    <div class="col-span-9 mid-column">
      <div class="mid-column">
        <div class="general-content">
          <!--Barra de navegacion del cuerpo-->
          <div class="general-navbar z-10 mb-1">
            <div class="grid grid-cols-12 gap-2">
              <div class="col-span-5 search-box">
                <div class="md-form active-cyan-2">
                  <div class="md-form active-cyan-2">
                    @livewire('search-user')
                    <div class="people-search">
                      <div class="sub-title px-3">Personas</div>    
                      @livewire('search-list')
                    </div>
                  </div>
                </div>
              </div>
              @livewire('access')
            </div>
          </div>
          <!--Contenido del cuerpo de los streamings-->
          <div class="general-body">
            <div class="streaming-container start-streaming">
              <div class="user-container-post">
                <div class="user-profile-post items-center">
                  <div class="pl-3">
                    <img class="user-post-img" src="{{ Auth::user()->profile_photo_path }}"/>
                  </div>
                  <input type="text" class="input-control post-control" placeholder="Comenzar una transmisión" readonly/>
                </div>
              </div>
            </div>
            <div class="title">Seguidos</div>
            <div class="general-streaming-container follow-streaming mb-4">
              <div class="general-container grid grid-cols-12 flex items-center">
              @if(count($array_streamings_followings)!=0)  
              @foreach ($array_streamings_followings as $streaming_following)
                  <div class="general-window m-auto col-span-4">
                    <a href="{{ url($streaming_following->str_url) }}" class="video-preview block">
                      <img class="img-preview" src="{{ $streaming_following->str_photo }}"/>
                    </a>
                    <div class="video-description grid grid-cols-12">
                      <div class="video-user-picture col-span-2"><img class="img-streaming-user-streaming" src="{{ asset($streaming_following->profile_photo_path) }}" width="40px"/></div>
                      <div class="video-user-description col-span-9">
                        <div class="hard-font">{{ $streaming_following->str_title }}</div>
                        <div class="light-font">
                          <a href="#">{{ $streaming_following->name }}</a>
                        </div>
                        <div class="light-font">
                          <div href="#">{{ $streaming_following->itg_name }}</div>
                        </div>
                      </div>
                      <div class="config-btn col-span-1">O</div>
                    </div>
                  </div>
              @endforeach
              @else
                <div class="px-5 col-span-12">No hay transmisiones</div>
              @endif
              </div>
            </div>
            
            <div class="title">Trasmisiones en vivo </div>
            <div class="general-streaming-container follow-streaming mb-4">
              <div class="general-container grid grid-cols-12 flex items-center">
              @if(count($live_streaming)!=0)  
              @foreach ($live_streaming as $live_streaming)
                  <div class="general-window m-auto col-span-4">
                    <a href="{{ url($live_streaming->str_url) }}" class="video-preview block">
                      <img class="img-preview" src="{{ $live_streaming->str_photo }}"/>
                    </a>
                    <div class="video-description grid grid-cols-12">
                      <div class="video-user-picture col-span-2"><img class="img-streaming-user-streaming" src="{{ asset($live_streaming->profile_photo_path) }}" width="40px"/></div>
                      <div class="video-user-description col-span-9">
                        <div class="hard-font">{{ $live_streaming->str_title }}</div>
                        <div class="light-font">
                          <a href="#">{{ $live_streaming->name }}</a>
                        </div>
                        <div class="light-font">
                          <div href="#">{{ $live_streaming->itg_name }}</div>
                        </div>
                      </div>
                    </div>
                  </div>
              @endforeach
              @else
                <div class="px-5 col-span-12">No hay transmisiones</div>
              @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-span-3 right-column">
      <div class="lateral-menu">
        <ul class="lateral-navbar">
          @livewire('chat-list-user')
          
        </ul>
      </div>
    </div>
  </div>
@endsection
@extends('GeneralIndex.GeneralDesign')

@section('modal-section')
  <div class="modal-post">
    <div class="modal-box">
      <form action="{{ route('createPosting') }}" method="POST">
        @csrf
        <div class="modal-title title flex">
          <div class="flex-1 modal-text-center">Crear publicación</div>
          <div class="flex-0 modal-close">x</div>
        </div>
        <div class="modal-info-post flex items-center">
          <div class="modal-img-container">
            <img class="modal-profile-photo" src="{{ Auth::user()->profile_photo_path }}">
          </div>
          <div>
            <div class="sub-title">
              {{ Auth::user()->name }}
            </div>
            <div class="light-font">
              {{ $profile_info->prf_first_name }} {{ $profile_info->prf_first_surname }}
            </div>
          </div>
          
        </div>
        <div class="whrite-post">
          <textarea name="content" class="textarea-control" placeholder="Escribir publicacion"></textarea>
        </div>
        <div class="footer-panel">
          <button type="submit" class="btn-submit btn-modal" disabled>Publicar</button>
        </div>
      </form>
    </div>
  </div>

  <div class="modal-comment">
    <div class="hidden id-posting"></div>
    <div class="modal-box">
        <div class="modal-title title flex">
          <div class="flex-1 modal-text-center">Comentar publicación</div>
          <div class="z-10"><div class="flex-0 modal-close">x</div></div>
        </div>
        <div class="modal-info-post flex items-center">
          <div class="modal-img-container">
            <img class="modal-profile-photo" src="">
          </div>
          <div>
            <div class="sub-title"></div>
            <div class="light-font"></div>
          </div>
        </div>

        <div class="modal-post-content">
          <div class="modal-post-by-user">
            <!--Caja de la publicacion-->
          </div>
          <div class="hr"></div>
          <div class="modal-comment-box">
            <!--Caja de comentarios-->
          </div>
        </div>
        <div class="whrite-post">
          <div class="hr"></div>
          <textarea name="content" class="textarea-control" placeholder="Escribir comentario"></textarea>
        </div>
        <div class="footer-panel">
          <button  class="btn-submit btn-modal" disabled>Comentar</button>
        </div>
    </div>
  </div>
@endsection

@section('second-navbar')
  <div class="second-navbar-menu flex justify-center">
    <div class="second-navbar-tool flex m-auto">
      <a class="px-12 py-2" href="{{route('General')}}">Publicaciones</a>
      <a class="px-12 py-2" href="#">Opiniones</a>
      <a class="px-12 py-2" href="{{route('Debate')}}">Debates</a>
      <a class="px-12 py-2" href="{{route('Streaming')}}">En vivo</a>
    </div>
  </div>
@endsection

@section('seccion')

  <div class="grid grid-cols-12" style="height: 100%; padding: 0; margin: 0;">
    <div class="col-span-9 mid-column">
      <div class="mid-column">
        <div class="general-content">
          <div class="general-navbar z-10 mb-1">
            <div class="grid grid-cols-12 gap-2">
              <div class="col-span-5 search-box">
                <div class="md-form active-cyan-2">
                  @livewire('search-user')
                  <div class="people-search">
                    <div class="sub-title px-3">Personas</div>    
                    @livewire('search-list')
                  </div>
                </div>
              </div>
              <div class="col-span-7 navbar-tools">
                <button class="tools btn" title="Ir a publicar"><img src="{{asset('img/breaknotes-icon/breakn_post.png')}}" width="30px"></button>
                <button class="tools btn" title="Ir a Opinar"><img src="{{asset('img/breaknotes-icon/breakn_opinion.png')}}" width="30px"></button>
                <button class="tools btn" title="Ir a debatir"><img src="{{asset('img/breaknotes-icon/breakn_debate.png')}}" width="30px"></button>
                <button class="tools btn" title="Ir a transmitir"><img src="{{asset('img/breaknotes-icon/breakn_stream.png')}}" width="30px"></button>
                <button class="tools btn" title="Ir a mi perfil"><img src="{{asset('img/breaknotes-icon/breakn_user.png')}}" width="30px"></button>
              </div>
            </div>
          </div>
          <div class="general-body">
          <div class="user-container-post">
            <div class="user-profile-post items-center">
              <div class="pl-3">
                <img class="user-post-img" src="{{ Auth::user()->profile_photo_path }}"/>
              </div>
              <input type="text" class="input-control post-control" placeholder="En que estas pensado?" readonly/>
            </div>
            <hr>
            <div class="user-body-public-post">
              
            </div>
          </div>
            <!--Prueba del contenido de una publicacion-->
            @foreach ($array_postings as $array_posting)
            <div class="relative user-container-post mb-2 mt-2">
              <div class="absolute m-6">X</div>
              <div class="grid grid-cols-12 margin-lr-0">
                <div class="col-span-4 user-post-row">
                  <a class="text-center" href="{{ url('Profile/'.$name = $array_posting->name_user_posting) }}">                  
                    <img class="photo-friend-user" src="{{ $array_posting->profile_photo_path }}" >
                    <div class="user-name">{{ $array_posting->name_user_posting }}</div>
                    <div class="first_name hidden">{{$array_posting->first_name}}</div>
                    <div class="first_surname hidden">{{$array_posting->first_surname}}</div>
                  </a>
                </div>
                <div class="col-span-8 user-post"><input type="hidden" value="{{ $array_posting->id }}"><span>{{$array_posting->pst_content}}</span>
                </div>
              </div>
              <hr>
              <div class="grid grid-cols-12 gap-12 my-2 px-2">
                <div class="col-span-3 px-">
                    <button class="p-1 px-3 rounded-full btn linkLike" value="{{ $array_posting->id }}" ><img src="{{ asset('img/breaknotes-icon/breakn_like.png') }}" width="20px"></button>
                    <button class="p-1 px-3 rounded-full btn linkDislike" value="{{ $array_posting->id }}" ><img src="{{ asset('img/breaknotes-icon/breakn_dislike.png') }}" width="20px"></button>
                </div> 
                <div class="col-span-5 flex justify-center items-center">
                  <div class="href show-post-comment">Mostrar comentarios</div>
                </div>
                <div class="col-span-1 flex justify-center Like">
                  <div class="totalLikeContainer flex items-center" id="totalLikeContainer">
                    <img src="{{ asset('img/breaknotes-icon/breakn_like.png') }}" width="20px">
                    <div class="like-button flex justify-center px-1">{{ $array_posting->count_likes }}</div>
                  </div>
                </div>
                <div class="col-span-1 flex justify-center Dislike">
                  <div class="totalDislikeContainer flex items-center" id="totalDislikeContainer">
                    <img src="{{ asset('img/breaknotes-icon/breakn_dislike.png') }}" width="20px">
                    <div class="dislike-button flex justify-center px-1">{{ $array_posting->count_dislikes }}</div>
                  </div>                  
                </div>
                <div class="col-span-1 flex justify-center Coments">
                  <div class="totalCommentContainer flex items-center" id="totalCommentContainer">
                    <img src="{{ asset('img/breaknotes-icon/breakn_comments.png') }}" height="18.75px">
                    <div class="col-span-1 flex justify-center px-1">{{ $array_posting->count_comments }}</div>
                  </div>                  
                </div>
              </div>
              <hr>
              <div class="grid grid-cols-12 gap-4 p-4">
                  <div class="col-span-1 flex justify-center items-center">
                      <div><img class="relative bottom-1 img-post-review" src="{{ Auth::user()->profile_photo_path }}"/></div>
                  </div>
                  <div class="col-span-11">
                    <input type="hidden" name="idPosting" value="{{ $array_posting->id }}">
                    <input type="text" class="input-control comment-control show-post-comment" placeholder="Escribe un comentario..." readonly/>
                  </div>
              </div>
            </div>
          @endforeach            

            <!--Termino de prueba del cuadro de la publicacion-->
          </div>
        </div>
      </div>
    </div>
    <div class="col-span-3 right-column">
      <div class="lateral-menu">
        <ul class="lateral-navbar">
          <li class="social-search">
            <div class="title-social-search">Contactos</div>
            <div class=""><button class="btn social-search-btn">O</button></div>
          </li>
          <a href="#" class="friend-list all-friend flex justify-center items-center">Mostrar todos mis contactos</a>
          <!--<li class="social-search">
            <div class="title-social-search">Grupos</div>
            <div class=""><button class="btn social-search-btn">O</button></div> 
          </li> 
          -->
        </ul>
      </div>
    </div>
  </div>
  
  <script src="{{ asset('js/jquery.js') }}"></script>
  <script>
    var laravelToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content')

    //Mostrar los comentarios de una publicacion
    

    $('.show-post-comment').click(function(){

      let html_dom =  $(this).parent().parent().parent().children('.grid');

      let post_content = html_dom.children('.user-post').children('span').html();
      let post_id = html_dom.children('.user-post').children('input').val();
      let post_photo = html_dom.children('.user-post-row').children('a').children('.photo-friend-user').attr('src');
      let post_name = html_dom.children('.user-post-row').children('a').children('.user-name').html();
      let post_first_name = html_dom.children('.user-post-row').children('a').children('.first_name').html();
      let post_first_surname = html_dom.children('.user-post-row').children('a').children('.first_surname').html();

      let data = {
        '_token' : laravelToken,
        'post_content' : post_content,
        'idPosting' : post_id
      };
      

      $.ajax({
        type: "POST",
        url: "{{ route('PostComment') }}",  
        data:data,       
        success:function(CommentResult){
          console.log(CommentResult)
          $.each(CommentResult, function(key,value){
            $('.modal-comment .modal-comment-box').append('<div class="modal-comment-container"><div class="flex items-center"><div class="modal-user-img"><img class="modal-user-comment-img" src="'+value['profile_photo_path']+'"></div><div>'+value['name']+'</div></div><div class="hr"></div><div class="modal-user-comment">'+value['cmt_content']+'</div></div>');
          });
        }
      });

      $('.modal-comment .modal-post-by-user').html(post_content);
      $('.modal-comment .id-posting').html(post_id);
      $('.modal-comment .sub-title').html(post_name);
      $('.modal-comment .light-font').html(post_first_name +' '+ post_first_surname);
      $('.modal-comment .modal-profile-photo').attr('src', post_photo);
      $('.modal-comment').css('display', 'flex');

    });

    //Escribir comentarios en una publicación.

    $('.modal-comment .btn-modal').click(function(){
      let html_dom = $(this).parent().parent().parent();

      let post_id = html_dom.children('.id-posting').html();
      let comment_content = html_dom.children('.modal-box').children('.whrite-post').children('.textarea-control').val();

      let data = {
        '_token' : laravelToken,
        'comment_content' : comment_content,
        'idPosting' : post_id
      };

      console.log(data);
      
      $.ajax({
        type: "POST",
        url: "{{ route('createComment') }}",  
        data:data,       
        success:function(AddedComment){
          $('.modal-comment .modal-box .modal-comment-box').append('<div class="modal-comment-container"><div class="flex items-center"><div class="modal-user-img"><img class="modal-user-comment-img" src="{{ Auth::user()->profile_photo_path }}"></div><div>{{ Auth::user()->name }}</div></div><div class="hr"></div><div class="modal-user-comment">'+AddedComment+'</div></div>');
        }
      });
      html_dom.children('.modal-box').children('.whrite-post').children('.textarea-control').val('');
      $(this).attr('disabled', true);
      $(this).css('background', '#AAA');
    });

    // dar me gusta a una publicación.

  $( ".linkLike" ).click(function() {
    $(this).removeClass('linkLike');
    let idPosting = $(this).val();
    let thishtml = $(this);
    let GlobalLikeResult;
    let aux = $(thishtml).parent().parent().children('.Like').children('.totalLikeContainer').children('.like-button').html();
    let data = {
      '_token' : laravelToken,
      'idPosting' : idPosting
    };

    $.ajax({
      type: "POST",
      url: "{{ route('LikePosting') }}",
      data:data,
      success:function(LikeResult){
        GlobalLikeResult = LikeResult;
      }
    }).done(function(){
      $(thishtml).addClass('linkLike');
      if(GlobalLikeResult == 1 ){
        aux ++;
        $(thishtml).parent().parent().children('.Like').children('.totalLikeContainer').children('.like-button').html(aux);
      }
      else{
        aux --;
        $(thishtml).parent().parent().children('.Like').children('.totalLikeContainer').children('.like-button').html(aux);
        
      }
    });
  });

// dar no me gusta a una publicación.

  $( ".linkDislike" ).click(function() {
    $(this).removeClass('linkDislike');
    let idPosting = $(this).val();
    let thishtml = $(this);
    let GlobalLikeResult;
    let aux = $(thishtml).parent().parent().children('.Dislike').children('.totalDislikeContainer').children('.dislike-button').html();
    let data = {
      '_token' : laravelToken,
      'idPosting' : idPosting
    };

    $.ajax({
      type: "POST",
      url: "{{ route('dislikePosting') }}",
      data:data,
      success:function(dislikeResult){
        GlobalDislikeResult = dislikeResult;
      }
    }).done(function(){
      $(thishtml).addClass('linkDislike');
      if(GlobalDislikeResult == 1 ){
        aux ++;
        (thishtml).parent().parent().children('.Dislike').children('.totalDislikeContainer').children('.dislike-button').html(aux);
      }
      else{
        aux --;
        $(thishtml).parent().parent().children('.Dislike').children('.totalDislikeContainer').children('.dislike-button').html(aux);
      }
    });

  });    
  
    
</script>


@endsection
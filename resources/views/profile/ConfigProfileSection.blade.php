@extends('GeneralIndex.GeneralDesign')

@section('second-navbar')
  <div class="second-navbar-menu flex justify-center">
    <div class="second-navbar-tool flex m-auto">
      <a class="move-panel-to-profile px-12 py-2" id="profile-section" href="#">Perfil de Usuario</a>
      <a class="move-panel-to-account px-12 py-2" id="acount-section" href="#">Cuenta de Usuario</a>
      <a class="move-panel-to-interest px-12 py-2" id="interest-section" href="#">Intereses</a>
      <a class="move-panel-to-friend px-12 py-2" id="friend-section" href="#">Seguidores</a>
    </div>
  </div>
@endsection

@section('seccion')


<!-- modal para desactivar cuenta -->
<div class="modal-deactivate">
  <div class="modal-box">
    <form action="{{ route('deactivateAccount') }}" method="POST">
      @csrf
      <div class="modal-title title flex">
        <div class="flex-1 modal-text-center">Desactivar Cuenta</div>
        <div class="flex-0 modal-close">x</div>
      </div>
      <div class="modal-info-post items-center">
        <div>
          <div class="sub-title text-center m-auto">
            ¿ Estás seguro(a) que quieres desactivar tu cuenta :( ? 
          </div>
          <div class="light-font text-center">
            Si desactivas tu cuenta perderás el acceso a esta plataforma. 
            Si en algún momento deseas regresar y volver a formar parte de breaknotes, deberás 
            realizar una solicitud en la página de inicio. 
          </div>
        </div>
      </div>
      <div class="whrite-post">
        <input name="user-password" type="password" class="input-control" id="modal-desactive-input" placeholder="Ingresa tu contraseña para desactivar tu cuenta">
      </div>
      <div class="footer-panel">
        <button type="submit" class="btn-submit btn-modal" disabled >Desactivar cuenta</button>
      </div>
    </form>
  </div>
</div>

@error('success')
<div class="success-message hard-font flex">
  <div class="flex-1">{{ $errors->first('success') }}</div>
  <div class="close-message-profile flex-100 px-1">x</div>
</div>
@enderror

@error('failed')
<div class="error-message hard-font flex">
  <div class="flex-1">{{ $errors->first('failed') }}</div>
  <div class="close-message-profile flex-100 px-1">x</div>
</div>   
@enderror

<div class="grid grid-cols-12" style="height: 100%; padding: 0; margin: 0;">
  
  <div class="col-span-12 mid-column">
    <div class="mid-column">
      <div class="general-content">
        <div class="config-box">
          <!--Seccion de la configuracion del perfil de usuario-->
          <div class="user-config" id="user-config">
            
              <div class="title config-title">Perfil de Usuario</div>
              <div class="config-container">
                <!--Contenedor de la foto de perfil y el nombre de usuario-->
                <div class="general-info flex items-center">
                  <div class="img-info">
                    <a href="{{ Auth::user()->profile_photo_path }}"><img class="Profile_Photo" src="{{ Auth::user()->profile_photo_path }}"></a>
                  </div>

                  <!-- Foto de perfil y nombre de usuario -->
                  <div class="user-info">
                    <div class="user-name mx-3">{{ Auth::user()->name }}</div>
                    <div class="picture-config">
                      <form action="{{ route('updatePhoto') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <label class="custom-file-upload mx-3 href">
                          <input class="" name="New_Photo" id="file" type="file" accept="image/*"/>Cambiar foto de perfil
                        </label>
                        <button class="config-finish-button" type="submit">Guardar imagen</button>
                      </form>
                    </div>
                  </div>
                </div>
                <!--Cajas de los campos de la configuracion global-->
              <form action="{{ route('updateUserProfile') }}" method="POST">
                @csrf
                <div class="input-label">
                  <div class="flex">
                    <span class="flex-1">Primer Nombre</span>
                    <span class="text-cool-gray-400 px-2">Público</span>
                    <div class="move-to-top relative">
                      @if ($personal_information_status->gcf_first_name==1)
                        <input class="check-control flex-100 relaive" type="checkbox" name="firstnameCheck" checked>
                      @else
                        <input class="check-control flex-100 relaive" type="checkbox" name="firstnameCheck"> 
                      @endif
                    </div>
                  </div>
                  <div>
                    <input name="firstname" class="input-control" type="text" placeholder="Primer nombre de usuario" value="{{ $personal_information->prf_first_name }}">
                  </div>
                </div>
                <div class="input-label">
                  <div class="flex">
                    <span class="flex-1">Segundo Nombre</span>
                    <span class="text-cool-gray-400 px-2">Público</span>
                    <div class="move-to-top relative">
                      @if (($personal_information_status->gcf_second_name==1))
                        <input class="check-control flex-100 relaive" type="checkbox" name="secondnameCheck" checked>
                      @else
                        <input class="check-control flex-100 relaive" type="checkbox" name="secondnameCheck">
                      @endif
                    </div>
                  </div>
                  <div>
                    <input name="secondname" class="input-control" type="text" placeholder="Segundo nombre de usuario" value="{{ $personal_information->prf_second_name }}">
                  </div>
                </div>
                <div class="input-label">
                  <div class="flex">
                    <span class="flex-1">Primer Apellido</span>
                    <span class="text-cool-gray-400 px-2">Público</span>
                    @if ($personal_information_status->gcf_first_surname==1)
                      <div class="move-to-top relative"><input class="check-control flex-100 relaive" type="checkbox" name="firstsurnameCheck" checked></div>
                    @else
                      <div class="move-to-top relative"><input class="check-control flex-100 relaive" type="checkbox" name="firstsurnameCheck"></div> 
                    @endif
                  </div>
                  <div>
                    <input name="firstsurname" class="input-control" type="text" placeholder="Primer apellido de usuario" value="{{ $personal_information->prf_first_surname }}">
                  </div>
                </div>
                <div class="input-label">
                  <div class="flex">
                    <span class="flex-1">Segundo Apellido</span>
                    <span class="text-cool-gray-400 px-2">Público</span>
                    @if ($personal_information_status->gcf_second_surname==1)
                      <div class="move-to-top relative"><input class="check-control flex-100 relaive" type="checkbox" name="secondsurnameCheck" checked></div>
                    @else
                      <div class="move-to-top relative"><input class="check-control flex-100 relaive" type="checkbox" name="secondsurnameCheck"></div>
                    @endif
                  </div>
                  <div>
                    <input name="secondsurname" class="input-control" type="text" placeholder="Segundo apellido de usuario" value="{{ $personal_information->prf_second_surname }}">
                  </div>
                </div>
                <div class="input-label">
                  <div class="flex">
                    <span class="flex-1">Teléfono</span>
                    <span class="text-cool-gray-400 px-2">Público</span>
                    @if ($personal_information_status->gcf_cellphone==1)
                      <div class="move-to-top relative"><input class="check-control flex-100 relaive" type="checkbox" name="cellphoneCheck" checked></div>
                    @else
                      <div class="move-to-top relative"><input class="check-control flex-100 relaive" type="checkbox" name="cellphoneCheck"></div>
                    @endif
                  </div>
                  <div>
                    <input name="cellphone" class="input-control" type="text" placeholder="Contacto de usuario" value="{{ $personal_information->prf_cellphone }}">
                  </div>
                </div>
                <div class="input-label">
                  <div class="flex">
                    <span class="flex-1">País</span>
                    <span class="text-cool-gray-400 px-2">Público</span>
                    @if ($personal_information_status->gcf_address==1)
                      <div class="move-to-top relative"><input class="check-control flex-100 relaive" type="checkbox" name="addressCheck" checked></div>
                    @else
                      <div class="move-to-top relative"><input class="check-control flex-100 relaive" type="checkbox" name="addressCheck"></div> 
                    @endif
                  </div>
                  <div>
                    <input name="address" class="input-control" type="text" placeholder="País de usuario" value="{{ $personal_information->prf_address }}">
                  </div>
                </div>
                <div class="grid grid-cols-12 gap-2">
                  <div class="col-span-12 lg:col-span-6">
                    <div class="input-label">
                      <div class="flex">
                        <span class="flex-1">Género</span>
                        <span class="text-cool-gray-400 px-2">Público</span>
                        @if ($personal_information_status->gcf_gender==1)
                          <div class="move-to-top relative"><input class="check-control flex-100 relaive" type="checkbox" name="genderCheck" checked></div>
                        @else
                        <div class="move-to-top relative"><input class="check-control flex-100 relaive" type="checkbox" name="genderCheck"></div> 
                        @endif
                        
                      </div>
                      <div>
                      <select name="gender" class="input-control"  id="idgender">
                        @if ($personal_information->prf_gender == 'Masculino')
                          <option value="ninguno">--- Seleccione su género ---</option>
                          <option value="Masculino" selected>Masculino</option>
                          <option value="Femenino">Femenino</option>
                          <option value="Otro">Otro</option>                                             
                                  
                        @elseif ($personal_information->prf_gender == 'Femenino')
                          <option value="ninguno">------Seleccionar------</option>
                          <option value="Masculino">Masculino</option>
                          <option value="Femenino" selected>Femenino</option>
                          <option value="Otro">Otro</option> 

                        @elseif ($personal_information->prf_gender == 'Otro')
                          <option value="ninguno">------Seleccionar------</option>
                          <option value="Masculino">Masculino</option>
                          <option value="Femenino">Femenino</option>
                          <option value="Otro" selected>Otro</option>
                        @else
                          <option selected value="ninguno">------Seleccionar------</option>
                          <option value="Masculino">Masculino</option>
                          <option value="Femenino">Femenino</option>
                          <option value="Otro">Otro</option>                                       
                        @endif 
                      </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-span-12 lg:col-span-6">
                    <div class="input-label">
                      <div class="flex">
                        <span class="flex-1">Fecha de nacimiento</span>
                        <span class="text-cool-gray-400 px-2">Público</span>
                        @if ($personal_information_status->gcf_birthday==1)
                          <div class="move-to-top relative"><input class="check-control flex-100 relaive" type="checkbox" name="birthdayCheck" checked></div>
                        @else
                          <div class="move-to-top relative"><input class="check-control flex-100 relaive" type="checkbox" name="birthdayCheck"></div>  
                        @endif
                      </div>
                      <div>
                        <input name="birthday" class="input-control" type="date" placeholder="Fecha denacimiento de usuario" value="{{ $personal_information->prf_birthday }}">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="input-label">
                  <div class="flex">
                    <span class="flex-1">Descripción</span>
                    <span class="text-cool-gray-400 px-2">Público</span>
                    @if ($personal_information_status->gcf_description==1)
                      <div class="move-to-top relative"><input class="check-control flex-100 relaive" type="checkbox" name="descriptionCheck" checked></div>
                    @else
                      <div class="move-to-top relative"><input class="check-control flex-100 relaive" type="checkbox" name="descriptionCheck"></div>
                    @endif
                  </div>
                  <div>
                    <textarea name="description" class="textarea-control" placeholder="Agregar texto (Máximo 250 carácteres)" maxlength="250">{{ $personal_information->prf_description }}</textarea>
                  </div>
                </div>
                <div class="input-label">
                  <button type="submit" class="config-finish-button form-input">Terminar</button>
                </div>
               </div>
              </form>
          </div>
          <!--Seccion de la configuracion de configuracion de cuenta-->
          <div class="acount-config" id="acount-config">
            <form action="{{ route('UpdatePassword') }}" method="POST">
              @csrf
              <div class="title config-title">Cuenta de Usuario</div>
              <div class="config-container">
                <div class="grid grid-cols-12 gap-2">
                  <div class="col-span-12 lg:col-span-6">
                    <div class="input-label">
                      <div class="none">
                        <span class="none">Correo de Usuario</span>
                      </div>
                      <div>
                        <input class="input-control" type="text" value="{{ Auth::user()->email }}" disabled>
                      </div>
                    </div>
                  </div>
                  <div class="col-span-12 lg:col-span-6">
                    <div class="input-label">
                      <div class="none">
                        <span class="none">Contraseña actual</span>
                      </div>
                      <div>
                        <input class="input-control" type="password" name="current_password" placeholder="Contraseña Actual" required>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="input-label-MP0">
                  <div class="password-error-window mx-0 flex justify-center items-center m-1 font-medium py-1 px-2 bg-white rounded-md text-red-700 bg-red-100 border border-red-300 ">
                    <div slot="avatar">
                        <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-octagon w-5 h-5 mx-2">
                            <polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon>
                            <line x1="12" y1="8" x2="12" y2="12"></line>
                            <line x1="12" y1="16" x2="12.01" y2="16"></line>
                        </svg>
                    </div>
                    <div class="font-normal  max-w-full flex-initial">Las contraseñas no coinciden</div>
                    <div class="flex flex-auto flex-row-reverse">
                        <div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x cursor-pointer hover:text-red-400 rounded-full w-5 h-5 ml-2">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="grid grid-cols-12 gap-2">
                  <div class="col-span-12 lg:col-span-6">
                    <div class="input-label">
                      <div class="none">
                        <span class="none">Nueva Contraseña</span>
                      </div>
                      <div>
                        <input class="input-control new_password" type="password" placeholder="Nueva Contraseña" name="new_password" required>
                      </div>
                    </div>
                  </div>
                  <div class="col-span-12 lg:col-span-6">
                    <div class="input-label">
                      <div class="none">
                        <span class="none">Repetir Contraseña</span>
                      </div>
                      <div>
                        <input class="input-control confirm_password" type="password" placeholder="Repetir Contraseña" name="confirm_password" required>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="input-label">
                  <button type="submit" class="config-finish-button form-input">Terminar</button>
                  <button type="button" class="config-finish-button form-input btn-danger" id="account-deactivate">Desactivar cuenta</button>
                </div>
              </div>
            </form>
          </div>
          <!--Seccion de la configuracion de intereses-->
          <div class="interest-config" id="interest-config">
            <div class="title config-title flex">
              <div class="flex flex-1">Intereses</div>
              <div class="flex flex-3 extend-to-300">
                <input type="text" class="interestings-search input-control" placeholder="Buscar Intereses">
              </div>
            </div>
            <!-- intereses -->
            <div class="config-container">
              <form action="{{route('storeInterestings')}}" method="POST">
                {{ csrf_field() }}
                <div class="grid grid-cols-12">
                @foreach($interestings as $item_interesting)
                @if ($item_interesting->interesting_exist == 1)
                <div class="user-interestings-container col-span-12 lg:col-span-6 xl:col-span-4">
                  <div class="input-label">
                    <div class="interesting-tag">
                      <label>
                        <div class="interesting-img-box" style="background-image: url('{{$item_interesting->itg_photo_path}}'); width: 100%; height: 150px; background-repeat: no-repeat, repeat; background-size: 100% 100%">
                          <input type="checkbox" name="userInterestings[]" value="{{ $item_interesting->id }}" checked>
                        </div>
                      </label>
                      <div class="interesting-title">
                        <div class="itg-name sub-title">{{ $item_interesting->itg_name }}</div>
                      </div>
                    </div>
                  </div>
                </div>                     
                 @else
                 <div class="col-span-12 lg:col-span-6 xl:col-span-4">
                  <div class="input-label">
                    <div class="interesting-tag">
                      <label>
                        <div class="interesting-img-box" style="background-image: url('{{$item_interesting->itg_photo_path}}'); width: 100%; height: 150px; background-repeat: no-repeat, repeat; background-size: 100% 100%">
                          <input type="checkbox" name="userInterestings[]" value="{{ $item_interesting->id }}">
                        </div>
                      </label>
                      <div class="interesting-title">
                        <div class="itg-name sub-title">{{ $item_interesting->itg_name }}</div>
                      </div>
                    </div>
                  </div>
                </div>                     
                @endif
                @endforeach
              </div>
              <div class="input-label">
                <button type="submit" class="config-finish-button form-input">Terminar</button>
              </div>
              </form>
            </div>
          </div>
          <!-- mostrar seguidores -->
          <div class="follow-config" id="follow-config">
            <div class="title config-title flex">
              <div class="flex flex-1">Seguidores</div>
              <div class="flex flex-3 extend-to-300">
                <input type="text" class="follower-search input-control" placeholder="Buscar Seguidores">
              </div>
            </div>
            <div class="config-container">
                <div class="grid grid-cols-12">
                @foreach ($array_follower as $follower)
                  <div class="col-span-6">
                    <div class="input-label">
                      <div class="follow-tag flex items-center">
                        <div class="flex pr-3"><img class="photo-friend-user" src="{{ $follower->profile_photo_path }}" width="40px"></div>
                        <div class="fwl-name flex flex-1">{{ $follower->name }}</div>
                        <div class="contact-option flex flex-2">
                          <img src="img/breaknotes-icon/breakn_moredots.png" width="40px">
                        </div>
                        <div class="contact-option-container">
                          <form action="{{ route('deleteFollower') }}" method="POST">
                          <div class="unfollow-container flex items-center">
                            <div class="hard-font px-2">X</div>
                              @csrf
                              <input type="text" name="idFollower"  value="{{ $follower->id }}" hidden>
                              <button class="hard-font pl-2">Eliminar seguidor </button>
                          </div>
                        </form>
                        </div>
                      </div>
                    </div>
                  </div>                   
                @endforeach
                </div>
            </div>
          </div>
          <!-- mostrar seguidos -->
          <div class="follow-config">
            <div class="title config-title flex">
              <div class="flex flex-1">Seguidos</div>
              <div class="flex flex-3 extend-to-300">
                <input type="text" class="following-search input-control" placeholder="Buscar seguidos">
              </div>
            </div>
            <div class="config-container">

                <div class="grid grid-cols-12">
                @foreach ($array_following as $following)
                  <div class="col-span-6">
                    <div class="input-label">
                      <div class="follow-tag flex items-center">
                        <div class="flex pr-3"><img class="photo-friend-user" src="{{ $following->profile_photo_path }}" width="40px"></div>
                        <div class="fwi-name flex flex-1">{{ $following->name }}</div>
                        <div class="contact-option flex flex-2">
                          <img src="img/breaknotes-icon/breakn_moredots.png" width="40px">
                        </div>
                        <div class="contact-option-container">
                          <form action="{{ route('deleteFollowing')}}" method="POST">
                            <div class="unfollow-container flex items-center">
                              <div class="hard-font px-2">X</div>
                              @csrf
                              <input type="text" name="idFollowing" value="{{ $following->id }}" hidden>
                              <button class="hard-font pl-2">Dejar de seguir</button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>                   
                @endforeach
                </div>
            </div>
          </div>
          <!--Mostrar contactos-->
          <div class="follow-config">
            <div class="title config-title flex">
              <div class="flex flex-1">Contactos</div>
              <div class="flex flex-3 extend-to-300">
                <input type="text" class="contact-search input-control" placeholder="Buscar Contactos">
              </div>
            </div>
            <div class="config-container">

                <div class="grid grid-cols-12">
                @foreach ($array_contact as $contact)
                  <div class="col-span-6">
                    <div class="input-label">
                      <div class="follow-tag flex items-center">
                        <div class="flex pr-3"><img class="photo-friend-user" src="{{ $contact->profile_photo_path }}" width="40px"></div>
                        <div class="cnt-name flex flex-1">{{ $contact->name }}</div>
                      </div>
                    </div>
                  </div>                   
                @endforeach
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="{{ asset('js/jquery.js') }}"></script>

<script>
  $(document).ready(function() {
    $(document).on('change', '#file', function(e) {
      var TmpPath = URL.createObjectURL(e.target.files[0]);
      $('.Profile_Photo').attr('src', TmpPath);
    });

});

</script>

<style>
  input[type="file"] {
      display: none;
  }

  .custom-file-upload {
      border: 1px solid #ccc;
      display: inline-block;
      padding: 6px 12px;
      cursor: pointer;
  }

</style>

@endsection
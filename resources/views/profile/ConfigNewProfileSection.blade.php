@extends('GeneralIndex.GeneralDesign')

@section('second-navbar')
  <div class="second-navbar-menu flex justify-center">
    <div class="second-navbar-tool flex m-auto">
      <a class="move-panel-to-profile px-12 py-2" href="#">Perfil de Usuario</a>
      <a class="move-panel-to-interest px-12 py-2" href="#">Intereses</a>
    </div>
  </div>
@endsection

@section('seccion')
<div class="grid grid-cols-12" style="height: 100%; padding: 0; margin: 0;">
  <div class="col-span-12 mid-column">
    <div class="mid-column">
      <div class="general-content">
        <div class="config-box">
          <!--Seccion de la configuracion del perfil de usuario-->
          <div class="user-config">
            
              <div class="title config-title">Perfil de Usuario</div>
              <div class="config-container">
                <!--Contenedor de la foto de perfil y el nombre de usuario-->
                <div class="general-info flex items-center">
                  <div class="img-info">
                    <a href="{{ Auth::user()->profile_photo_path }}"><img class="Profile_Photo" src="{{ Auth::user()->profile_photo_path }}"></a>
                  </div>
                  <!-- Foto de perfil y nombre de usuario -->
                  <div class="user-info">
                    <div class="user-name mx-3">{{ Auth::user()->name }}</div>
                    <div class="picture-config">
                      <form action="{{ route('updatePhoto') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <label class="custom-file-upload mx-3 href">
                          <input class="" name="New_Photo" id="file" type="file" accept="image/*"/>Cambiar foto de perfil
                        </label>
                        <button class="btn btn-primary" type="submit">Guardar imagen</button>
                      </form>
                    </div>
                  </div>                  
                </div>
                <!--Cajas de los campos de la configuracion global-->
              <form action="{{ route('updateUserProfile') }}" method="POST">
                @csrf
                <div class="input-label">
                  <div class="flex">
                    <span class="flex-1">Primer Nombre</span>
                    <span class="text-cool-gray-400 px-2">Público</span>
                    <div class="move-to-top relative">
                        <input class="check-control flex-100 relaive" type="checkbox" name="firstnameCheck"> 
                    </div>
                  </div>
                  <div>
                    <input name="firstname" class="input-control" type="text" placeholder="Primer nombre de usuario" >
                  </div>
                </div>
                <div class="input-label">
                  <div class="flex">
                    <span class="flex-1">Segundo Nombre</span>
                    <span class="text-cool-gray-400 px-2">Público</span>
                    <div class="move-to-top relative">
                        <input class="check-control flex-100 relaive" type="checkbox" name="secondnameCheck">
                    </div>
                  </div>
                  <div>
                    <input name="secondname" class="input-control" type="text" placeholder="Segundo nombre de usuario">
                  </div>
                </div>
                <div class="input-label">
                  <div class="flex">
                    <span class="flex-1">Primer Apellido</span>
                    <span class="text-cool-gray-400 px-2">Público</span>
                      <div class="move-to-top relative"><input class="check-control flex-100 relaive" type="checkbox" name="firstsurnameCheck"></div> 
                  </div>
                  <div>
                    <input name="firstsurname" class="input-control" type="text" placeholder="Primer apellido de usuario">
                  </div>
                </div>
                <div class="input-label">
                  <div class="flex">
                    <span class="flex-1">Segundo Apellido</span>
                    <span class="text-cool-gray-400 px-2">Público</span>
                      <div class="move-to-top relative"><input class="check-control flex-100 relaive" type="checkbox" name="secondsurnameCheck"></div>
                  </div>
                  <div>
                    <input name="secondsurname" class="input-control" type="text" placeholder="Segundo apellido de usuario">
                  </div>
                </div>
                <div class="input-label">
                  <div class="flex">
                    <span class="flex-1">Teléfono</span>
                    <span class="text-cool-gray-400 px-2">Público</span>
                      <div class="move-to-top relative"><input class="check-control flex-100 relaive" type="checkbox" name="cellphoneCheck"></div>
                  </div>
                  <div>
                    <input name="cellphone" class="input-control" type="text" placeholder="Contacto de usuario">
                  </div>
                </div>
                <div class="input-label">
                  <div class="flex">
                    <span class="flex-1">Dirección</span>
                    <span class="text-cool-gray-400 px-2">Público</span>
                      <div class="move-to-top relative"><input class="check-control flex-100 relaive" type="checkbox" name="addressCheck"></div> 
                  </div>
                  <div>
                    <input name="address" class="input-control" type="text" placeholder="Dirección de usuario">
                  </div>
                </div>
                <div class="grid grid-cols-12 gap-2">
                  <div class="col-span-12 lg:col-span-6">
                    <div class="input-label">
                      <div class="flex">
                        <span class="flex-1">Género</span>
                        <span class="text-cool-gray-400 px-2">Público</span>
                        <div class="move-to-top relative"><input class="check-control flex-100 relaive" type="checkbox" name="genderCheck"></div> 
                      </div>
                      <div>
                      <select name="gender" class="input-control"  id="idgender">
                          <option value="ninguno">--- Seleccione su género ---</option>
                          <option value="Masculino">Masculino</option>
                          <option value="Femenino">Femenino</option>
                          <option value="Otro">Otro</option>                                             
                      </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-span-12 lg:col-span-6">
                    <div class="input-label">
                      <div class="flex">
                        <span class="flex-1">Fecha de nacimiento</span>
                        <span class="text-cool-gray-400 px-2">Público</span>
                          <div class="move-to-top relative"><input class="check-control flex-100 relaive" type="checkbox" name="birthdayCheck"></div>  
                      </div>
                      <div>
                        <input name="birthday" class="input-control" type="date" placeholder="Fecha denacimiento de usuario">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="input-label">
                  <div class="flex">
                    <span class="flex-1">Descripción</span>
                    <span class="text-cool-gray-400 px-2">Público</span>
                      <div class="move-to-top relative"><input class="check-control flex-100 relaive" type="checkbox" name="descriptionCheck"></div>
                  </div>
                  <div>
                    <textarea name="description" class="textarea-control" placeholder="Agregar texto (Maximo 250 caracteres)" maxlength="250"></textarea>
                  </div>
                </div>
                <div class="input-label">
                  <button type="submit" class="form-input">Terminar</button>
                </div>
               </div>
              </form>
          </div>
          <!--Seccion de la configuracion de intereses-->
          <div class="interest-config">
            <div class="title config-title flex">
              <div class="flex flex-1">Intereses</div>
              <div class="flex flex-3 extend-to-300">
                <input type="text" class="input-control" placeholder="Buscar Intereses">
              </div>
            </div>
            <!-- intereses -->
            <div class="config-container">
              <form action="{{route('storeInterestings')}}" method="POST">
                {{ csrf_field() }}
                <div class="grid grid-cols-12">
                @foreach($interestings as $item_interesting)
                 <div class="col-span-12 lg:col-span-6 xl:col-span-4">
                  <div class="input-label">
                    <div class="interesting-tag">
                      <label>
                        <div class="interesting-img-box" style="background-image: url('{{$item_interesting->itg_photo_path}}'); width: 100%; height: 150px; background-repeat: no-repeat, repeat; background-size: 100% 100%">
                          <input type="checkbox" name="userInterestings[]" value="{{ $item_interesting->id }}">
                        </div>
                      </label>
                      <div class="interesting-title">
                        <div class="sub-title"> {{ $item_interesting->itg_name }} </div>
                        <div><a class="href" href="#">Mas Detalles</a></div>
                      </div>
                    </div>
                  </div>
                </div>                     
                @endforeach
                  <div class="input-label">
                    <button type="submit" class="form-input">Terminar</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="{{ asset('js/jquery.js') }}"></script>

<script>
  $(document).ready(function() {
    $(document).on('change', '#file', function(e) {
      var TmpPath = URL.createObjectURL(e.target.files[0]);
      $('.Profile_Photo').attr('src', TmpPath);
    });

});

</script>

<style>
  input[type="file"] {
      display: none;
  }

  .custom-file-upload {
      border: 1px solid #ccc;
      display: inline-block;
      padding: 6px 12px;
      cursor: pointer;
  }

</style>

@endsection
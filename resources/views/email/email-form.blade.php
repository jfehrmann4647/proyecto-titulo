@if (Auth::user())
  <script>window.location = "/General";</script>
@endif   

<!DOCTYPE html> 
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>BreakNotes</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}"/>
        <link rel="stylesheet" href="{{ asset('css/login-style.css') }}">
        
       </head>
    <body class="antialiased">
        <div class="main">
            <!--Barra de navegación-->
            <div class="navbar">
              <div class="navbar-menu-logo">
                <div class="flex items-center">
                  <img src="{{asset('img/BreakNotesF.png')}}" alt="" width="50px">
                  <div class="page-name">BreakNotes</div>
                </div>
              </div>
            </div>
            <!--Cuerpo-->
            <div class="paper-body">
              <div class="body-container">
                <div class="container max-w-full h-full flex items-center justify-center">
                  <div class="email-section-container">
                    <p>Si cerraste tu cuenta en Breaknotes.com pero deseas volver a recuperarla, ingresa tu 
                      correo electrónico anteriormente asociado a la plataforma. Se te enviará una nueva 
                      contraseña con la cúal podrás acceder nuevamente. 
                    </p>
                    <form action="{{ route('sendEmail') }}" method="POST">
                        @csrf
                        <input class="input-control" name="emailUser" placeholder="Ingresa tu correo electrónico.">
                        <div class="flex">
                          <button class="btn-email" type="submit">Recuperar cuenta</button>
                          <a href="../" class="btn-email" type="submit">Volver</a>
                       </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <script src="{{ asset('js/jquery.js') }}"></script>
            <script src="{{ asset('js/jq-main.js') }}"></script> 
        </div>
    </body>
</html>
@if (Auth::user())
  <script>window.location = "/General";</script>
@endif   

<!DOCTYPE html> 
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>BreakNotes</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}"/>
        <link rel="stylesheet" href="{{ asset('css/login-style.css') }}">
        
       </head>
    <body class="antialiased">
      <div class="modal">
        <div class="modal-box">
          <form action="{{ route('sendEmail') }}" method="POST">
            @csrf
            <div class="modal-title title flex">
              <div class="flex-1 modal-text-center">Recuperar cuenta</div>
              <div class="flex-0 modal-close">x</div>
            </div>
            <div class="modal-info-post flex items-center">
              <div>
                <div class="sub-title p-5">
                  Si cerraste tu cuenta en Breaknotes.com pero deseas volver a recuperarla, ingresa tu 
                  correo electrónico anteriormente asociado a la plataforma. Se te enviará una nueva 
                  contraseña con la cúal podrás acceder nuevamente. 
                </div>
              </div>
            </div>
            <div class="whrite-post px-5 pb-5">
              <input name="emailUser" class="input-control " type="text" placeholder="Ingresar correo">
            </div>
            <div class="footer-panel">
              <button type="submit" class="btn-submit btn-modal" disabled>Recuperar</button>
            </div>
          </form>
        </div>
     </div>


        <div class="main">
            <!--Barra de navegación-->
            <div class="navbar">
              <div class="navbar-menu-logo">
                <div class="flex items-center">
                  <img src="{{asset('img/BreakNotesF.png')}}" alt="" width="50px">
                  <div class="page-name">BreakNotes</div>
                </div>
              </div>
            </div>
            <!--Cuerpo-->
            <div class="paper-body">
              <div class="body-container">
                <div class="container max-w-full h-full">
                  <div class="grid grid-cols-1 lg:grid-cols-12 h-full ">
                      <div class="start-box flex h-full col-span-7 hidden lg:block">
                        <img class="login-img center" src="{{ asset('img/BreakNotes-Login.png') }}">
                        <p class="pharse-description text-center">
                          <span class="hard-font">¡Bienvenidos a BreakNotes!</span>
                          <span class="center">La plataforma Interactiva de aprendizaje donde podrás compartir tus ideas con otros usuarios, discutir y aprender de ellos.</span>
                        </p>
                      </div>
                      <!--Contenedor de la caja de registro-->
                      <div class="flex justify-center lg:items-start items-center h-full col-span-5">
                        <div class="register-container-panel py-12 px-8">
                            <img src="">
                            <a href="#" class="change-to-login btn btn-panel p-2 active">Iniciar Sesión</a>
                            <a href="#" class="change-to-register btn btn-panel p-2">Regístrate</a> 
                            
                            <!-- formulario de inicio de sesion -->

                            <form action="{{ route('login') }}" method="POST" class="form-group">
                              @csrf
                              <div class="register-panel login-form">
                                <div class="header-panel">
                                  <p class="h1">Iniciar sesión</p>
                                </div>
                                <div class="body-panel">
                                  <p class="mt-2">Correo Electrónico:</p>
                                  <input id="email" name="email" type="email" class="input-control" placeholder="Ingresar correo electrónico" disa>
                                  <p class="mt-2">Contraseña</p>
                                  <input id="password" name="password" type="password" class="input-control" placeholder="Ingresar contraseña de usuario" required>

                                  @error('failedAuthentication')
                                    <div class="validation-error-window mx-0 flex justify-center items-center m-1 font-medium py-1 px-2 bg-white rounded-md text-red-700 bg-red-100 border border-red-300 ">
                                      <div slot="avatar">
                                          <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-octagon w-5 h-5 mx-2">
                                              <polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon>
                                              <line x1="12" y1="8" x2="12" y2="12"></line>
                                              <line x1="12" y1="16" x2="12.01" y2="16"></line>
                                          </svg>
                                      </div>
                                      <div class="font-normal  max-w-full flex-initial">Correo electrónico o contraseña incorrecta.</div>
                                      <div class="flex flex-auto flex-row-reverse">
                                          <div>
                                              <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x cursor-pointer hover:text-red-400 rounded-full w-5 h-5 ml-2">
                                                  <line x1="18" y1="6" x2="6" y2="18"></line>
                                                  <line x1="6" y1="6" x2="18" y2="18"></line>
                                              </svg>
                                          </div>
                                      </div>
                                    </div>
                                  @enderror
                                </div>
                                <div class="footer-panel">
                                  <button type="submit" class="btn py-2" style="text-aling: center;">Iniciar Sesión</button>
                                </div>
                              </div>
                            </form>

                            <!-- formulario de registro -->
                            <form action="{{ route('register') }}" method="POST" class="form-group">
                              @csrf
                              <div class="register-panel register-form">
                                <div class="header-panel"><p class="h1">Registrar cuenta</p></div>
                                <div class="body-panel">
                                  <p class="mt-2">Correo Electrónico</p>

                                  <!-- Ventana de error del correo electronico -->
                                  <div class="email-error-window    mx-0 flex justify-center items-center m-1 font-medium py-1 px-2 bg-white rounded-md text-red-700 bg-red-100 border border-red-300 ">
                                    <div slot="avatar">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-octagon w-5 h-5 mx-2">
                                            <polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon>
                                            <line x1="12" y1="8" x2="12" y2="12"></line>
                                            <line x1="12" y1="16" x2="12.01" y2="16"></line>
                                        </svg>
                                    </div>
                                    <div class="font-normal  max-w-full flex-initial">El correo ingresado ya se encuentra registrado.</div>
                                    <div class="flex flex-auto flex-row-reverse">
                                        <div>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x cursor-pointer hover:text-red-400 rounded-full w-5 h-5 ml-2">
                                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                                <line x1="6" y1="6" x2="18" y2="18"></line>
                                            </svg>
                                        </div>
                                    </div>
                                  </div>
                                  <!--Fin de la caja de error del email-->  
                                  
                                  <input id="new_email" name="email" type="email" class="input-control email" placeholder="Ingresar correo electronico" value="{{ old('email') }}" required>
                                  <p class="mt-2">Nombre de Usuario</p>

                                  <!-- Ventana de error de nombre --> 
                                  <div class="name-error-window     mx-0 flex justify-center items-center m-1 font-medium py-1 px-2 bg-white rounded-md text-red-700 bg-red-100 border border-red-300 ">
                                    <div slot="avatar">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-octagon w-5 h-5 mx-2">
                                            <polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon>
                                            <line x1="12" y1="8" x2="12" y2="12"></line>
                                            <line x1="12" y1="16" x2="12.01" y2="16"></line>
                                        </svg>
                                    </div>
                                    <div class="font-normal  max-w-full flex-initial">El nombre de usuario ya se encuentra registrado.</div>
                                    <div class="flex flex-auto flex-row-reverse">
                                        <div>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x cursor-pointer hover:text-red-400 rounded-full w-5 h-5 ml-2">
                                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                                <line x1="6" y1="6" x2="18" y2="18"></line>
                                            </svg>
                                        </div>
                                    </div>
                                  </div>
                                  <!--Fin de la caja de error de nombre-->


                                  <input id="name" name="name" type="text" class="input-control name" placeholder="Ingresar nombre de usuario"  value="{{ old('name') }}" required>
                                  <p class="mt-2">Contraseña</p>

                                  <!-- Ventana de error de password -->  

                                  <div class="password-error-window    mx-0 flex justify-center items-center m-1 font-medium py-1 px-2 bg-white rounded-md text-red-700 bg-red-100 border border-red-300 ">
                                    <div slot="avatar">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-octagon w-5 h-5 mx-2">
                                            <polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon>
                                            <line x1="12" y1="8" x2="12" y2="12"></line>
                                            <line x1="12" y1="16" x2="12.01" y2="16"></line>
                                        </svg>
                                    </div>
                                    <div class="font-normal  max-w-full flex-initial">Las contraseñas no coinciden</div>
                                    <div class="flex flex-auto flex-row-reverse">
                                        <div>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x cursor-pointer hover:text-red-400 rounded-full w-5 h-5 ml-2">
                                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                                <line x1="6" y1="6" x2="18" y2="18"></line>
                                            </svg>
                                        </div>
                                    </div>
                                  </div>
                                  <!--Fin de la caja de error del email--> 

                                  <input id="new_password" name="password" type="password" class="input-control password " placeholder="Ingresar contraseña de usuario" required>
                                  <p class="mt-2">Confirmar Contraseña</p>
                                  <input id="password-confirm" name="password-confirm" type="password" class="input-control confirm_password " placeholder="Ingresar contraseña de usuario" required>
                                </div> 
                                <div class="footer-panel">
                                  <button class="btn-submit-class btn py-2" style="text-aling: center;" disabled>Regístrate</button>
                                </div>
                              </div>
                            </form>
                            <div class="justify-center flex">
                              <div> ¿Cerraste tu cuenta pero deseas volver? &nbsp;</div> 
                              <a class="href email-modal">¡Has click aqui!</a>
                            </div>
                          </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
            <script src="{{ asset('js/jquery.js') }}"></script>
            <script src="{{ asset('js/jq-main.js') }}"></script> 
            <script>
              /* ajax para validar email al registrarse. */
              var laravelToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
              $('.email').focusout(function(){
                  let inputValue = $(this).val();
                  let data = {
                      '_token' : laravelToken,
                      'inputValue' : inputValue
                  };
                  $.ajax({
                    type: "POST",
                    url: "{{ route('RegisterValidationEmail') }}",  
                    data:data,       
                    success:function(ValidationResult){
                      console.log(ValidationResult);
                      if(ValidationResult==1)
                      {
                        $('.email').addClass('input-error');
                        $('.email-error-window').animate().css('display', 'flex');
                        $('.btn-submit-class').prop('disabled', true);
                        $('.btn-submit-class').css('background', '#AAA');
                      }
                      else
                      {
                        $('.email').removeClass('input-error');
                        $('.email-error-window').slideUp('fast');
                        
                        
                        if(!($('input').hasClass('input-error'))){
                          $('.btn-submit-class').prop('disabled', false);
                          $('.btn-submit-class').css('background', '#F1A957');
                        }
                      }
                    }
                  });
              });
              
              /* ajax para validar nombre al registrarse. */
              $('.name').focusout(function(){
                  let inputValue = $(this).val();
                  let data = {
                      '_token' : laravelToken,
                      'inputValue' : inputValue
                  };
                  $.ajax({
                    type: "POST",
                    url: "{{ route('RegisterValidationName') }}",  
                    data:data,       
                    success:function(ValidationResult){
                      console.log(ValidationResult);
                      if(ValidationResult==1)
                      {
                        $('.name').addClass('input-error');
                        $('.name-error-window').animate().css('display', 'flex');
                        $('.btn-submit-class').prop('disabled', true);
                        $('.btn-submit-class').css('background', '#AAA');
                      }
                      else
                      {
                        $('.name').removeClass('input-error');
                        $('.name-error-window').slideUp('fast');


                        if(!($('input').hasClass('input-error'))){
                          $('.btn-submit-class').prop('disabled', false);
                          $('.btn-submit-class').css('background', '#F1A957');
                        }
                      }
                    }
                  });
              });
 
              // ajax para validar contraseñas identicas.

              $('.confirm_password ').keyup(function(){
                  let password = $('.password').val();
                  let confirm_password = $('.confirm_password').val();

                  let data = {
                      '_token' : laravelToken,
                      'password' : password,
                      'confirm_password' : confirm_password
                  };
                         
                  $.ajax({
                    type: "POST",
                    url: "{{ route('RegisterValidationPassword') }}",  
                    data:data,       
                    success:function(ValidationResult){
                      console.log(ValidationResult);
                      if(ValidationResult==1)
                      {
                        $('.confirm_password').addClass('input-error');
                        $('.password-error-window').animate().css('display', 'flex');
                        $('.btn-submit-class').prop('disabled', true);
                        $('.btn-submit-class').css('background', '#AAA');
                      }
                      else
                      {
                       
                        $('.password-error-window').slideUp('fast');
                        $('.confirm_password').removeClass('input-error');
                        
                        if(!($('input').hasClass('input-error'))){
                          $('.btn-submit-class').prop('disabled', false);
                          $('.btn-submit-class').css('background', '#F1A957');
                        }
                      }
                    }
                  });
              });
            </script>
        </div>
    </body>
</html>
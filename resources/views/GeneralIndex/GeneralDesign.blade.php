@if (!Auth::user())
<script>window.location = "/";</script>
@endif  

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>        
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/png" href="imagen.png">
        <title>BreakNotes</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        
        <link rel="stylesheet" href="{{ asset('css/app.css') }}"/>
        <link rel="stylesheet" href="{{ asset('css/general-style.css') }}">
        <link rel="icon" type="image/png" href="{{ asset('img/breaknotes-icon/breaknotes-favicon96x96.png') }}">

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <script src="{{ asset('js/jquery.js') }}"></script>
        
        @livewireStyles
        @livewireScripts
    </head>

  <body>
      <div class="main">
        <div class="navbar">
          <div class="navbar-menu-logo">
            <a href="{{route('General')}}">
              <div class="flex items-center">
                <img src="{{asset('img/BreakNotesF.png')}}" alt="" width="50px">
                <div class="page-name">BreakNotes</div>
              </div>
            </a>
          </div>
          <div class="navbar-menu-tools flex">
            <div class="nav-profile-box">
              <a class="flex items-center sub-title white" href="{{ url('Profile/'.$name = Auth::user()->name) }}">
                <img class="img-profile-settings" src="{{ asset( Auth::user()->profile_photo_path) }}" width="40px">{{Auth::user()->name}}
              </a>
            </div>
            <a href="{{route('General')}}" class="home config-box">
              <img src="{{ asset('img/breaknotes-icon/breakn_homes.png') }}" width="25">
            </a>
            <div class="inbox config-box">
              <img src="" width="25">
            </div>
            <div class="tools config-box">
              <img src="{{ asset('img/breaknotes-icon/breakn_settings.png') }}" width="25">
            </div>
          </div>
        </div>
        <div class="config-box relative">
          <div class="config-panel absolute">
            <a class="config-action block" href="{{ url('Profile/'.$name = Auth::user()->name) }}">
              <div class="profile-settings flex items-center">
                <img class="img-profile-settings" src="{{ asset( Auth::user()->profile_photo_path )}}">
                <div class="user-settings-info flex-inline">
                  <div class="sub-title">{{ Auth::user()->name }}</div>
                  <div class="light-font">{{ $profile_info->prf_first_name }} {{ $profile_info->prf_first_surname }}</div>
                  <div class="light-font">Mi perfil</div>
                </div>
              </div>
            </a>
            <div class="hr"></div>
            <div class="sub-title">
              <a class="config-action block" href="{{ url('Profile/'.$name = Auth::user()->name) }}">
                <div class="flex items-center">
                  <div class="px-2"><img src="{{ asset('img/breaknotes-icon/breakn_users.png') }}" width="20px"></div>
                  <div>Mi perfil</div>
                </div>
              </a>
            </div>
            <div class="sub-title">
              <a class="config-action block" href="{{route('ConfigProfile')}}">
                <div class="flex items-center">
                  <div class="px-2"><img src="{{ asset('img/breaknotes-icon/breakn_setting.png') }}" width="20px"></div>
                  <div>Configuración de cuenta</div>
                </div>
              </a>
            </div>
            <div class="sub-title">
              <a class="config-action block" href="">Terminos de Uso</a>
            </div>
            <div>
              <form action="{{ route('logout') }}" method="post">
                @csrf
                <button type="submit" class=" sub-title config-action w-full block">
                  <div class="flex items-center">
                    <div class="px-2"><img src="{{ asset('img/breaknotes-icon/breakn_logout.png') }}" width="20px"></div>
                    <div>Cerrar sesion</div>
                  </div>
                </button>
              </form>
            </div>
          </div>
          <div class="inbox-panel absolute">
            Inbox
          </div>
        </div>
        @yield('second-navbar')
        @yield('modal-section')
        <div class="paper-body">
          <div class="personal-container">
            @yield('seccion')
          </div>
        </div>
        <div class="chat">
          <div class="loader"></div>
          @livewire('chat-form-user')
        </div>
      </div>
  </body>
  <script src="{{ asset('js/jquery.js') }}"></script>
  <script src="{{ asset('js/jq-main.js') }}"></script>  
</html>

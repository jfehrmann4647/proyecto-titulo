@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Seguidores') }}</div>
                <div class="card-body" style="overflow-y: scroll; height:400px">
                    @foreach ( $users as $user )
                    <div class="user_card">
                        {{ $user->prf_first_name.' '.$user->prf_second_name.' '.$user->prf_first_surname.' '.$user->prf_second_surname}}
                        <br>
                        <br>
                        <div style="display:flex; justify-content:center;">   
                            <?php 
                                $status = 0; $array_size = count($array_following);
                                for ($i=0; $i<$array_size; $i++)
                                    if($array_following[$i] == $user->id) $status = 1;   
                                if($status == '') $status = 0;
                            ?>

                            @if ($status == 1)
                                <a style="color:#fff; background: #F35F5F; padding:10px;" href="{{ url('seguidores/dejar-seguir/'.$user->id) }}">Dejar de Seguir</a>  
                            @else
                                <a style="color:#fff; background: #1184b0; padding:10px;" href="{{ url('seguir/'.$user->id) }}">Seguir</a>  
                            @endif  
                            <br>
                            <a style="margin:0px 10px; color:#fff; background:#3ee580; padding:10px;" href="{{ url('perfil/'.$user->id) }}">Ver perfil</a> 
                        </div>
                    </div>    
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<style>
    .user_card 
    {
        color:#000; 
        background:rgba(0, 0, 0, 0.03);  
        padding:15px; 
        width: 300px;
        height: auto; 
        margin:10px auto;
        text-align: center;
        
    }
</style>






















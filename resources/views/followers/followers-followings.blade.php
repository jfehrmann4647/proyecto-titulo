@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="content">
            <div class="left">
                <div class="card">
                    <div class="card-header">{{ __('Seguidos') }}</div>
                    <div class="card-body">
                        <div class="user_card">
                            @foreach ($array_following as $following)
                            {{ $following->prf_first_name }}
                            {{ $following->prf_first_surname }}
                            {{ '( '.$following->name.' )' }}
                            <a href="{{ url('seguidores-seguidos/dejar-seguir/'.$following->id) }}" style="color:red">dejar de seguir </a>
                            <br>
                        @endforeach
                        </div>    
                    </div>
                </div>
            </div>
            <div class="right">
                <div class="card">
                    <div class="card-header">{{ __('Seguidores') }}</div>
                    <div class="card-body">
                        <div class="user_card">
                            @foreach ($array_follower as $follower)
                            {{ $follower->prf_first_name }}
                            {{ $follower->prf_first_surname }}
                            {{ '( '.$follower->name.' )' }}
                            <a href="#" style="color:red">Eliminar seguidor </a>
                            <br>
                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<style>
    .content {
    width: 100%;
}

.title {
    font-size: 20px;
    font-weight: bold;
    padding-left: 10px;
    padding-top: 10px;   
    padding-bottom: 10px;
    width: 97%;
}

.left {
    padding-left: 10px;
    padding-top: 10px;
    margin-left: 57px;
    float: left;
    position: relative;
    width: 45%;
    height: auto;
}

.right {
    padding-top: 10px;
    padding-left: 10px;
    margin-left: 10px;
    position: relative;
    float: left;
    width: 45%;
    height: auto;
}
</style>

<style>
    .user_card 
    {
        color:#000;   
        padding:15px; 
        width: 100%;
        height: auto; 
        margin:0px 10px;
        text-align: center;
        background:rgba(0, 0, 0, 0.03);
        
    }
</style>
















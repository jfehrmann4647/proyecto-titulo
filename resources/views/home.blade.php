@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Menú principal') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <a href="{{ route('interestings') }}">Intereses</a>
                    <br>
                    <a href="{{ route('showProfile') }}">Mi Perfil</a>
                    <br>
                    <a href="{{ route('showAllUsers') }}">Seguidores</a>
                    <br>
                    <a href="{{ route('showFollowerFollowings') }}">Seguidores/Seguidos</a>
                    <br>
                    <a href="{{ route('showPostings') }}">Publicaciones</a>
                    <br>
                    <a href="{{ route('createDebate') }}">Debates</a>
                    <br>
                    <form action="{{ route('logout')}}" method="POST" style="float:right;">
                        @csrf
                        <input type="submit" value="Cerrar Sesión">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<div class="chat-streaming-list">
    @foreach ($this->streaming_messages as $message)
    <div class="chat-streaming-container">
        <div class="flex">
            <a href="{{ url('Profile/'.$name = $message->stc_username)}}" class="flex-1 hard-font">{{ $message->stc_username }} </a>
            <div class="flex-100 light-font cursor-standar">[{{ $message->stc_upload_time }}]</div>
        </div>
        <div class="hr"></div>
        <div class="chat-streaming-message">
            {{ $message->stc_message }}
        </div>
    </div>
    @endforeach
</div>

<script>
    
    $( document ).ready(function(){   
        window.livewire.emit('messageReload',id_streaming);
    });


    setInterval(function(){ 
        window.livewire.emit('messageReload',id_streaming); 
    }, 4000);
 
</script>
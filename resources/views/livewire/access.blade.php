<div class="col-span-7 navbar-tools">
    <a class="tools btn href goPost" title="Ir a publicar"><img src="{{asset('img/breaknotes-icon/breakn_post.png')}}" width="30px"></a>
    <!--<button class="tools btn" title="Ir a Opinar"><img src="{{asset('img/breaknotes-icon/breakn_opinion.png')}}" width="30px"></button> -->
    <a class="tools btn href goDebate" title="Ir a debatir"><img src="{{asset('img/breaknotes-icon/breakn_debate.png')}}" width="30px"></a>
    <a class="tools btn href goTransmition" title="Ir a transmitir"><img src="{{asset('img/breaknotes-icon/breakn_stream.png')}}" width="30px"></a>
    <a class="tools btn href" href="{{ url('Profile/'.$name = Auth::user()->name) }}" title="Ir a mi perfil"><img src="{{asset('img/breaknotes-icon/breakn_user.png')}}" width="30px"></a>
</div>

<!-- modal publicación -->

<div class="modal-post-access">
    <div class="modal-box">
      <form action="{{ route('createPosting') }}" method="POST">
        @csrf
        <div class="modal-title title flex">
          <div class="flex-1 modal-text-center">Crear publicación</div>
          <div class="flex-0 modal-close">x</div>
        </div>
        <div class="modal-info-post flex items-center">
          <div class="modal-img-container">
            <img class="modal-profile-photo" src="{{ Auth::user()->profile_photo_path }}">
          </div>
          <div>
            <div class="sub-title">
              {{ Auth::user()->name }}
            </div>
            <div class="light-font">
              {{ $profile_info->prf_first_name }} {{ $profile_info->prf_first_surname }}
            </div>
          </div>
          
        </div>
        <div class="whrite-post">
          <textarea name="content" class="textarea-control" placeholder="Escribir publicacion"></textarea>
        </div>
        <div class="footer-panel">
          <button type="submit" class="btn-submit btn-modal" disabled>Publicar</button>
        </div>
      </form>
    </div>
</div>

<!-- modal transmisión -->

<div class="modal-transmition-access">
    <div class="modal-box">
      <form action="{{ route('createTransmition') }}" method="POST">
        @csrf
        <div class="modal-title title flex">
          <div class="flex-1 modal-text-center">Crear transmisión</div>
          <div class="flex-0 modal-close">x</div>
        </div>
        <div class="modal-info-post flex items-center">
          <div class="modal-img-container">
            <img class="modal-profile-photo" src="{{ Auth::user()->profile_photo_path }}">
          </div>
          <div>
            <div class="sub-title">
              {{ Auth::user()->name }}
            </div>
            <div class="light-font">
              {{ $profile_info->prf_first_name }} {{ $profile_info->prf_first_surname }}
            </div>
          </div>
        </div>
        <div class="streaming-title">
          <input name="streaming-title" type="text" class="input-control" placeholder="Título de la transmisión" required>
        </div>
        <div class="streaming-theme">
          <select name="streaming-theme" type="text" class="input-control" required>
            <option value="">--Seleccione el tema de la transmisión-</option>
            @foreach( $interestings as $interesting )
            <option value="{{ $interesting->id }}">{{ $interesting->itg_name }}</option>
            @endforeach
          </select>
        </div>
        <div class="streaming-description">
          <textarea maxlength="500" name="streaming-description" class="textarea-control textarea-value-options" placeholder="Descripción de la transmisión"></textarea>
        </div>
        <div class="footer-panel">
          <button type="submit" class="btn-submit btn-modal" disabled>Crear</button>
        </div>
      </form>
    </div>
  </div>

<!-- modal debates -->

<div class="modal-debate-access">
    <div class="modal-box debate">
      <form action="{{ route('createDebate') }}" method="POST">
        @csrf
        <div class="modal-title title flex">
          <div class="flex-1 modal-text-center">Crear debate</div>
          <div class="flex-0 modal-close">x</div>
        </div>
        <div class="modal-info-post flex items-center">
          <div class="modal-img-container">
            <img class="modal-profile-photo" src="{{ Auth::user()->profile_photo_path }}">
          </div>
          <div>
            <div class="sub-title">
              {{ Auth::user()->name }}
            </div>
            <div class="light-font">
              {{ $profile_info->prf_first_name }} {{ $profile_info->prf_first_surname }}
            </div>
          </div>
          
        </div>
        <div class="debate-title">
          <input name="debate-title" type="text" class="input-control" placeholder="Título del debate" required>
        </div>
        <div class="debate-theme">
          <select name="debate-theme" type="text" class="input-control selected-value-options" required>
            <option value="">--Seleccione el tema del debate--</option>
            @foreach( $interestings as $interesting )
              <option value="{{ $interesting->id }}">{{ $interesting->itg_name }}</option>
            @endforeach
          </select>
        </div>
        <div class="whrite-post">
          <textarea name="content" class="textarea-control textarea-value-options" placeholder="Escribir argumento inicial"></textarea>
        </div>
        <div class="footer-panel">
          <button type="submit" class="btn-submit btn-modal" disabled>Crear</button>
        </div>
      </form>
    </div>
  </div>


<script>

$( ".goPost" ).click(function() {
    $('.modal-post-access').css('display', 'flex');
});

$( ".goTransmition" ).click(function() {
    $('.modal-transmition-access').css('display', 'flex');
});

$( ".goDebate" ).click(function() {
    $('.modal-debate-access').css('display', 'flex');
});

</script>

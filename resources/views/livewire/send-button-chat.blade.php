<div>
    <button id="sendMessage" onkeydown="not_space(event)" class="btn-submit chat-send-messages-icon" wire:click.defer="sendMessageOk" wire:loading.attr="disabled" wire:offline.attr="disabled" disabled>
        <img src="{{asset('img/breaknotes-icon/breakn_sendmessage.png')}}" width="30px">
    </button>
</div>

<div>
  <li class="social-search">
    <div class="title-social-search">Contactos</div>
    <div class=""><button class="btn social-search-btn">({{$this->total_contacts }})</button></div>
  </li>
@for($i = 0; $i < count($this->contacts); $i++)
<button wire:click="selectUserMessage( {{ $this->contacts[$i]['id'] }} )" class="w-full friend-list flex items-center">
  <div class="friend-img">
    <img class="user-chat-photo" src="{{ asset( $this->contacts[$i]['profile_photo_path'] )}}"/>
  </div>  
  <div class="friend-username">
    <span>{{ $this->contacts[$i]['name'] }}</span>
 </div>
</button>
@endfor
</div> 

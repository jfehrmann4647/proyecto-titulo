<div >
        <div class="chat-section">
          <div class="chat-box">
            <div class="chat-name flex">
              <div class="name flex-1">{{ $this->username_chat }}</div>
              <div class="close-chat flex-100">
                <span class="move-close-chat">x</span>
              </div>
            </div>
            <div class="body-chat">
              <div class="message-container">
                <div class="move-chat-start"></div>
              @if (!$this->user_messages)
                <div class="h-full flex justify-center">
                  <img class="m-auto" src="{{ asset('img/breaknotes-icon/breakn-loading.svg') }} " width="100px">
                </div>
              @else
              @foreach ($this->user_messages as $messages)
              <div class="chat-message">
                <div class="user-chat-img flex items-center">
                  <div class="chat-img-container"><img class="user-photo-chat" src="{{ asset($messages->cht_photo_emit)}}"></div>
                  <div>{{ $messages->cht_username_emit }}</div>
                </div>
                <div class="hr"></div>
                <div class="user-message">{{ $messages->cht_users_message }}</div>
              </div>                
              @endforeach
              @endif
              <div class="move-chat-into"></div>
            </div>
          </div>
          <div class="flex p-1 items-center">
            <div class="w-full pr-1">
              <textarea id="message" type="text" wire:keydown.enter="sendMessage" wire:model.defer="message" class="w-full textarea-control chat-control" placeholder="Escribir"></textarea>
            </div>
            <div>
              @livewire('send-button-chat')
            </div>
          </div>
        </div>
      </div>
</div>

<script>
  setInterval(function(){ 
    window.livewire.emit('messageReloadUserChat'); 

    let move_chat = $('.move-chat-into').offset().top;
    let start_chat = $('.move-chat-start').offset().top * (-1);
    let move_to = move_chat + start_chat;
        $('.message-container').animate({
            scrollTop: move_to
      }, 0);
  }, 4000);

  $(document).ready(function() {
        $('#message').keypress(function(event) {
            if (event.keyCode == 13) {
                event.preventDefault();
            }
        });
  });

</script>


<div class="chat-form-list flex">
    <textarea type="text" id="message" class="textarea-control w-full" placeholder="Escribir un mensaje" 
    wire:model="message" wire:keydown.enter="sendMessage(`${id_streaming}`)"></textarea>
    <button type="submit" id="sendMessage" class="btn-submit send-streaming-message" 
    wire:click="sendMessage(`${id_streaming}`)" wire:loading.attr="disabled"
    wire:offline.attr="disabled">
        <img src="{{asset('img/breaknotes-icon/breakn_sendmessage.png')}}" width="30px">
    </button>

    <script>
        let id_streaming = $('#streaming-id').val();
        console.log(id_streaming);
    </script> 
</div>

<script>
    $(document).ready(function() {
        $('#message').keypress(function(event) {
            if (event.keyCode == 13) {
                event.preventDefault();
            }
        });
    });
</script>

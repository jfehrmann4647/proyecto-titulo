@extends('GeneralIndex.GeneralDesign')

@section('modal-section')


<div class="modal-post new-debate-debate">
  <div class="modal-box">
    <form action="{{ route('debateStart') }}" method="POST">
      @csrf
      <div class="modal-title title flex">
        <div class="flex-1 modal-text-center">Debatir argumento</div>
        <div class="flex-0 modal-close">x</div>
      </div>
      <div class="modal-info-post flex items-center">
        <div class="modal-img-container">
          <img class="modal-profile-photo" src="{{ asset(Auth::user()->profile_photo_path) }}">
        </div>
        <div>
          <div class="sub-title">
            {{ Auth::user()->name }}
          </div>
          <div class="light-font">
            {{ $profile_info->prf_first_name }} {{ $profile_info->prf_first_surname }}
          </div>
        </div>
        
      </div>
      <div class="whrite-post">
        <input type="hidden" name="idDebate" value="{{ $idDebate }}">
        <textarea name="contentDebate" class="textarea-control" placeholder="Escribir argumento"></textarea>
      </div>
      <div class="footer-panel">
        <button type="submit" class="btn-submit btn-modal" disabled>Debatir</button>
      </div>
    </form>
  </div>
</div>


  <div class="modal-debate">
    <div class="modal-box">
      <form action="{{ route('createArgument') }}" method="POST">
        @csrf
        <div class="modal-title title flex">
          <div class="flex-1 modal-text-center">Debatir argumento</div>
          <div class="flex-0 modal-close">x</div>
        </div>
        <div class="modal-info-post flex items-center">
          <div class="modal-img-container">
            <img class="modal-profile-photo" src="{{ asset( Auth::user()->profile_photo_path )}}">
          </div>
          <div>
            <div class="sub-title">
              {{ Auth::user()->name }}
            </div>
            <div class="light-font">
              {{ $profile_info->prf_first_name }} {{ $profile_info->prf_first_surname }}
            </div>
          </div>
        </div>
        <div class="whrite-post">
          <input type="hidden" name="idDebate" value="{{ $idDebate }}">
          <textarea name="debate-content" class="textarea-control" placeholder="Escribir argumento"></textarea>
        </div>
        <div class="footer-panel">
          <button type="submit" class="btn-submit btn-modal" disabled>Debatir</button>
        </div>
      </form>
    </div>
  </div>
@endsection

@section('second-navbar')
  <div class="second-navbar-menu flex justify-center">
    <div class="second-navbar-tool flex m-auto">
      <a class="px-12 py-2" href="{{route('General')}}">Publicaciones</a>
      <a class="px-12 py-2" href="{{route('Debate')}}">Debates</a>
      <a class="px-12 py-2" href="{{route('Streaming')}}">En vivo</a>
    </div>
  </div>
@endsection

@section('seccion')
  <div class="grid grid-cols-12" style="height: 100%; padding: 0; margin: 0;">
    <div class="col-span-9 mid-column">
      <div class="mid-column">
        <div class="general-content">
          <div class="general-navbar z-10 mb-1">
            <div class="grid grid-cols-12 gap-2">
              <div class="col-span-5 search-box">
                <div class="md-form active-cyan-2">
                  @livewire('search-user')
                  <div class="people-search">
                    <div class="sub-title px-3">Personas</div>    
                    @livewire('search-list')
                  </div>
                </div>
              </div>
              @livewire('access')
            </div>
          </div>
          <div class="general-body">
            @if ($debate->dbt_state == 1 && Auth::user()->id == $debate->usr_user || Auth::user()->id == $debate->usr_opponent )
              <div class="debate-room-messages">
                <div class="argument-box-messages none" id="argument-message">
                  <p>Esperando respuesta del oponente...</p>
                </div>
                <div class="argument-new-message" id="argument-new-message">
                  <div class="argument-box-messages">
                    <p>Nuevo argumento!</p>
                    <a id="reloadDebateRoom" >Click para verlo!</a>
                  </div>
                </div>
              </div>
            @endif
              <div class="user-debate-container mb-2 mt-2">
                <div class="title-box title">
                  <div class="span flex">
                    <div class="overflow-wrap">{{$debate->dbt_title}}</div>
                    <div class="light-font">{{$debate->itg_name}}
                    @if ( $debate->dbt_state != 0 && $debate->dbt_state_date )
                      <div class="light-font"> Finaliza el {{ date("d-m-Y", strtotime($debate->dbt_state_date)) }} a las {{$debate->dbt_state_time}}</div>
                    @endif
                    </div>
                  </div>
                  <div class="span text-right" style="display: none;">
                    @livewire('debate-alert',['id_debate' => $debate->id])

                  <!-- se muestra solamente si el debate ya inició y si los contrincantes acceden a la sala-->
                    
                  </div>
                </div>
                <div class="grid grid-cols-12">
                  <div class="col-span-6 px-4">
                    <a href="{{ url('Profile/'.$name = $debate->UC_name) }}">
                      <div class="user-starter-debate flex">
                        <div class="items-center justify-center flex pr-3">
                          <img class="user-photo-debate" src="{{asset ($debate->UC_photo) }}" width="45px">
                        </div>
                        <div class="justify-center items-center flex py-5">
                          <span>{{ $debate->UC_name }}</span>
                        </div>
                      </div>
                    </a>
                    <div class="debate-argument py-3">{{ $debate->dbt_content }}</div>
                  </div>

                  <!--si el debate aun no tiene oponente, se muestra -->
                  @if($debate->usr_opponent == null)
                    <div class="col-span-6 px-4">
                      <div class="non-user-debate">
                        @if($debate->usr_user != Auth::user()->id )
                        <button class="post-control new-debate-btn"><span>Debatir argumento</span></button>
                        @endif
                      </div>
                      <div class="debate-argument py-3"></div>
                    </div>
                  @else

                  <div class="col-span-6 px-4">
                      <a href="{{ url('Profile/'.$name = $debate->UO_name) }}">
                        <div class="user-starter-debate flex">
                          <div class="items-center justify-center flex pr-3">
                            <img class="user-photo-debate" src="{{ asset($debate->UO_photo )}}" >
                          </div>
                          <div class="justify-center items-center flex py-5">
                            <span>{{ $debate->UO_name }} </span>
                          </div>
                        </div>
                      </a>
                      <div class="debate-argument py-3">{{ $debate->dbt_content_opponent }}</div>
                    </div>
                    <div class="col-span-12 px-4">

                      <!-- se muestra si el debate ya finalizó -->
                      @if ($debate->dbt_state == 0)
                        <div class="debate-result ">
                          @if ($like_usr_user > $like_usr_opponent)
                          <div class="hr"></div>
                            <div class="flex items-center">
                              <div class="pr-3"><img class="user-photo-debate" src="{{ asset($debate->UC_photo) }}"></div>
                              <div>{{ $debate->UC_name }} &nbsp</div>
                              <div>Ganó el debate</div>
                            </div>
                            <div class="hr"></div>
                          @elseif($like_usr_user < $like_usr_opponent)
                          <div class="hr"></div>
                            <div class="flex items-center">
                              <div><img class="user-photo-debate" src="{{ asset($debate->UC_photo) }}"></div>
                              <div>{{ $debate->UC_name }}</div>
                              <div>Ganó el debate</div>
                            </div>
                            <div class="hr"></div>
                          @else
                          <div class="hr"></div>
                          <div class="flex items-center">
                            <div>Los contrincantes empataron el debate.</div>
                          </div>
                          <div class="hr"></div>
                          @endif
                        </div>
                      @endif

                      <!-- argumentos del debate -->
                    @foreach ($argument as $arguments)
                      <div class="argument-box">
                        <div class="info-user-argument flex items-center">
                          <div class="user-photo"><img class="debate-argument-photo" src="{{ asset($arguments->profile_photo_path) }}" width="40px"></div>
                          <div class="user-name">{{ $arguments->name }} </div>
                        </div>
                        <div class="hr"></div>
                        <div class="user-argument-content">{{ $arguments->dba_content }}</div>
                        <div class="hr"></div>
                        <div class="flex like-argument-container items-center">
                          <div class="flex-1">
                          <form  action="{{ route('likeArgument') }}" method="POST">
                            @csrf
                            <input type="hidden" name="idArgument" value="{{ $arguments->id }}">
                            @if ($debate->dbt_state != 0)
                            <button class="flex items-center">
                              <img class="mr-2" src="{{ asset('img/breaknotes-icon/breakn_like.png') }}" width="20px">
                              <div class="sub-title">Buen argumento</div>
                            </button>
                            @endif
                          </form>
                          </div>
                          <div class="light-font flex-100 href"> {{ $arguments->dba_like }} personas votaron por este argumento</div>
                        </div>
                        <!-- si soy el contricante lo veo-->
                      </div>
                      @endforeach
                      @if(Auth::user()->id == $debate->usr_user || Auth::user()->id == $debate->usr_opponent)
                        @if ( $debate->dbt_state != 0 )
                          <div class="grid grid-cols-12 gap-4 p-4">
                            <div class="col-span-1 flex justify-center items-center">
                                <div><img class="relative bottom-1 img-deb-review" src="{{ asset(Auth::user()->profile_photo_path) }}"/></div>
                            </div>
                            <div class="col-span-11">
                              <input type="hidden" name="idPosting" value="">
                              <input type="text" class="input-control comment-control modal-show-dabate" placeholder="Responder argumento" readonly/>
                            </div>
                          </div>
                        @endif
                      @endif
                    </div>
                  @endif
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-span-3 right-column">
      <div class="lateral-menu">
        <ul class="lateral-navbar">
          @livewire('chat-list-user')
          
        </ul>
      </div>
    </div>
  </div>

  <script>
    setInterval(function(){ 
      window.livewire.emit('reloadDebate');
    }, 5000);

    var observer = new MutationObserver(function(mutations) {
      mutations.forEach(function(mutation) {
        $('#argument-message').css('display','none'); 
        $('.debate-room-messages #argument-new-message').css('display','block'); 
      });    
    });


    var config = { childList: true, subtree:true, attributes: true, characterData: true,
        characterDataOldValue: true, attributeOldValue: true };

    var target = jQuery('#value_total_arguments').get(0); 
    observer.observe(target, config); 


    $('#reloadDebateRoom').click(function(){
      location.reload();
    });

    $('.modal-show-dabate').click(function(){
      $('.modal-debate').css('display', 'flex');
    });
  </script>
@endsection
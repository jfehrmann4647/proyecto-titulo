@extends('GeneralIndex.GeneralDesign')

@section('second-navbar')
  <div class="second-navbar-menu flex justify-center">
    <div class="second-navbar-tool flex m-auto">
      <a class="px-12 py-2" href="{{route('General')}}">Publicaciones</a>
      <a class="px-12 py-2" href="{{route('Debate')}}">Debates</a>
      <a class="px-12 py-2" href="{{route('Streaming')}}">En vivo</a>
    </div>
  </div>
@endsection

@section('seccion')
  <div class="grid grid-cols-12" style="height: 100%; padding: 0; margin: 0;">
    <div class="col-span-9 mid-column">
      <div class="mid-column">
        <div class="general-content">
          <div class="general-body">
            <div class="streaming-container-room">
              <script>
                  if(!location.hash.replace('#', '').length) {
                      location.href = location.href.split('#')[0] + '#' + (Math.random() * 100).toString().replace('.', '');
                      location.reload();
                  }
              </script>
              <!-- scripts used for screen-sharing -->
              <script src="https://www.webrtc-experiment.com/socket.io.js"> </script>
              <script src="https://www.webrtc-experiment.com/DetectRTC.js"></script>
              <script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>
              <script src="https://www.webrtc-experiment.com/CodecsHandler.js"></script>
              <script src="https://www.webrtc-experiment.com/BandwidthHandler.js"></script>
              <script src="https://www.webrtc-experiment.com/IceServersHandler.js"></script>
              <script src="{{ asset('js/conference.js')}}"></script>
                  <section id="logs-message" class="experiment" style="display: none;text-align: center;font-size: 1.5em;line-height: 2;color: red;">WebRTC getDisplayMedia API.</section>
                  <!-- just copy this <section> and next script -->
                  <section class="experiment">
                    @if (Auth::user()->id == $streaming_info->id_user)
                      <section class="hide-after-join" style="">                    
                          <input id="streaming-id" type="text" class="streaming-id" value="{{ $streaming_info->id_streaming }}" hidden>
                          <button id="share-screen" class="setup flex items-center">
                            <div class="rec"></div>
                            <div class="px-2">Comenzar transmisión</div>
                          </button>
                      </section>
                      <section class="show-after-join" style="display:none;">                    
                        <input id="streaming-id" type="text" class="streaming-id" value="{{ $streaming_info->id_streaming }}" hidden>
                        <button id="share-screen-stop" class="setup flex items-center">
                          <div class="rec"></div>
                          <div class="px-2">Terminar transmisión</div>
                        </button>
                      </section>
                    @else
                    <input id="streaming-id" type="text" class="streaming-id" value="{{ $streaming_info->id_streaming }}" hidden>
                    <section class="hide-after-join">                    
                        <input type="text" id="room-name" placeholder="Enter " hidden>
                    </section>                                           
                    @endif

                      <!-- local/remote videos container -->
                      <div class="video-box">
                        <div id="videos-container"></div>
                      </div>
                      <div class="info-streaming">
                        <div class="title text-overflow-elipsis">{{$streaming_info->str_title}}</div>
                        <div class="light-font">{{$streaming_info->itg_name}}</div>
                        <div class="hr"></div>
                        <div class="flex">
                          <a href="{{ url('Profile/'.$name = $streaming_info->name)}}" class="user-streaming flex items-center">
                            <div><img class="user-streaming-photo" src="{{asset( $streaming_info->profile_photo_path )}}"></div>
                            <div class="title ">{{$streaming_info->name}}</div>
                          </a>
                        </div>
                        <div class="hr"></div>
                        <div class="break-all-text">{{ $streaming_info->str_description }}</div>
                      </div>
                      <section id="unique-token" style="display: none; text-align: center;">
                      </section>
                  </section>
                  <script>
                      let config = {
                          openSocket: function(config) {
                              let SIGNALING_SERVER = 'https://socketio-over-nodejs2.herokuapp.com:443/';

                              config.channel = config.channel || location.href.replace(/\/|:|#|%|\.|\[|\]/g, '');
                              let sender = Math.round(Math.random() * 999999999) + 999999999;

                              io.connect(SIGNALING_SERVER).emit('new-channel', {
                                  channel: config.channel,
                                  sender: sender
                              });

                              let socket = io.connect(SIGNALING_SERVER + config.channel);
                              socket.channel = config.channel;
                              socket.on('connect', function () {
                                  if (config.callback) config.callback(socket);
                              });

                              socket.send = function (message) {
                                  socket.emit('message', {
                                      sender: sender,
                                      data: message
                                  });
                              };

                              socket.on('message', config.onmessage);
                          },
                          onRemoteStream: function(media) {
                              if(isbroadcaster) return;

                              let video = media.video;
                              videosContainer.insertBefore(video, videosContainer.firstChild);
                          

                              document.querySelector('.hide-after-join').style.display = 'none';
                          },
                          onRoomFound: function(room) {
                              if(isbroadcaster) return;

                              conferenceUI.joinRoom({
                                  roomToken: room.roomToken,
                                  joinUser: room.broadcaster
                              });

                              document.querySelector('.hide-after-join').innerHTML = '<img src="https://www.webrtc-experiment.com/images/key-press.gif" style="margint-top:10px; width:50%;" />';
                          },
                          onNewParticipant: function(numberOfParticipants) {
                              let text = numberOfParticipants + ' en vivo...';
                              
                              if(numberOfParticipants <= 0) {
                                  text = 'En vivo...';
                              }
                              else if(numberOfParticipants == 1) {
                                  text = 'En vivo...';
                              }

                              document.title = text;
                              showErrorMessage(document.title, 'green');
                          },
                          oniceconnectionstatechange: function(state) {
                              let text = '';

                              if(state == 'closed' || state == 'disconnected') {
                                  text = 'En vivo...';
                                  document.title = text;
                                  showErrorMessage(document.title);
                              }

                              if(state == 'failed') {
                                  text = 'En vivo...';
                                  document.title = text;
                                  showErrorMessage(document.title);
                              }

                              if(state == 'connected' || state == 'completed') {
                                  text = 'En vivo...';
                                  document.title = text;
                                  showErrorMessage(document.title, 'green');
                              }

                              if(state == 'new' || state == 'checking') {
                                  text = 'En vivo...';
                                  document.title = text;
                                  showErrorMessage(document.title, 'green');
                              }
                          }
                      };

                      function showErrorMessage(error, color) {
                          let errorMessage = document.querySelector('#logs-message');
                          errorMessage.style.color = color || 'red';
                          errorMessage.innerHTML = error;
                          errorMessage.style.display = 'none';
                      }

                      function getDisplayMediaError(error) {
                          if (location.protocol === 'http:') {
                              showErrorMessage('Please test this WebRTC experiment on HTTPS.');
                          } else {
                              showErrorMessage(error.toString());
                          }
                      }

                      function captureUserMedia(callback) {
                          let video = document.createElement('video');
                          
                          video.muted = false;
                          video.volume = 0;
                          try {
                              video.setAttributeNode(document.createAttribute('autoplay'));
                              video.setAttributeNode(document.createAttribute('playsinline'));
                              video.setAttributeNode(document.createAttribute('controls'));
                          } catch (e) {
                              video.setAttribute('autoplay', true);
                              video.setAttribute('playsinline', true);
                              video.setAttribute('controls', true );
                          }

                          if(navigator.getDisplayMedia || navigator.mediaDevices.getDisplayMedia) {
                              function onGettingSteam(stream) {
                                  video.srcObject = stream;
                                  videosContainer.insertBefore(video, videosContainer.firstChild);

                                  addStreamStopListener(stream, function() {
                                      location.reload();
                                  });

                                  config.attachStream = stream;
                                  callback && callback();

                                  addStreamStopListener(stream, function() {
                                      location.reload();
                                  });

                                  showPrivateLink();

                                  document.querySelector('.hide-after-join').style.display = 'none';
                                  document.querySelector('.show-after-join').style.display = 'block';
                              }

                              if(navigator.mediaDevices.getDisplayMedia) {
                                  navigator.mediaDevices.getUserMedia({video: true, audio:true}).then(stream => {
                                      onGettingSteam(stream);
                                  }, getDisplayMediaError).catch(getDisplayMediaError);
                              }
                              else if(navigator.getDisplayMedia) {
                                  navigator.getUserMedia({video: true, audio:true }).then(stream => {
                                      onGettingSteam(stream);
                                  }, getDisplayMediaError).catch(getDisplayMediaError);
                              }
                          }
                          else {
                              if (DetectRTC.browser.name === 'Chrome') {
                                  if (DetectRTC.browser.version == 71) {
                                      showErrorMessage('Please enable "Experimental WebPlatform" flag via chrome://flags.');
                                  } else if (DetectRTC.browser.version < 71) {
                                      showErrorMessage('Please upgrade your Chrome browser.');
                                  } else {
                                      showErrorMessage('Please make sure that you are not using Chrome on iOS.');
                                  }
                              }

                              if (DetectRTC.browser.name === 'Firefox') {
                                  showErrorMessage('Please upgrade your Firefox browser.');
                              }

                              if (DetectRTC.browser.name === 'Edge') {
                                  showErrorMessage('Please upgrade your Edge browser.');
                              }

                              if (DetectRTC.browser.name === 'Safari') {
                                  showErrorMessage('Safari does NOT supports getDisplayMedia API yet.');
                              }
                          }
                      }

                      /* on page load: get public rooms */
                      let conferenceUI = conference(config);

                      /* UI specific */
                      let videosContainer = document.getElementById("videos-container") || document.body;

                      document.getElementById('share-screen').onclick = function() {

                          let roomName = document.getElementById('room-name') || { };
                          roomName.disabled = true;

                          captureUserMedia(function() {
                              conferenceUI.createRoom({
                                  roomName: (roomName.value || 'Anonymous') + ' shared his screen with you'
                              });
                          });
                          this.disabled = true;
                      };

                      function showPrivateLink() {
                          let uniqueToken = document.getElementById('unique-token');
                          uniqueToken.style.display = 'block';
                      }


                  </script>
                  <section class="experiment"><small id="send-message"></small></section>
              <script src="https://www.webrtc-experiment.com/commits.js" async> </script>  
              <script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>           
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-span-3 right-column">
      <div class="lateral-menu">
        <ul class="lateral-navbar">
          <li class="social-search">
            <div class="title-social-search">Chat en Vivo </div>
          </li>
          @livewire("chat-list-streaming")
          @livewire("chat-form-streaming")
        </ul>
      </div>
    </div>
  </div>

    
    
  </div>
  <script src="{{ asset('js/jquery.js') }}"></script>
<script>

    let laravelToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
    
    $("#share-screen").click(function() {
    let idStreaming = $(this).parent().children('.streaming-id').val();
    let urlStreaming = window.location.href; 

    let data = {
      '_token' : laravelToken,
      'idStreaming' : idStreaming,
      'urlStreaming' : urlStreaming
    };

    $.ajax({
        type: "POST",
        url: "{{ route('startTransmition') }}",  
        data:data
    });
  });


  $("#share-screen-stop").click(function() {

    let idStreaming = $(this).parent().children('.streaming-id').val();

    let data = {
      '_token' : laravelToken,
      'idStreaming' : idStreaming
    };

    $.ajax({
        type: "POST",
        url: "{{ route('stopTransmition') }}",  
        data:data,       
        success:function(streamingResult){
            location.reload();
        }
    });
  });


    console.info(performance.navigation.type);
    if (performance.navigation.type == performance.navigation.TYPE_RELOAD)
    {
        let idStreaming = $('#share-screen-stop').parent().children('.streaming-id').val();
        let data = {
        '_token' : laravelToken,
        'idStreaming' : idStreaming
        };
        $.ajax({
            type: "POST",
            url: "{{ route('stopTransmition') }}",  
            data:data
        });
    }
</script>
@endsection
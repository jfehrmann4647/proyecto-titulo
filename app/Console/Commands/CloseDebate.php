<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

Use App\Models\Debate; 

class CloseDebate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'close:debate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'cierre del debate cada 3 dias';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $schedule_debate = Debate::select('id','dbt_state_date','dbt_state_time','dbt_state')->get();
        $current_day_hour = Carbon::now()->format('Y-m-d h:i:s');
      
        foreach($schedule_debate as $debate)
        {
            $debate_day_hour = $debate->dbt_state_date.' '.$debate->dbt_state_time;

            if($current_day_hour > $debate_day_hour && $debate->dbt_state == 1)
            {
                Debate::where('id',$debate->id)->update(['dbt_state'=> 0]);
            }
        }

    }
 
    /* php artisan make:command CloseDebate ---> crea el comando a ejecutar con su tarea. */
    /* php artisan schedule:list ---> lista los procesos que se han creado. */
    /* php artisan schedule:run ---> ejecuta la tarea una vez. */
    /* php artisan schedule:work ---> pone a trabajar la tarea en segundo plano. */

    /* PROCESO PARA CREAR CRON JOB EN LARAVEL. 
    
    1. crear el comando a ejecutar.
    2. agregar el comando a kernel.php en la función schedule.
    3. agregar Commands\nombre_comando::class, a protected commands en kernel.php.
    4. escribir código que la tarea ejecutará en la función handle.php del comando. 
    5. ejecutar la tarea. 
    
    */
    
}

<?php


function GetDateTime()
{
    date_default_timezone_set('America/Santiago');
    $date = date('Y-m-d');  
    $time = date('G:i:s');

    $date_time_collection = collect(['date'=>$date,'time'=>$time]);
    return $date_time_collection;
}

function DeleteDuplicate($array)
{
    $array_posting_clear = [];
    foreach($array as $key => $value) 
        if (!in_array($value, $array_posting_clear)) 
            $array_posting_clear[$key] = $value;
   
    return $array_posting_clear;
}

function deleteEspecialCharacter($string) 
{
    $string = mb_convert_case($string, MB_CASE_LOWER, "UTF-8");
    $string = preg_replace('/\s+/', '-', $string); 
    $string = str_replace ('?',' ', $string); 
    
    $not_allowed= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
    $allowed= array ("a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E");
    $clear_string = str_replace($not_allowed, $allowed ,$string);
    return $clear_string;
}


function deleteAccentMark($string) 
{
    $string = mb_convert_case($string, MB_CASE_LOWER, "UTF-8");
    $string = str_replace ('?',' ', $string); 
    
    $not_allowed= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
    $allowed= array ("a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E");
    $clear_string = str_replace($not_allowed, $allowed ,$string);
    return $clear_string;
}

function photoStreaming()
{
    $array = [
        1 => 'img/breaknotes-streaming-photos/musica-streaming.png',
        2 => 'img/breaknotes-streaming-photos/programacion-streaming.png',
        3 => 'img/breaknotes-streaming-photos/historia-streaming.png',
        4 => 'img/breaknotes-streaming-photos/ciencias-streaming.png',
        5 => 'img/breaknotes-streaming-photos/politica-streaming.png',
        6 => 'img/breaknotes-streaming-photos/deportes-streaming.png',
        7 => 'img/breaknotes-streaming-photos/astronomia-streaming.png',
        8 => 'img/breaknotes-streaming-photos/tecnologia-streaming.png',
        9 => 'img/breaknotes-streaming-photos/matematicas-streaming.png',
        10 => 'img/breaknotes-streaming-photos/medioambiente-streaming.png'
    ];

    return $array; 
}











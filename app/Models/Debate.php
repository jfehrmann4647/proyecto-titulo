<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Debate extends Model
{
    protected $table = 'debates'; 

    protected $fillable = ['id',
        'dbt_title',
        'dbt_content',
        'dbt_content_opponent',
        'dbt_upload_date',
        'dbt_upload_time',
        'dbt_state',
        'dbt_state_date',
        'dbt_state_time',
        'usr_user',
        'usr_opponent',
        'itg_interesting'
    ];


    public $timestamps = false;
}

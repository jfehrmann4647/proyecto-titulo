<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserMessage extends Model
{
    protected $table = 'user_messages'; 
    protected $fillable = ['id','id_user_from',
    'id_user_to',
    'name_user_from',
    'name_user_to',
    'user_message'
    ];


}

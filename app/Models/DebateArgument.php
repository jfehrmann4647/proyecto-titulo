<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DebateArgument extends Model
{
    protected $table = 'debate_arguments'; 

    protected $fillable = ['id',
        'dba_content',
        'dba_upload_date',
        'dba_upload_time',
        'dba_like',
        'dba_like_list',
        'usr_user',
        'dbt_debate'
    ];

    public $timestamps = false;
}

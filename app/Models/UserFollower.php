<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserFollower extends Model
{
    protected $table = 'user_followers'; 
    protected $fillable = ['id','ufl_following_list','ufl_follower_list','usr_user'];

    public $timestamps = false;
}

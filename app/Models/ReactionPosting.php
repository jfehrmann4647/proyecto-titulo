<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReactionPosting extends Model
{
    protected $table = 'reaction_postings'; 

    protected $fillable = ['id',
    'rpst_type',
    'usr_user',
    'pst_code'
    ];

    public $timestamps = false;
}

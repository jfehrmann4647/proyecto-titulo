<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeactivateAccount extends Model
{
    protected $table = 'deactivate_accounts'; 

    protected $fillable = ['id',
        'deac_usr_user',
        'deac_username',
        'deac_email',
        'deac_password_no_hashed',
        'deac_upload_date',
        'deac_upload_time',
    ];

    public $timestamps = false;
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GlobalConfig extends Model
{
    protected $table = 'global_configs';
    protected $fillable = ['id',
    'gcf_first_name',
    'gcf_second_name',
    'gcf_first_surname',
    'gcf_second_surname',
    'gcf_cellphone',
    'gcf_address',
    'gcf_birthday',
    'gcf_gender',
    'gcf_description'];

    public $timestamps = false; 
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserInteresting extends Model
{
    protected $table = 'users_interestings'; 
    protected $fillable = ['id','usr_itg_upload_date','usr_itg_upload_time','usr_user','itg_code'];

    public $timestamps = false;

}

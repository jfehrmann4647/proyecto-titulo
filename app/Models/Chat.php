<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $table = 'chats';
    protected $fillable = [
        'id',
        'cht_username_emit',
        'cht_id_emit',
        'cht_photo_emit',
        'cht_username_receive',
        'cht_id_receive',
        'cht_users_message',
        'cht_upload_date',
        'cht_upload_time'
    ];

    public $timestamps = false;
}

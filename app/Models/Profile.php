<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profiles';
    protected $fillable = ['id',
    'prf_first_name',
    'prf_second_name',
    'prf_first_surname',
    'prf_second_surname',
    'prf_cellphone',
    'prf_address',
    'prf_birthday',
    'prf_gender',
    'prf_description',
    'gcf_code'];

    public $timestamps = false; 
}

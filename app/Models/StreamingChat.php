<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StreamingChat extends Model
{
    protected $table = 'streaming_chats'; 

    protected $fillable = ['id',
    'stc_username',
    'stc_message',
    'stc_upload_date',
    'stc_upload_time',
    'str_code'
    ];

    
    public $timestamps = false;
}

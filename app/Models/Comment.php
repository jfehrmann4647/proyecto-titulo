<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments'; 


    protected $fillable = ['id',
    'cmt_content',
    'cmt_upload_date',
    'cmt_upload_time',
    'usr_user',
    'pst_code'
    ];
    
    public $timestamps = false;
}

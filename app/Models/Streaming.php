<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Streaming extends Model
{
    protected $table = 'streamings'; 

    protected $fillable = ['id',
    'str_title',
    'str_description',
    'str_upload_date',
    'str_upload_time',
    'str_url',
    'str_photo',
    'str_state',
    'usr_user',
    'itg_interesting'
    ];

    
    public $timestamps = false;
}

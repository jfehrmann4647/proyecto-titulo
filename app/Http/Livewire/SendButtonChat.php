<?php

namespace App\Http\Livewire;

use Livewire\Component;

class SendButtonChat extends Component
{
    public function sendMessageOk() 
    {
        $this->emit('sendMessageOk');
    }
    public function render()
    {
        return view('livewire.send-button-chat');
    }
}

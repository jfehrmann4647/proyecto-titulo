<?php

namespace App\Http\Livewire;

use Illuminate\Http\Request;
use Livewire\Component;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;

use App\Models\DebateArgument;

class DebateAlert extends Component
{
    protected $listeners = ['reloadDebate','afterReloadDebate'];

    public $total_arguments; 
    public $id_debate;

    private $arguments;
    
    public function mount()
    {
        $arguments = DebateArgument::where('dbt_debate',$this->id_debate)->get();
        $this->total_arguments = count($arguments);       
    }

    public function reloadDebate()
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');
        
        $arguments = DebateArgument::where('dbt_debate',$this->id_debate)->get();
        $this->total_arguments = count($arguments);
    }

    public function render()
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');

        return view('livewire.debate-alert');
    }
}

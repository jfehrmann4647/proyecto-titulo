<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class SearchList extends Component
{
    public $user; 

    protected $listeners = ['nameRecieved'];

    public function mount()
    {
        $this->user = [];
    }

    public function nameRecieved($name)
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');

        if(!$name)
        {
            $this->user = [];
        }
        else
        {
            $this->user = DB::table('users')
            ->where('name', 'LIKE', '%'.$name.'%')
            ->select('users.name','users.profile_photo_path')
            ->where('status_account',1)
            ->get();
        }

    }
    public function render()
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');
        
        return view('livewire.search-list');
    }
}

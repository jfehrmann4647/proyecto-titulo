<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use App\Events\sendStreamingMessage;

use App\Models\User;
use App\Models\StreamingChat;

class ChatFormStreaming extends Component
{
    public $message;
    public $id_streaming; 
    public $username; 

    protected $listeners = ['sendMessage'];

    public function mount()
    {
        $this->message = '';
    }
    
    public function sendMessage($id_streaming)
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');

        $this->username = User::select('name')->where('id',Auth::user()->id)->first();
        $this->id_streaming = $id_streaming;

        $date_time = GetDateTime();

        StreamingChat::create([
            'stc_username' => $this->username->name,
            'stc_message' => $this->message,
            'stc_upload_date' => $date_time['date'],
            'stc_upload_time' => $date_time['time'],
            'str_code' => $id_streaming
        ]);
        
        $this->emit('messageOk',$this->id_streaming);
        $this->message = '';
    }

    public function render()
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');
        
        return view('livewire.chat-form-streaming');
    }
}


<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Models\StreamingChat;

class ChatListStreaming extends Component
{
    protected $listeners = ['messageOk','messageReload'];

    public $id_streaming; 
    public $streaming_messages = [];
    public $profile_photo;

    public function mount()
    {
        $this->streaming_messages = DB::table('streaming_chats')
        ->select()
        ->where('str_code',$this->id_streaming)
        ->get();
    }

    public function messageReload($id_streaming)
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');

        $this->id_streaming = $id_streaming;
        $this->messageOk($id_streaming);
    }

    public function messageOk($id_streaming)
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');

        $this->id_streaming = $id_streaming;
        $this->streaming_messages = DB::table('streaming_chats')
        ->select()
        ->where('str_code',$this->id_streaming)
        ->get();
    }


    public function render()
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');
        
        return view('livewire.chat-list-streaming');
    }
}

<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Models\Interesting;
use App\Models\User; 
use App\Models\UserInteresting; 
use App\Models\Profile; 
use App\Models\GlobalConfig;
use App\Models\UserFollower;
use App\Models\Chat;

class ChatListUser extends Component
{
    protected $listeners = ['reloadTotalMessages'];

    public $total_contacts = 1;
    public $contacts = [];
    public $total_messages = [];
  
    public function mount()
    {

        //validación de usuario.
        if(!Auth::user()) return redirect('/');

        //----------------- RECUPERAR SEGUIDOS ---------------------

        $id_user = Auth::user()->id;

        $restore_json = UserFollower::where('usr_user',$id_user)->first()->toArray();
        $restore_json_following  = $restore_json['ufl_following_list'];
        $restore_array_following = json_decode($restore_json_following);

        $array_following = [];

        $profile_user = User::all()->toArray();    

        $array_size_following = count($restore_array_following);

        for($i = 0; $i < count($profile_user); $i++)
            for ($j=0; $j<$array_size_following; $j++)
                if($profile_user[$i]['id'] == $restore_array_following[$j])
                    array_push($array_following,$profile_user[$i]); 


       
        //----------------- RECUPERAR SEGUIDORES ---------------------

        $restore_json_follower = $restore_json['ufl_follower_list'];
        $restore_array_follower = json_decode($restore_json_follower);
        $array_follower = [];
        $array_size_follower = count($restore_array_follower);

        
        for($i = 0; $i < count($profile_user); $i++)
            for ($j=0; $j<$array_size_follower; $j++)
                if($profile_user[$i]['id'] == $restore_array_follower[$j])
                    array_push($array_follower,$profile_user[$i]); 

        

     //----------------- CONTACTOS ---------------------


        $array_id_contact = [];
    
        for ($i=0; $i < $array_size_following; $i++) 
            for ($j=0; $j < $array_size_follower; $j++) 
                if($restore_array_following[$i] == $restore_array_follower[$j])
                    array_push($array_id_contact,$restore_array_following[$i]);


        $array_size_contact = count($array_id_contact);

        for($i = 0; $i < count($profile_user); $i++)
        {
            for ($j = 0; $j < $array_size_contact; $j++)
            {
                if($profile_user[$i]['id'] == $array_id_contact[$j])
                {
                    array_push($this->contacts,$profile_user[$i]);
               }
            }          
        }

        $this->total_contacts = count($this->contacts);
 
        //contar la cantidad de mensajes por cada usuario. 
        for ($i=0; $i < count($this->contacts) ; $i++) 
        { 
            $messages = Chat::where('cht_id_emit',$id_user)
            ->where('cht_id_receive',$this->contacts[$i]['id'])
            ->OrWhere('cht_id_emit',$this->contacts[$i]['id'])
            ->Where('cht_id_receive',$id_user)
            ->get();

            array_push($this->total_messages,count($messages));
        }
       
    }

    public function reloadTotalMessages()
    {
        $id_user = Auth::user()->id;

        //contar la cantidad de mensajes por cada usuario. 
        for ($i=0; $i < count($this->contacts) ; $i++) 
        { 
            $messages = Chat::where('cht_id_emit',$id_user)
            ->where('cht_id_receive',$this->contacts[$i]['id'])
            ->OrWhere('cht_id_emit',$this->contacts[$i]['id'])
            ->Where('cht_id_receive',$id_user)
            ->get();

            $this->total_messages[$i] = count($messages);
        }
    }

    public function selectUserMessage($id_user)
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');

        $this->emit('userSelected',$id_user);
    }

    public function render()
    {
        return view('livewire.chat-list-user');
    }


}

<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Models\Chat;

class ChatFormUser extends Component
{
    protected $listeners = ['userSelected','messageReloadUserChat','sendMessageOk'];

    public $username_chat; 
    public $message; 
    public $user_messages = [];
    public $id_user_selected; 
    public $photo_user_recieve;
    
    private $username;
    private $array_user_messages; 

    public function sendMessageOk()
    {
        $this->sendMessage();
    }
    public function sendMessage()
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');

        $date_time = GetDateTime();
        $id_user_auth = Auth::user()->id;
        $name_user_auth = Auth::user()->name;  
        $photo_user_auth = Auth::user()->profile_photo_path;     

        Chat::create([
            'cht_username_emit' => $name_user_auth,
            'cht_id_emit' => $id_user_auth,
            'cht_photo_emit' => $photo_user_auth,
            'cht_username_receive' => $this->username_chat,
            'cht_id_receive' => $this->id_user_selected,
            'cht_users_message' => $this->message,
            'cht_upload_date' => $date_time['date'],
            'cht_upload_time' => $date_time['time'],
        ]);


        $this->userSelected($this->id_user_selected);
        $this->message = '';

        $this->emit('reloadTotalMessages');
        
    }

    public function messageReloadUserChat()
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');

        $this->userSelected($this->id_user_selected);

    }

    public function userSelected($id_user)
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');

        $this->id_user_selected = $id_user; //capturar el id del usuario seleccionado.

        if($this->id_user_selected != null)
        {
            $username = DB::table('users')->select('name','profile_photo_path')->where('id',$id_user)->first();
            $this->username_chat = $username->name; //enviar el nombre del usuario en la burbuja.
    
            $this->photo_user_recieve = $username->profile_photo_path; 


            $id_user_auth = Auth::user()->id;
    
            $messages_list_1 = DB::table('chats')
            ->where('cht_id_emit',$id_user_auth)
            ->where('cht_id_receive',$id_user)
            ->get();
    
            $messages_list_2 = DB::table('chats')
            ->where('cht_id_emit',$id_user)
            ->where('cht_id_receive',$id_user_auth)
            ->get();
    
           
            $array_user_messages = $messages_list_1->merge($messages_list_2);

            //ordernar el arreglo segun su id.
            $this->user_messages = collect($array_user_messages)->sortBy('id')->toArray();

            //reiniciar indice del arreglo. 
            $this->user_messages = array_values($this->user_messages);
        }


    }

    public function render()
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');
        
        return view('livewire.chat-form-user');
    }
}

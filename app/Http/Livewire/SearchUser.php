<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class SearchUser extends Component
{
    public $name = ''; 

    public function mount()
    {
        $this->name = '';
    }

    public function nameRecieved()
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');

        $this->emit('nameRecieved',$this->name);
    }

    public function render()
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');
        
        return view('livewire.search-user');
    }
}

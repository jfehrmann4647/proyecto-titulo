<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

use App\Models\User;
use App\Models\Posting; 
use App\Models\Profile; 
use App\Models\Comment; 
use App\Models\UserFollower;
use App\Models\ReactionPosting;
use App\Models\Interesting;

class Access extends Component
{
    public function render()
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');
        
        //retornar información asociada al perfil de usuario.
        
        $id_user = Auth::user()->id;

        $restore_json = UserFollower::where('usr_user',$id_user)->first();
        $restore_json_following  = $restore_json->ufl_following_list;
        $restore_array_following = json_decode($restore_json_following);

        $profile_info = DB::table('users')
            ->select('profiles.prf_first_name',
                      'profiles.prf_first_surname')
            ->join('profiles', 'users.prf_code', '.profiles.id')
            ->where('users.id', $id_user)
            ->first();

        //obtener intervalo de tiempo. 
        $current_day = Carbon::now();
        $next_weekend = Carbon::now()->addDay(7);
    
        $postings = Posting::all()->sortByDesc('id')
        ->whereBetween('pst_upload_date',[$current_day->format('Y-m-d'),$next_weekend->format('Y-m-d')]);

        $array_postings = [];
        $array_size = count($restore_array_following);
        
        foreach($postings as $posting)
        {
            $count_comments = DB::table('comments')
                ->where('pst_code', $posting->id)
                ->count();
            
            for ($i=0; $i < $array_size ; $i++) 
            { 
                if($posting->usr_user == $restore_array_following[$i] || $posting->usr_user == $id_user)
                {
                    //obtenemos el nombre de usuario para agregarlo a la colección.
                    $user_data = User::find($posting->usr_user);

                    //obtenemos el nombre y apellido para agregarlo a la colección.
                    $profile_data = Profile::find($posting->usr_user);

                    //obtenemos el total de me gusta y no me gusta para agregarlo a la colección.
                    $count_likes = ReactionPosting::where('pst_code',$posting->id)
                    ->where('rpst_type','like')->count('rpst_type');

                    $count_dislikes = ReactionPosting::where('pst_code',$posting->id)
                    ->where('rpst_type','dislike')->count('rpst_type');

                    $posting->name_user_posting = $user_data->name; 
                    $posting->profile_photo_path = $user_data->profile_photo_path;
                    
                    $posting->first_name = $profile_data->prf_first_name; 
                    $posting->first_surname = $profile_data->prf_first_surname;
                    $posting->count_comments = $count_comments;
                    $posting->count_likes = $count_likes;
                    $posting->count_dislikes = $count_dislikes; 

                    array_push($array_postings,$posting); 

                }
            }
        } 
        $array_clear = DeleteDuplicate($array_postings);
        $array_postings = $array_clear;


        // retornar información asociada a intereses.

        $interestings = Interesting::orderBy('itg_name')->get();

        return view('livewire.access')
        ->with('profile_info',$profile_info)
        ->with('interestings',$interestings);
    }
}

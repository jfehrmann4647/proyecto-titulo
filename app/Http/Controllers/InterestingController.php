<?php

namespace App\Http\Controllers;
use App\Models\Interesting;
use App\Models\User; 
use App\Models\UserInteresting; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;



class InterestingController extends Controller
{

    public function storeInterestings(Request $request)
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');

        $date_time = GetDateTime();
        //obtenemos la petición y la guardamos en una variable.
        $data = request()->except('_token');

        //guardamos el id del usuario que esta logeado. 
        $id_user = Auth::user()->id;
        
        //comprobamos si no se ha seleccionado ningún interés, si se cumple retornamos un error. 
        if(!$data)
            return redirect('/ConfigProfile?error-seleccione-un-interes');

        //luego de comprobar lo anterior guardamos el array en una variable para poder iterar.
        $interestings = $data['userInterestings'];

        //buscamos si existen interes anteriores asociados al usuario y los borramos, 
        //para evitar error si el usuario selecciona dos intereses y los guarda. pero luego 
        //desea cambiar y agregar solo uno.

        $exists_interesting = DB::table('users_interestings')
        ->select('id','usr_user','itg_code')
        ->where('usr_user','=',$id_user)
        ->get();

        if($exists_interesting->isEmpty()==false)
        {
            $delete_interesting = UserInteresting::where('usr_user',$id_user);
            $delete_interesting->delete();
        }

        //luego de comprobar lo anterior se pueden insertar datos "limpios". 
        //iteramos el array y hacemos la inserción. 
        foreach ($interestings as $id_interesting)
        {
            //obtenemos los interes de los usuarios si es que existe alguno.
            $exists_interesting = DB::table('users_interestings')
            ->select('id','usr_user','itg_code')
            ->where('usr_user','=',$id_user)
            ->where('itg_code','=',$id_interesting)
            ->get();

            //si la colección esta vacia, significa que el interés no existe y por lo 
            // tanto podemos insertar, de lo contrario reedireccionamos a la vista que mas adelante
            // contendrá un mensaje de error. 
            
            if($exists_interesting->isEmpty()==true)
            {
                UserInteresting::create([
                    'usr_itg_upload_date' => $date_time['date'],
                    'usr_itg_upload_time' => $date_time['time'],
                    'usr_user' => $id_user,
                    'itg_code' => $id_interesting,
                ]);
            }
        }
        return redirect('/ConfigProfile')->with('message', 'WORKS!');
    } 

    public function deleteInterestings(Request $request)
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');
        
        $data = request()->except('_token');

        $id_user_interesting = $data['idUserInteresting'];

        $id_user = Auth::user()->id;

        //obtenemos el primer registro correspondiente a la id y lo guardamos en una variable.
        $delete_interesting = UserInteresting::where('usr_user',$id_user)
        ->where('itg_code',$id_user_interesting)->first();

        // invocamos al metodo delete para eliminar el registro.
        $delete_interesting->delete();
        
        return redirect('/intereses');
    }
   

}

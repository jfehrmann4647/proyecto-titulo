<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class ValidationController extends Controller
{
     
    public function RegisterValidationEmail(Request $request)
    {
        //validación de usuario.

        //si existe el email, retornar falso. 
        $data = request()->except('_token');
        $email_user = $data['inputValue'];
        $email_result = User::where('email',$email_user)->first();

        if(!$email_result) return 0; else return 1;   
    }

    public function RegisterValidationName(Request $request)
    {
        //validación de usuario.

        //si existe el nombre, retornar falso. 
        $data = request()->except('_token');
        $name_user = $data['inputValue'];
        $name_result = User::where('name',$name_user)->first();

        if(!$name_result) return 0; else return 1;   
    } 

    public function RegisterValidationPassword(Request $request)
    {
        //validación de usuario.
        
        $data = request()->except('_token');
        $password = $data['password'];
        $confirm_password = $data['confirm_password'];

        if($password != $confirm_password) return 1; else return 0; 
    }

}

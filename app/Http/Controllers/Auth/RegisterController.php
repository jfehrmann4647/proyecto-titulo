<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\Profile;
use App\Models\GlobalConfig;
use App\Models\UserFollower;

class RegisterController extends Controller
{
    use RegistersUsers;

    protected $redirectTo = RouteServiceProvider::HOME;
 
    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function validator(array $input)
    {

        return Validator::make($input, [
            'name' => ['required','unique:users'],
            'email' => ['required','unique:users'],
            'password' => ['required'],
        ]);
        
    }

    protected function create(array $input)
    {

        $name_user = $input['name'];

        // creamos el perfil asociado al usuario, el cual estará vacio.
        $create_global_config = GlobalConfig::create();
        $id_global_config = $create_global_config->id;
        $create_profile = Profile::create(['gcf_code'=>$id_global_config]);
        
        //obtenemos el id asociado al perfil del usuario que se creo.
        $id_user_profile = $create_profile->id;

        $url_photo = 'img/default-photos/breaknotes-profile.png';

     
        //creamos el usuario.
        User::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
            'profile_photo_path' => $url_photo,
            'status_connection' => 1,
            'status_new_user' => 1,
            'status_account' => 1,
            'prf_code' => $id_user_profile
        ]);

        //creamos la lista de seguidores y seguidos asociado al usuario.
        $create_follower_list = UserFollower::create([
            'ufl_following_list'=>'["0"]',
            'ufl_follower_list'=>'["0"]',
            'usr_user'=>$id_user_profile
        ]);

        //$personal_folder = mkdir("storage/$name_user",0774,true); 

        // retornamos el usuario porque el autenticador de laravel lo requiere.
        return User::find($id_user_profile);
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


use App\Models\User;
use App\Models\DeactivateAccount;

class LoginController extends Controller
{

    public function __construct()
    {
        //solo pueden acceder usuarios no logeados, de lo contrario 
        //reedirige al home.

        $this->middleware('guest',['only' => 'showLoginForm']);
    
    }

    public function showLoginForm()
    {
        return view('GeneralIndex.login'); //mostramos el formulario de inicio de sesión.
    }

    public function  login()
    {
        //Validamos el usuario y contraseña. 
        $credentials = $this->validate(request(),[
            'email'=> 'email|required|string',
            'password' => 'required|string'

        ]);

        /* CONDICION PARA VERIFICAR SI USUARIO HA INGRESADO POR PRIMERA VEZ 
        LUEGO DE DAR DE BAJA SU CUENTA */

        $first_login_deactivate = DeactivateAccount::where('deac_email',$credentials['email'])
        ->where('deac_password_no_hashed',$credentials['password'])->first();


        if($first_login_deactivate!=null)
        {
            $hash_password = Hash::make($first_login_deactivate->deac_password_no_hashed);
            User::where('email',$first_login_deactivate->deac_email)
            ->update(['password' => $hash_password, 'status_account' => 1]);   
        }

        if(Auth::attempt($credentials))
        {
            //obtenemos el id del usuario.
            $id_user = Auth::user()->id;

            //actualizamos su estado de conexión a 1 (conectado).
            User::where('id',$id_user)->update(['status_connection'=> 1]);
            return redirect()->route('General');
        }

        return back()
        ->withErrors(['failedAuthentication' => 'Usuario NO registrado'])
        ->withInput(request(['email']));
    }

    public function logout()
    {
        //obtenemos el id del usuario.
        $id_user = Auth::user()->id;

        //actualizamos su estado de conexión a 0 (desconectado).
        User::where('id',$id_user)->update(['status_connection'=> 0]);

        //cerramos sesión y reedirigimos. 
        Auth::logout();
        return redirect ('/');
    }

}



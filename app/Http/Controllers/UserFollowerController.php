<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Profile;
use App\Models\UserFollower;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserFollowerController extends Controller
{

    public function userFollowing($id)
    {

        //validación de usuario.
        if(!Auth::user()) return redirect('/');

        $id_user = Auth::user()->id;

        //recuperamos el json perteneciente al usuario.
        $restore_json = UserFollower::where('usr_user',$id_user)->first();
        $restore_json_2 = UserFollower::where('usr_user',$id)->first();

        $restore_json_following  = $restore_json->ufl_following_list;
        $restore_json_follower = $restore_json_2->ufl_follower_list; 

        //decodificamos el json para poder iterarlo.
        $restore_array_following = json_decode($restore_json_following);
        $restore_array_follower = json_decode($restore_json_follower);

        //creamos un arreglo donde se guardarán los usuarios que ya sigue el usuario.

    
        $array_following = [];
        $array_follower = [];

        foreach ($restore_array_following as $value) 
            array_push($array_following,$value);

        foreach ($restore_array_follower as $value) 
            array_push($array_follower,$value);

        //hacemos la inserción del nuevo usuario a seguir.

        foreach ($array_following as $value) 
        {
            if($value == $id)
                return back();
            else
                $id_verified = $id; 
        }
        
        $id_user_string = strval($id_user); //convertir a string para evitar conflicto.
        $id_verified_2 = $id_user_string; 
        
        //agregamos a la lista el id previamente validado. 
        array_push($array_following,$id_verified);
        array_push($array_follower,$id_verified_2);

        
        //codificamos el json.
        $json_following = json_encode($array_following);
        $json_follower = json_encode($array_follower);
        
    
        //hacemos la actualización. 
        UserFollower::where('usr_user',$id_user)->update(['ufl_following_list'=>$json_following]);
        UserFollower::where('usr_user',$id)->update(['ufl_follower_list'=>$json_follower]);

        return back();
    
    }

   public function userUnfollowing($id)
   {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');
       
        $id_user = Auth::user()->id;

        //recuperamos el json perteneciente al usuario.
        $restore_json = UserFollower::where('usr_user',$id_user)->first();
        $restore_json_following  = $restore_json->ufl_following_list;
        $restore_json_follower = $restore_json->ufl_follower_list; 

        //decodificamos el json para poder iterarlo.
        $restore_array_following = json_decode($restore_json_following);
        $restore_array_follower = json_decode($restore_json_follower);

        //creamos un arreglo donde se guardaran los usuarios segun la condición siguiente.
        $array_following = [];
        $array_follower = [];

        // el id del usuario que se quiere dejar de seguir es distinto de los ya existentes
        // en la lista, por lo tanto se actualiza. 
        // De esta forma, se elimina el valor porque no se inserta ya que no cumple la condición.
        foreach ($restore_array_following as $value) 
            if($value != $id)
                array_push($array_following,$value);

        foreach ($restore_array_follower as $value) 
            if($value != $id)
                array_push($array_follower,$value);
        
        //codificamos el json.
        $json_following = json_encode($array_following);
        $json_follower = json_encode($array_follower);

        //hacemos la actualización. 
        UserFollower::where('usr_user',$id_user)->update(['ufl_following_list'=>$json_following]);
        UserFollower::where('usr_user',$id)->update(['ufl_follower_list'=>$json_follower]);

        return back();
   }


    public function showFollowerFollowings()
    {

        //validación de usuario.
        if(!Auth::user()) return redirect('/');

        $id_user = Auth::user()->id;

        //Funcionalidad para recuperar seguidos por el usuario.
        $restore_json = UserFollower::where('usr_user',$id_user)->first();
        $restore_json_following  = $restore_json->ufl_following_list;
        $restore_array_following = json_decode($restore_json_following);
        
        $array_following = [];

        $profile_user = DB::table('profiles')
        ->join('users', 'users.prf_code', '=', 'profiles.id')
        ->join('global_configs', 'global_configs.id', '=', 'profiles.gcf_code')
        ->select()
        ->get();
        
        $array_size = count($restore_array_following);

        foreach($profile_user as $user)
        {
            for ($i=0; $i<$array_size; $i++)
            {
                if($user->id == $restore_array_following[$i])
                {
                    array_push($array_following,$user); 
                }
            } 
        } 

        //Funcionalidad para recuperar seguidores del el usuario.

        $restore_json_follower = $restore_json->ufl_follower_list;
        $restore_array_follower = json_decode($restore_json_follower);

        $array_follower = [];

        $array_size_follower = count($restore_array_follower);

        foreach($profile_user as $user)
        {
            for ($i=0; $i<$array_size_follower; $i++)
            {
                if($user->id == $restore_array_follower[$i])
                {
                    array_push($array_follower,$user); 
                }
            } 
        } 

        return view('followers.followers-followings')
        ->with('array_following',$array_following)
        ->with('array_follower',$array_follower);
    }


    public function searchFollowing(Request $request)
    {   
        //validación de usuario.
        if(!Auth::user()) return redirect('/');
        
        // recuperamos la petición, y guardamos en una variable. 
        $request = request()->except('_token');
        $name_following = $request['nameFollowing'];

        //obtenemos el id del usuario que está logeado.
        $id_user = Auth::user()->id;

        //Recuperamos el json de usuarios seguidos. 
        $restore_json = UserFollower::where('usr_user',$id_user)->first();
        $restore_json_following  = $restore_json->ufl_following_list;
        $restore_array_following = json_decode($restore_json_following);

        //creamos un arreglo para guardarlos. 
        $array_following = [];

        //quitamos el valor inicial del json y guardamos en el arreglo. 
        foreach ($restore_array_following as $value) 
            if($value != 0)
                array_push($array_following,$value);

        //obtenemos todos los perfiles existentes.        
        $profiles = Profile::all();

        //creamos un arreglo donde se guardarán los usuarios que estamos siguiendo y que cumplan 
        //una condición.
        $array_following_profile = [];

        //obtenemos el tamaño del arreglo donde guardamos el json.
        $array_size = count($array_following);

        // iteramos las colecciones, si el id de un perfil existente es igual al 
        // id de nuestra lista de usuarios seguidos, se guardará en el arreglo.
        foreach($profiles as $profile)
        {
            for ($i=0; $i < $array_size; $i++) { 
                if($profile->id == $array_following[$i])
                {
                    array_push($array_following_profile,$profile);
                }
            }
        } 
        //creamos un arreglo donde guardaremos la coincidencia de la búsqueda.
        //lo guardamos en un arreglo por temas de compatibilidad con la vista followings-search
        $array_coincidence = [];

        //iteramos el arreglo y preguntamos si existe una subcadena (el dato a buscar) dentro 
        //de la cadena principal (el nombre del usuario).
        // Si la condición se cumple, retornamos a la vista el arreglo con la coincidencia. 
        foreach($array_following_profile as $profile)
        {
            //Posibles casos de busqueda. EJ: por minisculas
            $first_name_lower = strtolower($profile->prf_first_name);
            $first_name_upper = $profile->prf_first_name; 

            $case_1 = strpos($first_name_lower, $name_following); 
            $case_2 = strpos($first_name_upper, $name_following);

            if($case_1===false && $case_2===false)
            {
                $coincidence_false = 1;
            }
            else
            {
                array_push($array_coincidence,$profile);
                return view('debates.followings-search')->with('array_following_profile',$array_coincidence);
            }
        }

        //si el estado de la coincidencia es falso, retornamos un mensaje. 
        if($coincidence_false==1)
            return 'No se han encontrado resultados'; 
    }
    

}



<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Posting;
use App\Models\Comment;
use App\Models\ReactionPosting;

class PostingController extends Controller
{
    public function createPosting(Request $request)
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');
        
        $id_user = Auth::user()->id;
        $data = request()->except('_token');
        $date_time = GetDateTime();

        Posting::create([
            'pst_content' => $data['content'],
            'pst_upload_date' => $date_time['date'],
            'pst_upload_time' => $date_time['time'],
            'usr_user' => $id_user
        ]);

        return redirect('General');
    }

    public function createComment(Request $request)
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');

        $id_user = Auth::user()->id;
        $data = request()->except('_token');
        
        $date_time = GetDateTime();

        Comment::create([
            'cmt_content' => $data['comment_content'],
            'cmt_upload_date' => $date_time['date'],
            'cmt_upload_time' => $date_time['time'],
            'usr_user' => $id_user,
            'pst_code' => $data['idPosting']
        ]);


        return $data['comment_content']; 
    }

    public function deletePosting(Request $request)
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');

        $data = request()->except('_token');
        $delete_posting = Posting::find($data['idPosting'])->delete();
        return redirect('General');
    }

    public function deletePostingProfile(Request $request)
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');

        $data = request()->except('_token');
        $delete_posting = Posting::find($data['idPosting'])->delete();
        return back();
    }

    public function deleteComment(Request $request)
    {
        //
    }

    public function LikePosting(Request $request)
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');

        $id_user = Auth::user()->id;
        $data = request()->except('_token');

        $reactions = ReactionPosting::where('usr_user',$id_user)
        ->where('pst_code',$data['idPosting'])
        ->where('rpst_type','like')
        ->first();
        
        if($reactions)
        {
            $reactions->delete();
            return 0; 
        }
        else
        {
            ReactionPosting::create([
                'rpst_type' => 'like',
                'usr_user' => $id_user,
                'pst_code' => $data['idPosting']
            ]);
            return 1;  
        }
    }

    public function dislikePosting(Request $request)
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');
        
        $id_user = Auth::user()->id;
        $data = request()->except('_token');

        $reactions = ReactionPosting::where('usr_user',$id_user)
        ->where('pst_code',$data['idPosting'])
        ->where('rpst_type','dislike')
        ->first();
        
        if($reactions)
        {
            $reactions->delete();
            return 0; 
        }
        else
        {
            ReactionPosting::create([
                'rpst_type' => 'dislike',
                'usr_user' => $id_user,
                'pst_code' => $data['idPosting']
            ]);
            return 1;  
        }
    }

    public function likeComment()
    {
        //
    }

    public function dislikeComment()
    {
        //
    }
}

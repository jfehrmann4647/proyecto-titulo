<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


use App\Models\Interesting;
use App\Models\User; 
use App\Models\UserInteresting; 
use App\Models\Profile; 
use App\Models\GlobalConfig;
use App\Models\UserFollower;
use App\Models\DeactivateAccount;

class ConfigProfileController extends Controller
{
    public function view()
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');

        $id_user = Auth::user()->id;
        $profile_info = DB::table('users')
            ->select('profiles.prf_first_name',
                      'profiles.prf_first_surname')
            ->join('profiles', 'users.prf_code', '.profiles.id')
            ->where('users.id', $id_user)
            ->first();
                
        //-------------- INTERESES --------------------------
        
        $interestings = Interesting::all();
        
        $id_user = Auth::user()->id;
        $users_interestings = DB::table('users_interestings')
            ->join('users', 'users.id', '=', 'users_interestings.usr_user')
            ->join('interestings', 'interestings.id', '=', 'users_interestings.itg_code')
            ->select()->where('usr_user','=',$id_user)->get();

        foreach($interestings as $interesting)
            foreach($users_interestings as $user_interesting)
                if($user_interesting->itg_code == $interesting->id)
                    $interesting->interesting_exist = 1; 

       //-------------- DATOS DEL PERFIL --------------------------

        $id_user = Auth::user()->id;
        $personal_information = Profile::find($id_user);
        $personal_information_status = GlobalConfig::find($id_user);   
        
        

        //----------------- RECUPERAR SEGUIDOS ---------------------

        $id_user = Auth::user()->id;

        $restore_json = UserFollower::where('usr_user',$id_user)->first();
        $restore_json_following  = $restore_json->ufl_following_list;
        $restore_array_following = json_decode($restore_json_following);
        
        $array_following = [];

        
        $profile_user = DB::table('profiles')
        ->join('users', 'users.prf_code', '=', 'profiles.id')
        ->join('global_configs', 'global_configs.id', '=', 'profiles.gcf_code')
        ->select('users.id','users.profile_photo_path','users.name')
        ->get();

        $array_size_following = count($restore_array_following);

        foreach($profile_user as $user)
            for ($i=0; $i<$array_size_following; $i++)
                if($user->id == $restore_array_following[$i])
                    array_push($array_following,$user); 


        //----------------- RECUPERAR SEGUIDORES ---------------------

        $restore_json_follower = $restore_json->ufl_follower_list;
        $restore_array_follower = json_decode($restore_json_follower);
        $array_follower = [];
        $array_size_follower = count($restore_array_follower);

        foreach($profile_user as $user)
            for ($i=0; $i<$array_size_follower; $i++)
                if($user->id == $restore_array_follower[$i])
                    array_push($array_follower,$user); 



     //----------------- CONTACTOS ---------------------


        $array_id_contact = [];
        $array_contact = [];
        
        for ($i=0; $i < $array_size_following; $i++) 
            for ($j=0; $j < $array_size_follower; $j++) 
                if($restore_array_following[$i] == $restore_array_follower[$j])
                    array_push($array_id_contact,$restore_array_following[$i]);


        $array_size_contact = count($array_id_contact);

        foreach($profile_user as $user)
            for ($i=0; $i<$array_size_contact; $i++)
                if($user->id == $array_id_contact[$i])
                    array_push($array_contact,$user);

        
        return view('profile.ConfigProfileSection')->with('interestings', $interestings)
            ->with('users_interestings',$users_interestings)
            ->with('personal_information',$personal_information)
            ->with('personal_information_status',$personal_information_status)
            ->with('array_follower',$array_follower)
            ->with('array_following',$array_following)
            ->with('profile_info', $profile_info)
            ->with('array_contact',$array_contact);
    }


    public function deactivateAccount(Request $request)
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');
        
        $data = request()->except('_token');
        
        $id_user = Auth::user()->id;

        $data_user = User::where('id',$id_user)->first();

        $date_time = GetDateTime();

        if(Hash::check($data['user-password'],$data_user->password))
        {   
            //generamos una nueva contraseña por si el usuario quisiera en futuro solicitar volver. 
            $random_password = Str::random(8);
            
            $delete_deactivate_register = deactivateAccount::where('deac_usr_user',$data_user->id);
            
            if($delete_deactivate_register != null)
            {
                $delete_deactivate_register->delete();
                DeactivateAccount::create([
                    'deac_usr_user' => $data_user->id,
                    'deac_username' => $data_user->name,
                    'deac_email' => $data_user->email,
                    'deac_password_no_hashed' => $random_password,
                    'deac_upload_date' => $date_time['date'],
                    'deac_upload_time' => $date_time['time']
                ]);  
            }
            else
            {
                DeactivateAccount::create([
                    'deac_usr_user' => $data_user->id,
                    'deac_username' => $data_user->name,
                    'deac_email' => $data_user->email,
                    'deac_password_no_hashed' => $random_password,
                    'deac_upload_date' => $date_time['date'],
                    'deac_upload_time' => $date_time['time']
                ]);
            }

            User::where('id',$id_user)->update(['status_connection' => 0, 'status_account' => 0 ,'password' => '']);
            Auth::logout();
            return redirect ('/');
        }
        else
        {
            return bacK();
        }
    }
    
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
  
    public function __construct()
    {
        //solo deja ingresar a usuarios autenticados. 
        $this->middleware('auth');
    }


    public function index()
    {
        return view('General');
    }

    public function indexChat()
    {
        return view("home.index");
    }
}

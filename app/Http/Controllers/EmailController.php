<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\SendEmail;
use Mail;

use App\Models\DeactivateAccount;

class EmailController extends Controller
{
    public function getEmail()
    {
        return view('email.email-form');
    }

    public function sendEmail(Request $request)
    {
        $data_form = request()->except('_token');
        $deactivate_account = DeactivateAccount::where('deac_email',$data_form['emailUser'])->first();
        
        if($deactivate_account != null)
        {
            $data = [
                'email' => $deactivate_account->deac_email,
                'password' => $deactivate_account->deac_password_no_hashed
            ];
            Mail::to($deactivate_account->deac_email)->send(new SendEmail($data));
            
            return back();           
        }
        else
        {
            return back();
        }

    }
}

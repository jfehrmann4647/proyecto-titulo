<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use App\Models\User; 
use App\Models\Profile; 
use App\Models\GlobalConfig; 
use App\Models\UserInteresting; 
use App\Models\UserFollower; 

// controlador encargado de manejo de datos en el perfil de usuario.

class UpdateProfileController extends Controller
{
    //--------------ACTUALIZAR CONTRASEÑA DE USUARIO --------------------------

    public function UpdatePassword(Request $request)
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');

        $data = request()->except('_token');

        //obtener datos del request.
        $current_password = $data['current_password'];
        $new_password = $data['new_password'];
        $confirm_password = $data['confirm_password'];

        $id_user = Auth::user()->id;
        $data_user = User::where('id',$id_user)->first();

        //obtenemos la contraseña asociada al usuario. 
        $password_user = $data_user->password; 

        //si la contraseña actual ingresada coincide con la que viene de la base de datos y ademas
        //la contraseña nueva y confirmada coinciden,
        //se encripta la nueva contraseña y se hace la actualización. 

        if(Hash::check($current_password,$password_user) && $new_password == $confirm_password) 
        {
            $new_password_hash = Hash::make($new_password);
            User::where('id',$id_user)->update(['password'=>$new_password_hash]);
            return redirect('/ConfigProfile')->withErrors(['success' => '¡ Contraseña actualizada !']);
        }
        else
        {
            return redirect('/ConfigProfile')->withErrors(['failed' => 'Error al actualizar contraseña.']);
        }
    }

    //--------------GUARDAR DATOS DEL PERFIL --------------------------

    public function updateUserProfile(Request $request)
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');
        
        //obtenemos la petición y la guardamos en una variable.
        $data = request()->except('_token');

        
        //obtenemos el id del usuario que esta logeado.
        $id_user = Auth::user()->id;

        // guardamos los datos del perfil de usuario en una variable. 
        $first_name = $data['firstname'];
        $second_name = $data['secondname'];
        $first_surname = $data['firstsurname'];
        $second_surname = $data['secondsurname'];
        $cellphone = $data['cellphone'];
        $address_ = $data['address'];
        $birthday = $data['birthday'];
        $gender = $data['gender'];
        $description = $data['description'];

        
        $address = strtolower($address_);
        $address = deleteAccentMark($address_);
      
        // Verificamos el estado del checkbox
        // Si el checkbox fue seleccionado, se le agrega un 1 a la variable, en caso contrario,
        // se le asigna un 0.

        if(empty($data['firstnameCheck'])) $first_name_check = 0; else $first_name_check = 1;
        if(empty($data['secondnameCheck'])) $second_name_check = 0; else $second_name_check = 1;
        if(empty($data['firstsurnameCheck'])) $first_surname_check = 0; else $first_surname_check = 1;
        if(empty($data['secondsurnameCheck'])) $second_surname_check = 0; else $second_surname_check = 1;
        if(empty($data['cellphoneCheck'])) $cellphone_check = 0; else $cellphone_check = 1;
        if(empty($data['addressCheck'])) $address_check = 0; else $address_check = 1;
        if(empty($data['birthdayCheck'])) $birthday_check = 0; else $birthday_check = 1;
        if(empty($data['genderCheck'])) $gender_check = 0; else $gender_check = 1;
        if(empty($data['descriptionCheck'])) $description_check = 0; else $description_check = 1;


        //actualizamos la tabla perfil de usuario cada vez que el usuario lo requiera. 
        Profile::where('id',$id_user)->update(['prf_first_name'=>$first_name,
        'prf_second_name'=>$second_name,
        'prf_first_surname'=>$first_surname,
        'prf_second_surname'=>$second_surname,
        'prf_cellphone'=>$cellphone,
        'prf_address'=>$address,
        'prf_birthday'=>$birthday,
        'prf_gender'=>$gender,
        'prf_description'=>$description
        ]);

        //actualizamos la tabla de configuraciones globales cada vez que el usuario lo requiera.
        
        GlobalConfig::where('id',$id_user)->update(['gcf_first_name'=>$first_name_check,
        'gcf_second_name'=>$second_name_check,
        'gcf_first_surname'=>$first_surname_check,
        'gcf_second_surname'=>$second_surname_check,
        'gcf_cellphone'=>$cellphone_check,
        'gcf_address'=>$address_check,
        'gcf_birthday'=>$birthday_check,
        'gcf_gender'=>$gender_check,
        'gcf_description'=>$description_check
        ]);

        return redirect('/ConfigProfile')->withErrors(['success' => '¡ Perfil actualizado !']);
    }

    public function updatePhoto(Request $request){

        //validación de usuario.
        if(!Auth::user()) return redirect('/');

        $id_user = Auth::user()->id;
        //Buscamos crear la carpeta de fotos de perfil del usuario
        $User_Folder = User::
            select('name')
            ->where('id', $id_user)
            ->first();

        //Validamos que lo que se subio fue una imagen
        $request->validate([
            'New_Photo' => 'required|image'
        ]);

        //Guardamos la imagen con un nombre encritpado en la variable Image
        $Image=$request->file('New_Photo')->store($User_Folder->name, 'ProfilePhoto');

        //Obtenemos la URL temporal de la carpeta para guardar la imagen
        $URL=Storage::url($Image);
        
        //Se guarda la imagen en la base de datos
        $Affected = User::
              where('id', $id_user)
              ->update(['profile_photo_path' => $URL]);
        return back()->withErrors(['success' => '¡ Foto de perfil actualizada !']);;
    }

    //--------------CREAR O ACTUALIZAR INTERESES  --------------------------

    public function storeInterestings(Request $request)
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');

        $date_time = GetDateTime();
        $data = request()->except('_token');
        $id_user = Auth::user()->id;

        if(!$data)
        {
            $delete_interesting = UserInteresting::where('usr_user',$id_user)->delete(); 
            return redirect('ConfigProfile')->withErrors(['success' => '¡ Intereses actualizados !']);       
        }

        $interestings = $data['userInterestings'];
        $exists_interesting = DB::table('users_interestings')
        ->select('id','usr_user','itg_code')
        ->where('usr_user','=',$id_user)
        ->get();

        if($exists_interesting->isEmpty()==false)
        {
            $delete_interesting = UserInteresting::where('usr_user',$id_user);
            $delete_interesting->delete();
        }

        foreach ($interestings as $id_interesting)
        {
            $exists_interesting = DB::table('users_interestings')
            ->select('id','usr_user','itg_code')
            ->where('usr_user','=',$id_user)
            ->where('itg_code','=',$id_interesting)
            ->get();
 
            if($exists_interesting->isEmpty()==true)
            {
                UserInteresting::create([
                    'usr_itg_upload_date' => $date_time['date'],
                    'usr_itg_upload_time' => $date_time['time'],
                    'usr_user' => $id_user,
                    'itg_code' => $id_interesting,
                ]);
            }
        }

        return redirect('/ConfigProfile')->withErrors(['success' => '¡ Intereses actualizados !']);
    }

    public function deleteFollower(Request $request)
    {
        //seguidores = follower
        //seguidos = following 

        //validación de usuario.
        if(!Auth::user()) return redirect('/');

        $data = request()->except('_token');

        $id_user = Auth::user()->id;

        $id = $data['idFollower'];

        //recuperamos el json perteneciente al usuario.
        $restore_json = UserFollower::where('usr_user',$id_user)->first();
        $restore_json_following  = $restore_json->ufl_following_list;
        $restore_json_follower = $restore_json->ufl_follower_list; 

        //decodificamos el json para poder iterarlo.
        $restore_array_following = json_decode($restore_json_following);
        $restore_array_follower = json_decode($restore_json_follower);

        //creamos un arreglo donde se guardaran los usuarios segun la condición siguiente.
        $array_following = [];
        $array_follower = [];

        // el id del usuario que se quiere dejar de seguir es distinto de los ya existentes
        // en la lista, por lo tanto se actualiza. 
        // De esta forma, se elimina el valor porque no se inserta ya que no cumple la condición.
        foreach ($restore_array_following as $value) 
            if($value != $id)
                array_push($array_following,$value);

        foreach ($restore_array_follower as $value) 
            if($value != $id)
                array_push($array_follower,$value);
        
        //codificamos el json.
        $json_following = json_encode($array_following);
        $json_follower = json_encode($array_follower);

        //hacemos la actualización. 
        UserFollower::where('usr_user',$id_user)->update(['ufl_follower_list'=>$json_follower]);
        UserFollower::where('usr_user',$id)->update(['ufl_following_list'=>$json_following]);

        return back()->withErrors(['success' => '¡ Usuario seguidor eliminado !']);
    }

    public function deleteFollowing(Request $request)
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');
        
        //seguidores = follower
        //seguidos = following 

        $data = request()->except('_token');

        $id_user = Auth::user()->id;

        $id = $data['idFollowing'];

        //recuperamos el json perteneciente al usuario.
        $restore_json = UserFollower::where('usr_user',$id_user)->first();
        $restore_json_following  = $restore_json->ufl_following_list;
        $restore_json_follower = $restore_json->ufl_follower_list; 

        //decodificamos el json para poder iterarlo.
        $restore_array_following = json_decode($restore_json_following);
        $restore_array_follower = json_decode($restore_json_follower);

        //creamos un arreglo donde se guardaran los usuarios segun la condición siguiente.
        $array_following = [];
        $array_follower = [];

        // el id del usuario que se quiere dejar de seguir es distinto de los ya existentes
        // en la lista, por lo tanto se actualiza. 
        // De esta forma, se elimina el valor porque no se inserta ya que no cumple la condición.
        foreach ($restore_array_following as $value) 
            if($value != $id)
                array_push($array_following,$value);

        foreach ($restore_array_follower as $value) 
            if($value != $id)
                array_push($array_follower,$value);
        
        //codificamos el json.
        $json_following = json_encode($array_following);
        $json_follower = json_encode($array_follower);

        
        //hacemos la actualización. 
        UserFollower::where('usr_user',$id_user)->update(['ufl_following_list'=>$json_following]);
        UserFollower::where('usr_user',$id)->update(['ufl_follower_list'=>$json_follower]);

        return back()->withErrors(['success' => '¡ Usuario seguido eliminado !']);
    }

}

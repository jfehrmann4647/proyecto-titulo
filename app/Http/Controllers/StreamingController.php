<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;

Use App\Models\Interesting; 
Use App\Models\Streaming;
Use App\Models\User;
Use App\Models\UserFollower;

class StreamingController extends Controller
{
    public function view(){

        //validación de usuario.
        if(!Auth::user()) return redirect('/');

        $id_user = Auth::user()->id;

        $interestings = Interesting::orderBy('itg_name')->get();

        $profile_info = DB::table('users')
            ->select('profiles.prf_first_name',
                    'profiles.prf_first_surname')
            ->join('profiles', 'users.prf_code', '.profiles.id')
            ->where('users.id', $id_user)
            ->first();

        //obtener transmisiones en vivo.

        $live_streaming = DB::table('streamings')
        ->select('users.name','users.profile_photo_path','streamings.str_title',
        'streamings.str_url','interestings.itg_name','streamings.usr_user','streamings.str_photo')
        ->join('users','users.id','streamings.usr_user')
        ->join('interestings','interestings.id','streamings.itg_interesting')
        ->where('streamings.str_state',2)
        ->get();

        
        //obtener transmisiones de usuarios seguidos.
        
        $array_streamings_followings = [];

        $json_user_data = UserFollower::where('usr_user',$id_user)->first();
        $restore_json_following = $json_user_data->ufl_following_list;
        $restore_array_following = json_decode($restore_json_following);

        $array_size_following = count($restore_array_following);
        
        foreach($live_streaming as $streaming)
        {
           for ($i=0; $i < $array_size_following; $i++) { 
               if($restore_array_following[$i] == $streaming->usr_user)
               {
                   array_push($array_streamings_followings,$streaming);
               }
           }
        }

    

        return view('Views.StreamingSection')
            ->with('profile_info', $profile_info)
            ->with('interestings', $interestings)
            ->with('live_streaming',$live_streaming)
            ->with('array_streamings_followings',$array_streamings_followings);
    }




    public function viewRoom($creator_user){

        //validación de usuario.
        if(!Auth::user()) return redirect('/');

        $id_user = Auth::user()->id;
        
        $profile_info = DB::table('users')
            ->select('profiles.prf_first_name','profiles.prf_first_surname')
            ->join('profiles', 'users.prf_code', '.profiles.id')
            ->where('users.id', $id_user)
            ->first();

        $streaming_info = DB::table('streamings')
            ->select('users.id as id_user','users.name', 'users.profile_photo_path','interestings.itg_name', 'streamings.id as id_streaming','streamings.str_title', 'streamings.str_description')
            ->join('interestings', 'streamings.itg_interesting', 'interestings.id')
            ->join('users', 'users.id', 'streamings.usr_user')
            ->where('users.name', $creator_user) 
            ->orderby('streamings.id','DESC')
            ->first();
        
        return view('Rooms.StreamingRoom')
            ->with('profile_info', $profile_info)
            ->with('streaming_info', $streaming_info);
    }



    
    public function createTransmition(Request $request)
    {

        //validación de usuario.
        if(!Auth::user()) return redirect('/');

        $data = request()->except('_token');
        $id_user = Auth::user()->id;

        $date_time = GetDateTime();
        // estados : 0 => terminado, 1 => en preparación, 2 => en vivo. 

        $array_photo_streaming = photoStreaming();
        

        for ($i=1; $i <= count($array_photo_streaming); $i++) 
        { 
            if(intval($data['streaming-theme']) == $i)
            {
                $photo_streaming = $array_photo_streaming[$i];
            }
        }

        Streaming::create([
            'str_title' => $data['streaming-title'],
            'str_description' => $data['streaming-description'],
            'str_upload_date' => $date_time['date'],
            'str_upload_time' => $date_time['time'],
            'str_url'=>'',
            'str_photo' => $photo_streaming,
            'str_state' => 1,
            'usr_user' => $id_user,
            'itg_interesting' => $data['streaming-theme'],
        ]);
        $creator_user = User::select('name')->where('id',$id_user)->first();
        return redirect('Streaming/StreamingRoom/'.$creator_user->name);
    }

    public function startTransmition(Request $request)
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');

        $data = request()->except('_token');
        Streaming::where('id',$data['idStreaming'])->update(['str_url'=>$data['urlStreaming'],'str_state'=>2]);
        return 1; 
    }

    public function stopTransmition(Request $request)
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');
        
        $data = request()->except('_token');
        Streaming::where('id',$data['idStreaming'])->update(['str_state' => 0]);
        return 1; 
    }
}

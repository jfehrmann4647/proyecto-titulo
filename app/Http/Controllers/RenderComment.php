<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Models\User;
use App\Models\Posting; 
use App\Models\Profile; 
use App\Models\Comment; 
use App\Models\UserFollower;

class RenderComment extends Controller
{
    public function PostComment(Request $request){

        //validación de usuario.
        if(!Auth::user()) return redirect('/');
        
        $data = request()->except('_token');

        $comment_content = Comment::where('pst_code', $data['idPosting'])
            ->select('comments.*','users.name','users.profile_photo_path')
            ->join('users', 'users.id', 'usr_user')->get();
        
        return $comment_content; 
    }
}

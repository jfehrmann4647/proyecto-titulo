<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Models\Profile;
use App\Models\GlobalConfig;
use App\Models\UserFollower;
use App\Models\User; 

class ProfileController extends Controller
{
    public function view($user_name)
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');
   
        $date=date('Y-m-d');
        $time = date('G:i:s');

        $profile_info = DB::table('users')
            ->join('profiles', 'users.prf_code', '.profiles.id')
            ->join('global_configs', 'global_configs.id', 'profiles.gcf_code')
            ->where('users.name', $user_name)
            ->where('users.status_account',1)
            ->first();

        //retornar a la vista principal si se quiere ver el perfil de un usuario
        // que ya no esta activo. 
        if($profile_info == null) return redirect()->route('General');

        $profile_interesting = DB::table('users_interestings')
            ->select('users_interestings.*',
                     'interestings.*')
            ->join('interestings', 'interestings.id', 'users_interestings.itg_code')
            ->where('users_interestings.usr_user', '=',$profile_info->id)
            ->get();
        
        $profile_posting = DB::table('postings')
            ->join('users', 'users.id', 'postings.usr_user')
            ->select('postings.*', 'users.name', 'users.profile_photo_path')
            ->where('postings.usr_user', $profile_info->id)
            ->orderBy('postings.id', 'DESC')
            ->get();

        $profile_debate = DB::table('debates')
            ->select('debates.*', 'interestings.itg_name', 'UC.name as UC_name','UO.name as UO_name', 
            'UC.profile_photo_path as UC_photo','UO.profile_photo_path as UO_photo')
            ->join('users as UC', 'UC.id', 'debates.usr_user')
            ->leftJoin('users as UO', 'UO.id', 'debates.usr_opponent')
            ->join('interestings', 'interestings.id', 'debates.itg_interesting')
            ->orderBy('debates.id', 'DESC')
            ->where('usr_user', $profile_info->id)
            ->get();


        $edad = Carbon::parse($profile_info->prf_birthday)->age; // 1990-10-25

        
        //comprobar si el usuario es seguido o no.
        $id_user = Auth::user()->id;
        $restore_json = UserFollower::where('usr_user',$id_user)->first();
        $restore_json_following  = $restore_json->ufl_following_list;
        $restore_array_following = json_decode($restore_json_following);
        

        $array_following = [];
        foreach ($restore_array_following as $value) 
            array_push($array_following,$value);

        $array_size = count($array_following);
        $status = 0; 

        for ($i=0; $i < $array_size ; $i++) 
            if($array_following[$i] == $profile_info->id)
               $status = 1;  

        // ---------obtener total de seguidores y seguidos -------


        //seguidores = follower
        //seguidos = following 

        $user_profile = User::where('name',$user_name)->select('id')->first();
        $json_user_profile = UserFollower::where('usr_user',$user_profile->id)
        ->first();

        $restore_json_following = $json_user_profile->ufl_following_list;
        $restore_array_following = json_decode($restore_json_following);

        $restore_json_follower = $json_user_profile->ufl_follower_list;
        $restore_array_follower = json_decode($restore_json_follower);

        $followings_total = count($restore_array_following);
        $followers_total = count($restore_array_follower);

        if($followings_total >= 1 || $followers_total >= 1)
        {
            $followings_total --;
            $followers_total --;
        }

        return view('Views.ProfileSection')
            ->with('edad', $edad)
            ->with('profile_info', $profile_info)
            ->with('profile_interesting', $profile_interesting)
            ->with('profile_posting', $profile_posting)
            ->with('profile_debate', $profile_debate)
            ->with('status',$status)
            ->with('this_date', $date)
            ->with('this_time', $time)
            ->with('followings_total',$followings_total)
            ->with('followers_total',$followers_total);
    }
}

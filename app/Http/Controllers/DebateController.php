<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Models\User;
use App\Models\UserFollower;
use App\Models\Profile;
Use App\Models\Interesting; 
Use App\Models\Debate; 
Use App\Models\DebateArgument; 

class DebateController extends Controller
{
    
    public function view(){

        //validación de usuario.
        if(!Auth::user()) return redirect('/');

        date_default_timezone_set('America/Santiago');
        $interestings = Interesting::orderBy('itg_name')->get();

        
        $debates = DB::table('debates')
            ->select('debates.*', 'interestings.itg_name', 'UC.name as UC_name','UO.name as UO_name', 
            'UC.profile_photo_path as UC_photo','UO.profile_photo_path as UO_photo')
            ->join('users as UC', 'UC.id', 'debates.usr_user')
            ->leftJoin('users as UO', 'UO.id', 'debates.usr_opponent')
            ->join('interestings', 'interestings.id', 'debates.itg_interesting')
            ->orderBy('debates.id', 'DESC')
            ->get();
        
        $id_user = Auth::user()->id;
        $date=date('Y-m-d');
        $time = date('G:i:s');


        $profile_info = DB::table('users')
            ->select('profiles.prf_first_name','profiles.prf_first_surname')
            ->join('profiles', 'users.prf_code', '.profiles.id')
            ->where('users.id', $id_user)
            ->first();

        return view('Views.DebateSection')
            ->with('profile_info', $profile_info)
            ->with('interestings',$interestings)
            ->with('debates',$debates)
            ->with('this_date', $date)
            ->with('this_time', $time);
    }
    
    public function viewRoom($id, $name){
        // 0 = debate cerrado; 1 = debate abierto; 2 = debate en espera. 

        //validación de usuario.
        if(!Auth::user()) return redirect('/');

        date_default_timezone_set('America/Santiago');
        $date=date('Y-m-d');
        $time = date('G:i:s');

        $debate = DB::table('debates')
            ->select('debates.*', 'interestings.itg_name','UC.name as UC_name', 'UC.profile_photo_path as UC_photo',
                     'UO.name as UO_name', 'UO.profile_photo_path as UO_photo')
            ->join('users as UC', 'UC.id', 'debates.usr_user')
            ->leftJoin('users as UO', 'UO.id', 'debates.usr_opponent')
            ->join('interestings', 'interestings.id', 'debates.itg_interesting')
            ->where('debates.id', $id)
            ->first();


        if($debate->dbt_state == 1){

            if($debate->dbt_state_date <= $date){
                if($debate->dbt_state_time < $time){
                    DB::table('debates')
                        ->where('id', $id)
                        ->update(['dbt_state' => 0]);

                        $debate = DB::table('debates')
                        ->select('debates.*', 'interestings.itg_name','UC.name as UC_name', 'UC.profile_photo_path as UC_photo',
                                 'UO.name as UO_name', 'UO.profile_photo_path as UO_photo')
                        ->join('users as UC', 'UC.id', 'debates.usr_user')
                        ->leftJoin('users as UO', 'UO.id', 'debates.usr_opponent')
                        ->join('interestings', 'interestings.id', 'debates.itg_interesting')
                        ->where('debates.id', $id)
                        ->first();
                }
            }
        }
                    
        $id_user = Auth::user()->id;

        $profile_info = DB::table('users')
            ->select('profiles.prf_first_name','profiles.prf_first_surname')
            ->join('profiles', 'users.prf_code', '.profiles.id')
            ->where('users.id', $id_user)
            ->first();

        $argument = DB::table('debate_arguments')
            ->join('users','users.id','debate_arguments.usr_user')
            ->where('dbt_debate',$id)
            ->select('users.name','users.profile_photo_path',
            'debate_arguments.id','debate_arguments.dba_content','debate_arguments.dba_like')
            ->get();


        $like_usr_user = DB::table('debate_arguments')
        ->select('dba_like')
        ->where('usr_user',$debate->usr_user)
        ->where('dbt_debate',$debate->id)
        ->sum('dba_like');
        
        $like_usr_opponent = DB::table('debate_arguments')
        ->select('dba_like')
        ->where('usr_user',$debate->usr_opponent)
        ->where('dbt_debate',$debate->id)
        ->sum('dba_like');
        
        return view('Rooms.DebateRoom')
            ->with('idDebate', $id)
            ->with('profile_info', $profile_info)
            ->with('debate',$debate)
            ->with('argument',$argument)
            ->with('like_usr_user',$like_usr_user)
            ->with('like_usr_opponent',$like_usr_opponent);
    }
    
    public function createDebate(Request $request)
    // 0 = debate cerrado; 1 = debate abierto; 2 = debate en espera.  
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');

        $data = request()->except('_token');
        $id_user = Auth::user()->id;
        $date_time = GetDateTime();


        Debate::create([
            'dbt_title' => $data['debate-title'],
            'dbt_content' => $data['content'],
            'dbt_upload_date' => $date_time['date'],
            'dbt_upload_time' => $date_time['time'],
            'dbt_state' => 2,
            'usr_user' => $id_user,
            'itg_interesting'=>$data['debate-theme']
        ]);

        return redirect('Debate');
    }

    public function deleteDebate(Request $request)
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');

        $data = request()->except('_token');
        Debate::find($data['idDebate'])->delete();
        return back();
    }

    public function debateStart(Request $request)
    {
    
        //validación de usuario.
        if(!Auth::user()) return redirect('/');

        $data = request()->except('_token');  
        $id_user = Auth::user()->id;
        $date_time = GetDateTime();

        $debate_finish_date = Carbon::now()->addDay(3);

        $user_opponent = DB::table('users')
            ->select('name', 'profile_photo_path')
            ->where('id', $id_user)
            ->first();

        $debate_name=DB::table('debates')
            ->select('dbt_title')
            ->where('id', $data['idDebate'])
            ->first();


        Debate::where('id',$data['idDebate'])
        ->update(['dbt_content_opponent' => $data['contentDebate'],
        'dbt_state' => 1,'dbt_state_date' => $debate_finish_date,
        'dbt_state_time' => $date_time['time'],'usr_opponent' => $id_user]);

        //solucionar reedireccion al debate.
        return back();
    }

    public function createArgument(Request $request)
    {
        
        //validación de usuario.
        if(!Auth::user()) return redirect('/');

        $id_user = Auth::user()->id;
        $data = request()->except('_token');  
        $date_time = GetDateTime(); 

        DebateArgument::create([
            'dba_content' => $data['debate-content'],
            'dba_upload_date' => $date_time['date'],
            'dba_upload_time' => $date_time['time'],
            'dba_like' => 0,
            'dba_like_list' => '[0]',
            'usr_user' => $id_user,
            'dbt_debate' => $data['idDebate']
        ]);
        return back();
    }

    public function likeArgument(Request $request)
    {
        //validación de usuario.
        if(!Auth::user()) return redirect('/');
        
        $id_user = Auth::user()->id;
        $data = request()->except('_token');  
        $json_like_list = DebateArgument::where('id',$data['idArgument'])->first();

        $like_list = json_decode($json_like_list->dba_like_list);
        $count_like_list = count($like_list);

        for ($i=0; $i <$count_like_list ; $i++) 
            if($like_list[$i] == $id_user) return back(); else $id_new_like = $id_user; 

        array_push($like_list,$id_new_like);

        $count_like_list = count($like_list);
        $json_like_list = json_encode($like_list);

        DebateArgument::where('id',$data['idArgument'])->update(['dba_like_list'=>$json_like_list]);
        DebateArgument::where('id',$data['idArgument'])->update(['dba_like'=>$count_like_list-1]);

        return back();

    }

}

<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return view('GeneralIndex.login');
})->name('/');


//Validaciones con Ajax
Route::POST('/RegisterValidationEmail', [App\Http\Controllers\ValidationController::class, 'RegisterValidationEmail'])->name('RegisterValidationEmail');
Route::POST('/RegisterValidationName', [App\Http\Controllers\ValidationController::class, 'RegisterValidationName'])->name('RegisterValidationName');
Route::POST('/RegisterValidationPassword', [App\Http\Controllers\ValidationController::class, 'RegisterValidationPassword'])->name('RegisterValidationPassword');




//Vistas generales 
Route::get('General', [App\Http\Controllers\GeneralController::class, 'view'])->name('General')->middleware('auth');
Route::get('Streaming', [App\Http\Controllers\StreamingController::class, 'view'])->name('Streaming');
Route::get('Debate', [App\Http\Controllers\DebateController::class, 'view'])->name('Debate');
Route::get('ConfigProfile', [App\Http\Controllers\ConfigProfileController::class, 'view'])->name('ConfigProfile');
Route::get('ConfigNewProfile', [App\Http\Controllers\ConfigNewProfileController::class, 'view'])->name('ConfigNewProfile');
Route::get('Profile/{name}', [App\Http\Controllers\ProfileController::class, 'view'])->name('Profile');



Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


//Intereses
Route::post('ConfigProfile/guardar-intereses', [App\Http\Controllers\UpdateProfileController::class, 'storeInterestings'])->name('storeInterestings');

//contraseña
Route::post('ConfigProfile/actualizar-contrasena', [App\Http\Controllers\UpdateProfileController::class, 'UpdatePassword'])->name('UpdatePassword');

//mi perfil
Route::POST('ConfigProfile/actualizar-informacion', [App\Http\Controllers\UpdateProfileController::class, 'updateUserProfile'])->name('updateUserProfile');
Route::POST('ConfigProfile/actulizar-foto', [App\Http\Controllers\UpdateProfileController::class, 'updatePhoto'])->name('updatePhoto');
Route::POST('ConfigProfile/desactivar-cuenta', [App\Http\Controllers\ConfigProfileController::class, 'deactivateAccount'])->name('deactivateAccount');
Route::POST('ConfigProfile/delete-follower', [App\Http\Controllers\UpdateProfileController::class, 'deleteFollower'])->name('deleteFollower');
Route::POST('ConfigProfile/delete-following', [App\Http\Controllers\UpdateProfileController::class, 'deleteFollowing'])->name('deleteFollowing');


//publicaciones
Route::POST('General/crear-publicacion', [App\Http\Controllers\PostingController::class, 'createPosting'])->name('createPosting');
Route::POST('General/crear-comentario', [App\Http\Controllers\PostingController::class, 'createComment'])->name('createComment');
Route::POST('PostComment', [App\Http\Controllers\RenderComment::class, 'PostComment'])->name('PostComment');
Route::POST('General/me-gusta', [App\Http\Controllers\PostingController::class, 'LikePosting'])->name('LikePosting');
Route::POST('General/no-me-gusta', [App\Http\Controllers\PostingController::class, 'dislikePosting'])->name('dislikePosting');
Route::POST('General/borrar-publicacion', [App\Http\Controllers\PostingController::class, 'deletePosting'])->name('deletePosting');
Route::POST('Profile', [App\Http\Controllers\PostingController::class, 'deletePostingProfile'])->name('deletePostingProfile');

// seguidores-seguidos
Route::GET('Profile/seguir/{id}', [App\Http\Controllers\UserFollowerController::class, 'userFollowing'])->name('userFollowing');
Route::GET('Profile/dejar-seguir/{id}', [App\Http\Controllers\UserFollowerController::class, 'userUnfollowing'])->name('userUnfollowing');

//debates
Route::POST('Debate/crear-debate', [App\Http\Controllers\DebateController::class, 'createDebate'])->name('createDebate');
Route::POST('Debate/eliminar-debate', [App\Http\Controllers\DebateController::class, 'deleteDebate'])->name('deleteDebate');
Route::GET('Debate/{id}/{name}', [App\Http\Controllers\DebateController::class, 'viewRoom'])->name('DebateRoom');
Route::POST('Debate/iniciar-debate', [App\Http\Controllers\DebateController::class, 'debateStart'])->name('debateStart');
Route::POST('Debate/crear-argumento', [App\Http\Controllers\DebateController::class, 'createArgument'])->name('createArgument');
Route::POST('Debate/apoyar-argumento', [App\Http\Controllers\DebateController::class, 'likeArgument'])->name('likeArgument');

//streamings
Route::get('Streaming/StreamingRoom/{name}', [App\Http\Controllers\StreamingController::class, 'viewRoom'])->name('StreamingRoom');
Route::POST('Streaming/crear-transmision', [App\Http\Controllers\StreamingController::class, 'createTransmition'])->name('createTransmition');
Route::POST('StreamingRoom/iniciar-transmision', [App\Http\Controllers\StreamingController::class, 'startTransmition'])->name('startTransmition');
Route::POST('StreamingRoom/detener-transmision', [App\Http\Controllers\StreamingController::class, 'stopTransmition'])->name('stopTransmition');


//recuperar cuenta
Route::GET('Recuperate-account', [App\Http\Controllers\EmailController::class, 'getEmail'])->name('getEmail');
Route::POST('Recuperate-account/send-email', [App\Http\Controllers\EmailController::class, 'sendEmail'])->name('sendEmail');









